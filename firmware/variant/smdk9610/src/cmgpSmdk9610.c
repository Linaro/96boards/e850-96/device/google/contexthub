/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <csp_common.h>
#include <cmgpSmdk9610.h>
#include <usi.h>
#include <cmgp.h>

typedef struct {
    uint32_t    port;
    UsiProtocolType protocol;
    CmgpHwacgControlType hwacg;
}UsiBoardConfigType;

static UsiBoardConfigType mUsiBoardConfig[] = {
    {USI_CHUB0,     USI_PROTOCOL_SPI, CMGP_DEFAULT_HWACG},
    {USI_CMGP1,     USI_PROTOCOL_I2C, CMGP_DEFAULT_HWACG},
    {USI_I2C_CMGP1, USI_PROTOCOL_I2C, CMGP_DEFAULT_HWACG},
    {USI_I2C_CMGP2, USI_PROTOCOL_I2C, CMGP_DEFAULT_HWACG},
    {USI_CMGP3,     USI_PROTOCOL_SPI, CMGP_DEFAULT_HWACG},
};

typedef struct {
    uint32_t intNum;
    uint32_t priority;
}CmgpIrqBoardConfigType;

static CmgpIrqBoardConfigType mCmgpIrqBoardConfig[] = {
    {CMGP_USI0_IRQ,     8},
    {CMGP_I2C0_IRQ,     8},
    {CMGP_GPIOM0_0_IRQ, 8}, // EINT to receive vsync
    {CMGP_GPIOM0_1_IRQ, 8}, // EINT to receive vsync
    {CMGP_GPIOM0_8_IRQ, 8}, // BMI160 INT
    {CMGP_GPIOM0_9_IRQ, 8}, // BMI160 INT
};

void cmgpBoardInit(void)
{
    uint32_t i, size;

    size = sizeof(mUsiBoardConfig) / sizeof(mUsiBoardConfig[0]);

    for( i = 0 ; i < size ; i++) {
        usiOpen( mUsiBoardConfig[i].port, mUsiBoardConfig[i].protocol, mUsiBoardConfig[i].hwacg );
    }

    size = sizeof(mCmgpIrqBoardConfig) / sizeof(mCmgpIrqBoardConfig[0]);

    for( i = 0 ; i < size ; i++) {
        cmgpSetIrqPriority( mCmgpIrqBoardConfig[i].intNum, mCmgpIrqBoardConfig[i].priority );
        cmgpEnableInterrupt(mCmgpIrqBoardConfig[i].intNum);
    }
}
