/*----------------------------------------------------------------------------
 *      SMDK9610  -  GPIO
 *----------------------------------------------------------------------------
 *      Name:    gpioSmdk9610.c
 *      Purpose: Initialize GPIO Pads for POR reset and wakeup from system power mode
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gpioSmdk9610.h>
#include <plat/gpio/gpio.h>
#include <plat/cmgp/cmgp.h>
#include <csp_common.h>

#if defined(SEOS)
#include <cmsis.h>
#endif
// Upon POR reset, initial settings of GPIO Pads are defined here.
static const uint8_t mGpioPortInitValue[][7] = {
// RPR-0521RS Light Sensor:: CMGP_I2C01 as I2C
    /* I2C SCL / SDA */
    {GPIO_M06_0, GPM06_0_I2C_CMGP01_SCL, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    {GPIO_M07_0, GPM07_0_I2C_CMGP01_SDA, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    /*  Interrupt : */
    {GPIO_H1_2, GPH1_2_NWEINT, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},

// BMI160 Accelation-Gyro Sensor:: CHUB_USI00 as SPI
    /* SPI CLK, CS, MISO, MOSI*/
    {GPIO_H0_0, GPH0_0_USI00_CHUB_USI_RXD_SPICLK_SCL, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    {GPIO_H0_1, GPH0_1_USI00_CHUB_USI_TXD_SPIDO_SDA, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    {GPIO_H0_2, GPH0_2_USI00_CHUB_USI_RTS_SPIDI_NA, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    {GPIO_H0_3, GPH0_3_USI00_CHUB_USI_CTS_SPICS_NA, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    /*  Interrupt : */
    //Interrupt A_INT_1 @ CMGP
    {GPIO_M08_0, GPM08_0_NWEINT, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    //Interrupt A_INT_2 @ CMGP
    {GPIO_M09_0, GPM09_0_NWEINT, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},

// BMI280 PRESSURE:: CMGP_I2C02 as I2C
    /* I2C SCL / SDA */
    {GPIO_M10_0, GPM10_0_I2C_CMGP02_SCL, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    {GPIO_M11_0, GPM11_0_I2C_CMGP02_SDA, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    /*  Interrupt : */

// LIS3MDL MAGNETIC Sensor:: CMGP_USI01 as I2C
    // I2C SCL / SDA
    {GPIO_M04_0, GPM04_0_USI_CMGP01_RXD_SPICLK_SCL, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    {GPIO_M05_0, GPM05_0_USI_CMGP01_TXD_SPIDO_SDA, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    /*  Interrupt : */
    //Interrupt M_INT @ CHUB
    //{GPIO_H1_0, GPH1_0_NWEINT, GPIO_DAT_HIGH, GPIO_PUD_PULLUP_ENABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    //Interrupt M_DRDY @ CHUB
    //{GPIO_H1_1, GPH1_1_NWEINT, GPIO_DAT_HIGH, GPIO_PUD_PULLUP_ENABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},

    // Workaround for SYSPE-37
#if defined(WORKAROUND_FOR_EXYNOS_VSYNC)
    {GPIO_M12_0, GPM12_0_GYRO_O_CSIS0_VVALID, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    {GPIO_M13_0, GPM13_0_GYRO_O_CSIS1_VVALID, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    {GPIO_M00_0, GPM00_0_NWEINT, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    {GPIO_M01_0, GPM01_0_NWEINT, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
#endif

// Sensor_Virtual#1 :: CMGP_USI03 as UART
    /* UART RX / TX */
    //{GPIO_M14_0, GPM14_0_USI_CMGP03_RXD_SPICLK_SCL, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    //{GPIO_M15_0, GPM15_0_USI_CMGP03_TXD_SPIDO_SDA, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
    //  Interrupt H_TEST1
    //Interrupt UART @ CMGP
    //{GPIO_M16_0, GPM16_0_NWEINT, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_CONPDN_OUTPUT_LOW, GPIO_PUDPDN_DISABLE, GPIO_DRV_1X},
};

// Initialize GPIO Pads to meet smdk8895 board requirement
void gpioBoardInit(void)
{
    uint32_t i, size;

    size = sizeof(mGpioPortInitValue) / sizeof(mGpioPortInitValue[0]);

    for (i = 0; i < size; i++) {
        gpioConfig((GpioPinNumType) mGpioPortInitValue[i][0],
                   (uint32_t) mGpioPortInitValue[i][1]);
        gpioSetData((GpioPinNumType) mGpioPortInitValue[i][0],
                    (uint32_t) mGpioPortInitValue[i][2]);
        gpioSetPud((GpioPinNumType) mGpioPortInitValue[i][0],
                   (uint32_t) mGpioPortInitValue[i][3]);
        gpioConfigPdn((GpioPinNumType) mGpioPortInitValue[i][0],
                      (uint32_t) mGpioPortInitValue[i][4]);
        gpioSetPudPdn((GpioPinNumType) mGpioPortInitValue[i][0],
                      (uint32_t) mGpioPortInitValue[i][5]);
        gpioSetDrvStrength((GpioPinNumType) mGpioPortInitValue[i][0],
                           (uint32_t) mGpioPortInitValue[i][6]);
    }
}

