/*----------------------------------------------------------------------------
 *      ERD3830  -  GPIO
 *----------------------------------------------------------------------------
 *      Name:    gpioErd3830.c
 *      Purpose: Initialize GPIO Pads for POR reset and wakeup from system power mode
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gpioErd3830.h>
#include <plat/gpio/gpio.h>
#include <plat/cmgp/cmgp.h>
#include <csp_common.h>

#if defined(SEOS)
#include <cmsis.h>
#endif
// Upon POR reset, initial settings of GPIO Pads are defined here.
static const uint8_t mGpioPortInitValue[][7] = {
    // BMI160 Accelation-Gyro Sensor:: CHUB_USI00 as SPI
    /* SPI CLK, CS, MISO, MOSI*/
    {GPIO_M00_0, GPM00_0_USI_CMGP00_RXD_SPICLK_SCL, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_DRV_1X},
    {GPIO_M01_0, GPM01_0_USI_CMGP00_TXD_SPIDO_SDA, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_DRV_1X},
    {GPIO_M02_0, GPM02_0_USI_CMGP00_RTS_SPIDI_NA, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_DRV_1X},
    {GPIO_M03_0, GPM03_0_USI_CMGP00_CTS_SPICS_NA, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_DRV_1X},

    /* I2C SCL / SDA */
    {GPIO_M04_0, GPM04_0_USI_CMGP01_RXD_SPICLK_SCL, GPIO_DAT_HIGH, GPIO_PUD_PULLUP_ENABLE, GPIO_DRV_1X},
    {GPIO_M05_0, GPM05_0_USI_CMGP01_TXD_SPIDO_SDA, GPIO_DAT_HIGH, GPIO_PUD_PULLUP_ENABLE, GPIO_DRV_1X},

    /*  Interrupt : */
    //G_SENSOR_INT_1
    {GPIO_M06_0, GPM06_0_NWEINT, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_DRV_1X},
    //PROXI_INT
    {GPIO_M07_0, GPM07_0_NWEINT, GPIO_DAT_HIGH, GPIO_PUD_DISABLE, GPIO_DRV_1X},

};

// Initialize GPIO Pads to meet smdk8895 board requirement
void gpioBoardInit(void)
{
    uint32_t i, size;

    size = sizeof(mGpioPortInitValue) / sizeof(mGpioPortInitValue[0]);

    for (i = 0; i < size; i++) {
        gpioConfig((GpioPinNumType) mGpioPortInitValue[i][0],
                   (uint32_t) mGpioPortInitValue[i][1]);
        gpioSetData((GpioPinNumType) mGpioPortInitValue[i][0],
                    (uint32_t) mGpioPortInitValue[i][2]);
        gpioSetPud((GpioPinNumType) mGpioPortInitValue[i][0],
                   (uint32_t) mGpioPortInitValue[i][3]);
        gpioSetDrvStrength((GpioPinNumType) mGpioPortInitValue[i][0],
                           (uint32_t) mGpioPortInitValue[i][6]);
    }
}

