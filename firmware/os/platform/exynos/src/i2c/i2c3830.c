#include <gpio.h>
#include CSP_HEADER(gpio)

typedef struct {
    GpioPinNumType pinSCL;
    GpioPinNumType pinSDA;
}I2cGpioInfoType;

/* save the speed of i2c channel which is used */
static I2cGpioInfoType mI2cGpioInfo[I2C_CHANNEL_MAX] = {
    {GPIO_M00_0, GPIO_M01_0},  // I2C_CMGP0
    {GPIO_M04_0, GPIO_M05_0},  // I2C_CMGP1
};
