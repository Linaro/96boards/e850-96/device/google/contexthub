#include <gpio.h>
#include CSP_HEADER(gpio)

typedef struct {
    GpioPinNumType pinSCL;
    GpioPinNumType pinSDA;
}I2cGpioInfoType;

/* save the speed of i2c channel which is used */
static I2cGpioInfoType mI2cGpioInfo[I2C_CHANNEL_MAX] = {
    {GPIO_H0_0, GPIO_H0_1},    // I2C_CHUB0
    {GPIO_H0_2, GPIO_H0_3},    // I2C_CHUB1
    {GPIO_M00_0, GPIO_M01_0},  // I2C_CMGP0
    {GPIO_M02_0, GPIO_M03_0},  // I2C_CMGP1
    {GPIO_M04_0, GPIO_M05_0},  // I2C_CMGP2
    {GPIO_M06_0, GPIO_M07_0},  // I2C_CMGP3
    {GPIO_M08_0, GPIO_M09_0},  // I2C_CMGP4
    {GPIO_M10_0, GPIO_M11_0},  // I2C_CMGP5
    {GPIO_M12_0, GPIO_M13_0},  // I2C_CMGP6
    {GPIO_M14_0, GPIO_M15_0},  // I2C_CMGP7
    {GPIO_M16_0, GPIO_M17_0},  // I2C_CMGP8
    {GPIO_M18_0, GPIO_M19_0},  // I2C_CMGP9
};
