/*----------------------------------------------------------------------------
 *      Exynos SoC  -  I2C
 *----------------------------------------------------------------------------
 *      Name:    i2c.c
 *      Purpose: To implement I2C APIs
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <csp_common.h>
#include <csp_printf.h>
#if defined(SEOS)
    #include <seos.h>
    #include <errno.h>
    #include <heap.h>
#endif
#include <i2cDrv.h>
#include <i2cOS.h>
#include <string.h>
#include <stdlib.h>
#include <plat/gpio/gpio.h>

#include CSP_SOURCE(i2c)

/* save the speed of i2c channel which is used */
static uint32_t mI2cChannelStatus[I2C_CHANNEL_MAX];

int i2cMasterRequest(uint32_t busId, uint32_t speedInHz)
{
    I2C_DEBUG_PRINT("i2cMasterRequest : busId = %d, speed = %d\n", (int)busId, (int)speedInHz);

    if ( busId >= I2C_CHANNEL_MAX)
        return -I2C_ERR_INVALID_VALUE;

    mI2cChannelStatus[busId] = speedInHz;
    return i2cDrvMasterOpen(busId, speedInHz);
}

int i2cMasterRelease(uint32_t busId)
{
    if ( busId >= I2C_CHANNEL_MAX)
        return -I2C_ERR_INVALID_VALUE;

    mI2cChannelStatus[busId] = 0;
    return i2cDrvClose(busId);
}

static void i2cMasterReset(uint32_t busId)
{
    uint32_t speed;
    uint32_t gpioFuncScl, gpioFuncSda;
    uint32_t timeout;
    int sclVal, sdaVal;
    int clkCnt;

    GpioPinNumType pinSCL, pinSDA;

    if(busId >= I2C_CHANNEL_MAX)
        return;

    speed = i2cDrvGetSpeed(busId);

    pinSCL = mI2cGpioInfo[busId].pinSCL;
    pinSDA = mI2cGpioInfo[busId].pinSDA;

    gpioFuncScl =  gpioGetConfig(pinSCL);
    gpioFuncSda =  gpioGetConfig(pinSDA);

    i2cDrvClose(busId);

    sdaVal = gpioGetData(pinSDA);
    sclVal = gpioGetData(pinSCL);

    CSP_PRINTF_INFO("SDA line:%d, SCL line:%d\n", sdaVal, sclVal);

    if (sdaVal == 1)
        return;

    /* Wait for SCL as high */
    if (sclVal == 0) {
        timeout = 0;
	while(timeout < 500) { // for 500ms
            sclVal = gpioGetData(pinSCL);
	    if(sclVal == 0) {
                timeout = 0;
		break;
	    }
	    mSleep(10);
            timeout += 10;
	}

	if (timeout)
            CSP_PRINTF_ERROR("SCL line is still LOW!!!\n");
    }

    sdaVal = gpioGetData(pinSDA);

    if (sdaVal == 0) {
        gpioConfig(pinSCL, GPIO_CON_OUTPUT);
	gpioConfig(pinSDA, GPIO_CON_INPUT);

        for (clkCnt = 0 ; clkCnt < 100 ; clkCnt++) {
            gpioSetData(pinSCL, 0);
	    uSleep(5);
            gpioSetData(pinSCL, 1);
	    uSleep(5);
	    if (gpioGetData(pinSDA) == 1) {
                CSP_PRINTF_INFO("SDA line is recovered\n");
		break;
	    }
	}
	if (clkCnt == 100)
            CSP_PRINTF_ERROR("SDA line is not recovered\n");
    }

    gpioConfig(pinSCL, gpioFuncScl);
    gpioConfig(pinSDA, gpioFuncSda);

    i2cDrvReset(busId);
    i2cDrvMasterOpen(busId, speed);
}

int i2cMasterTxRx(uint32_t busId, uint32_t addr, const void *txBuf, size_t txSize,
        void *rxBuf, size_t rxSize, I2cCallbackF callback, void *cookie)
{
    I2cMsgType i2cMsg;
    int ret;

    I2C_DEBUG_PRINT("i2cMasterTxRx : txSize = %d, rxSize = %d\n", txSize, rxSize);

    if ( busId >= I2C_CHANNEL_MAX)
        return -I2C_ERR_INVALID_VALUE;
    if ( addr & 0x80 )
        return -I2C_ERR_INVALID_VALUE;


    i2cMsg.addr   = addr;
    i2cMsg.rxSize = rxSize;
    i2cMsg.rxBuf  = (uint8_t *)rxBuf;
    i2cMsg.txSize = txSize;
    i2cMsg.txBuf  = (uint8_t *)txBuf;

    ret = i2cDrvMasterTxRx(busId, &i2cMsg);

    if( ret ){
	i2cMasterReset(busId);
        return ret;
    }

    if(callback)
        i2cOSRunCallback( callback, cookie, txSize, rxSize, ret );

    return 0;
}

int i2cSlaveRequest(uint32_t busId, uint32_t addr)
{
    (void)busId;
    (void)addr;
    return 0;
}

int i2cSlaveRelease(uint32_t busId)
{
    (void)busId;
    return 0;
}

void i2cSlaveEnableRx(uint32_t busId, void *rxBuf, size_t rxSize,
        I2cCallbackF callback, void *cookie)
{
    (void)busId;
    (void)rxBuf;
    (void)rxSize;
    (void)callback;
    (void)cookie;
}

int i2cSlaveTxPreamble(uint32_t busId, uint8_t byte,
        I2cCallbackF callback, void *cookie)
{
    (void)busId;
    (void)byte;
    (void)callback;
    (void)cookie;

    return 0;
}

int i2cSlaveTxPacket(uint32_t busId, const void *txBuf, size_t txSize,
        I2cCallbackF callback, void *cookie)
{
    (void)busId;
    (void)txBuf;
    (void)txSize;
    (void)callback;
    (void)cookie;

    return 0;
}

int i2cOpen(uint32_t busId, uint32_t speedInHz)
{
	return i2cMasterRequest(busId, speedInHz);
}

int i2cRead(uint32_t busId, uint32_t slaveAddr, uint32_t regAddr, uint32_t length, uint8_t *rxBuf)
{
		if( rxBuf == NULL || length == 0)
			return -I2C_ERR_INVALID_VALUE;

		rxBuf[0] = (uint8_t)regAddr;

		return i2cMasterTxRx(busId, slaveAddr, rxBuf, 1, rxBuf, length, NULL, NULL);
}


int i2cWrite(uint32_t busId, uint32_t slaveAddr, uint32_t regAddr, uint32_t length, uint8_t *txBuf)
{
		uint8_t *tempBuf = NULL;
		int ret;

		if( txBuf == NULL || length == 0)
			return -I2C_ERR_INVALID_VALUE;

#if defined(SEOS)
		tempBuf = (uint8_t*)heapAlloc(length+1);
#endif
		if(tempBuf == NULL)
				return -1;

		memcpy(tempBuf+1, txBuf, length);
		tempBuf[0] = (uint8_t)regAddr;

		ret = i2cMasterTxRx(busId, slaveAddr, tempBuf, length+1, NULL, 0, NULL, NULL);

#if defined(SEOS)
		heapFree(tempBuf);
#endif

		return ret;
}

void i2cSaveState(void)
{
    int i;

    for (i = 0 ; i < I2C_CHUB_CHANNEL_MAX ; i++){
        if(mI2cChannelStatus[i])
            i2cDrvClose(i);
    }
}

void i2cRestoreState(void)
{
    int i;

    for (i  =0 ; i < I2C_CHUB_CHANNEL_MAX ; i++){
        if(mI2cChannelStatus[i])
            i2cDrvMasterOpen(i, mI2cChannelStatus[i]);
    }
}

void i2cCmgpSaveState(void)
{
    int i;

    for (i = I2C_CHUB_CHANNEL_MAX ; i < I2C_CHANNEL_MAX ; i++){
        if (mI2cChannelStatus[i])
            i2cDrvClose(i);
    }
}

void i2cCmgpRestoreState(void)
{
    int i;

    for (i = I2C_CHUB_CHANNEL_MAX ; i < I2C_CHANNEL_MAX ; i++){
        if(mI2cChannelStatus[i])
            i2cDrvMasterOpen(i, mI2cChannelStatus[i]);
    }
}
