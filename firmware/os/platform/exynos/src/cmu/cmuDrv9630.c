/*----------------------------------------------------------------------------
 *      Exynos SoC  -  CMU
 *----------------------------------------------------------------------------
 *      Name:    cmuDrv9610.c
 *      Purpose: To implement CMU driver functions
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cmu.h>
#include <cmuDrv.h>
#include <sysreg.h>
#include <sysregDrv.h>

static uint32_t tChubMainClk = CHUB_MAIN_CLK;

//
void cmuDrvInit(uint32_t mainclk)
{
    uint32_t val = 0;
	uint32_t divider = 0;

    tChubMainClk = mainclk;

    // Initializes mux and divider
    // !!! Make sure that clock source for CHUB, CLKCMU_CHUB_BUS_USER is 360Mhz and provided at this initializing time
    // MUX_CLKCMU_CHUB_BUS_USER
    val = 0;
    val |= SELECT_CLKCMU_CHUB_BUS;
    __raw_write32(val, REG_CMU_PLL_CON0_MUX_CLKCMU_CHUB_RCO_USER);

    // Wait while mux is changing
    while(__raw_read32(REG_CMU_PLL_CON0_MUX_CLKCMU_CHUB_RCO_USER) & MUX_CLKCMU_CHUB_BUS_USER_IS_BUSY);

    // CHUB_CONTROLLER_OPTION
    val = 0;
    val |= 0xF1000000;
	__raw_write32(val, REG_CMU_CHUB_CMU_CHUB_CONTROLLER_OPTION);

    // MUX_CLKCMU_CHUB_BUS_USER
    val = 0;
    val |= SELECT_CLKCMU_CHUB_BUS;
    __raw_write32(val, REG_CMU_PLL_CON0_MUX_CLK_CHUB_BUS_USER);

    // Wait while mux is changing
    while(__raw_read32(REG_CMU_PLL_CON0_MUX_CLK_CHUB_BUS_USER) & MUX_CLKCMU_CHUB_BUS_USER_IS_BUSY);

	// MUX_CLK_CHUB_BUS
    val = 0;
    val |= SELECT_BUS_MUC_CLK_CHUB_BUS_USER;
    __raw_write32(val, REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_BUS);

    // Wait while mux is changing
    while(__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_BUS) & MUX_CLK_CHUB_BUS_USER_IS_BUSY);


    // DIV_CLK_CHUB_BUS
    val = 0;
    __raw_write32(val, REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_BUS);

    // MUX_CLK_CHUB_TIMER_FCLK
    val = 0;
    val |= SELECT_TIMER_RTCCLK_CHUB;
    __raw_write32(val, REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_TIMER);

    // Wait while mux is changing
    while(__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_TIMER) & MUX_CLK_CHUB_TIMER_IS_BUSY);

    // MUX_CLK_CHUB_I2C
    val = 0;
    val |= SELECT_OSCCLK_RCO;
    __raw_write32(val, REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_I2C);

    // Wait while mux is changing
    while(__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_I2C) & MUX_CLK_CHUB_I2C_IS_BUSY);

    // divider value is clear and set to given divider value
    divider = 0x0;
    __raw_write32(((__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_I2C) & 0xFFFFFFF0) | divider), REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_I2C);

    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_I2C) & DIV_CLK_CHUB_USI00_IS_BUSY);

    // MUX_CLK_CHUB_USI00
    val = 0;
    val |= SELECT_OSCCLK_RCO;
    __raw_write32(val, REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI00);

    // Wait while mux is changing
    while(__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI00) & MUX_CLK_CHUB_USI0_IS_BUSY);

    // divider value is clear and set to given divider value
    divider = 0x1;
    __raw_write32(((__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_USI00) & 0xFFFFFFF0) | divider), REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_USI00);

    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_USI00) & DIV_CLK_CHUB_USI00_IS_BUSY);

    // MUX_CLK_CHUB_USI01
    val = 0;
    val |= SELECT_OSCCLK_RCO;
    __raw_write32(val, REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI01);

    // Wait while mux is changing
    while(__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI01) & MUX_CLK_CHUB_USI1_IS_BUSY);

	// MUX_CLK_CHUB_USI02
	val = 0;
	val |= SELECT_OSCCLK_RCO;
	__raw_write32(val, REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI02);

	// Wait while mux is changing
	while(__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI02) & MUX_CLK_CHUB_USI2_IS_BUSY);


}

static uint32_t cmuDrvGetMuxClkChubTimer(void)
{
    return (__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_TIMER) & SELECT_TIMER_RTCCLK_CHUB)? CLKCMU_RTC : CLKCMU_RCO;
    return CLKCMU_RCO;
}

static uint32_t cmuDrvGetMuxClkCmuChubBusUser(void)
{
    return (__raw_read32(REG_CMU_PLL_CON0_MUX_CLK_CHUB_BUS_USER) & SELECT_CLKCMU_CHUB_BUS)? tChubMainClk : CLKCMU_RCO;
}

static uint32_t cmuDrvGetMuxClkChubBus(void)
{
    uint32_t speed = 0;

    speed = cmuDrvGetMuxClkCmuChubBusUser();

    return speed;
}

static uint32_t cmuDrvGetCpuSpeed(void)
{
    uint32_t speed = 0;

    speed = cmuDrvGetMuxClkCmuChubBusUser();

    return speed;
}

static uint32_t cmuDrvGetMuxClkChubI2C(void)
{
    uint32_t speed = 0;

    speed = (__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_I2C) & SELECT_I2C_MUX_CLK_CHUB_BUS_USER) ? cmuDrvGetMuxClkChubBus() : CLKCMU_RCO;
    speed /= (__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_I2C) & 0xF) + 1;

    return speed;
}


static void cmuDrvSetDivChubBus(IN uint32_t divider)
{
    // No divider for shub bus
    return;
}

static void cmuDrvSetDivChubI2C(IN uint32_t divider)
{
    // divider value is clear and set to given divider value
    divider = divider & 0xF;
    __raw_write32(((__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_I2C) & 0xFFFFFFF0) | divider), REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_I2C);

    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_I2C) & DIV_CLK_CHUB_I2C_IS_BUSY);
}

static uint32_t cmuDrvGetMuxClkChubUSI00(void)
{
    uint32_t speed = 0;

    speed = (__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI00) & 0x1) ? cmuDrvGetMuxClkChubBus() : CLKCMU_RCO;
    speed /= (__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_USI00) & 0xF) + 1;

    return speed;
}

static void cmuDrvSetDivChubUSI00(IN uint32_t divider)
{
    // divider value is clear and set to given divider value
    divider = divider & 0xF;
    __raw_write32(((__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI00) & 0xFFFFFFF0) | divider), REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI00);

    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI00) & DIV_CLK_CHUB_USI00_IS_BUSY);
}

static uint32_t cmuDrvGetMuxClkChubUSI01(void)
{
    uint32_t speed = 0;

    speed = (__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI01) & 0x1) ? cmuDrvGetMuxClkChubBus() : CLKCMU_RCO;
    speed /= (__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_USI01) & 0xF) + 1;

    return speed;
}

static void cmuDrvSetDivChubUSI01(IN uint32_t divider)
{
    // divider value is clear and set to given divider value
    divider = divider & 0xF;
    __raw_write32(((__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI01) & 0xFFFFFFF0) | divider), REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI01);

    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI01) & DIV_CLK_CHUB_USI01_IS_BUSY);
}

static uint32_t cmuDrvGetMuxClkChubUSI02(void)
{
    uint32_t speed = 0;

    speed = (__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI02) & 0x1) ? cmuDrvGetMuxClkChubBus() : CLKCMU_RCO;
    speed /= (__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_USI02) & 0xF) + 1;

    return speed;
}

static void cmuDrvSetDivChubUSI02(IN uint32_t divider)
{
    // divider value is clear and set to given divider value
    divider = divider & 0xF;
    __raw_write32(((__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI02) & 0xFFFFFFF0) | divider), REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI02);

    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI02) & DIV_CLK_CHUB_USI00_IS_BUSY);
}

static uint32_t cmuDrvGetMuxClkCmgpBus(void)
{
    uint32_t speed = 0;

    speed = CLKCMU_CMGP_BUS;

    return speed;
}

static uint32_t cmuDrvGetMuxClkCmgpI2C(void)
{
#if 1
	CSP_PRINTF_INFO("[CMU] %s NOT ready! \n",__FUNCTION__);
	return cmuDrvGetMuxClkCmgpBus();
#else
	uint32_t speed = 0;

    speed = (__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_I2C_CMGP) & 0x1) ? cmuDrvGetMuxClkCmgpBus() : CLKCMU_RCO;
    speed /= (__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_I2C_CMGP) & 0xF) + 1;

    return speed;
#endif
}

static uint32_t cmuDrvGetMuxClkCmgpUSI00(void)
{
#if 1
	CSP_PRINTF_INFO("[CMU] %s NOT ready! \n",__FUNCTION__);
	return cmuDrvGetMuxClkCmgpBus();
#else
    uint32_t speed = 0;

    speed = (__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_USI_CMGP00) & 0x1) ? cmuDrvGetMuxClkCmgpBus() : CLKCMU_RCO;
    speed /= (__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP00) & 0xF) + 1;

    return speed;
#endif	
}

static uint32_t cmuDrvGetMuxClkCmgpUSI01(void)
{
#if 1
	CSP_PRINTF_INFO("[CMU] %s NOT ready! \n",__FUNCTION__);
	return cmuDrvGetMuxClkCmgpBus();
#else
    uint32_t speed = 0;

    speed = (__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_USI_CMGP01) & 0x1) ? cmuDrvGetMuxClkCmgpBus() : CLKCMU_RCO;
    speed /= (__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP01) & 0xF) + 1;

    return speed;
#endif
}

static uint32_t cmuDrvGetMuxClkCmgpUSI02(void)
{
#if 1
	CSP_PRINTF_INFO("[CMU] %s NOT ready! \n",__FUNCTION__);
	return cmuDrvGetMuxClkCmgpBus();
#else
    uint32_t speed = 0;

    speed = (__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_USI_CMGP02) & 0x1) ? cmuDrvGetMuxClkCmgpBus() : CLKCMU_RCO;
    speed /= (__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP02) & 0xF) + 1;

    return speed;
#endif
}

static uint32_t cmuDrvGetMuxClkCmgpUSI03(void)
{
#if 1
	CSP_PRINTF_INFO("[CMU] %s NOT ready! \n",__FUNCTION__);
	return cmuDrvGetMuxClkCmgpBus();
#else
    uint32_t speed = 0;

    speed = (__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_USI_CMGP03) & 0x1) ? cmuDrvGetMuxClkCmgpBus() : CLKCMU_RCO;
    speed /= (__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP03) & 0xF) + 1;

    return speed;
#endif
}

static void cmuDrvSetDivCmgpI2C(IN uint32_t divider)
{
#if 1
	CSP_PRINTF_INFO("[CMU] %s NOT ready! \n",__FUNCTION__);
#else
    // divider value is clear and set to given divider value
    divider = divider & 0xF;
    __raw_write32(((__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_I2C_CMGP) & 0xFFFFFFF0) | divider), REG_CMU_CLK_CON_DIV_DIV_CLK_I2C_CMGP);

    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_I2C_CMGP) & DIV_CLK_I2C_CMGP_IS_BUSY);
#endif
}

static void cmuDrvSetDivCmgpUSI00(IN uint32_t divider)
{
#if 1
	CSP_PRINTF_INFO("[CMU] %s NOT ready! \n",__FUNCTION__);
#else
    // divider value is clear and set to given divider value
    divider = divider & 0xF;
    __raw_write32(((__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP00) & 0xFFFFFFF0) | divider), REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP00);

    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP00) & DIV_CLK_USI_CMGP00_IS_BUSY);
#endif
}

static void cmuDrvSetDivCmgpUSI01(IN uint32_t divider)
{
#if 1
	CSP_PRINTF_INFO("[CMU] %s NOT ready! \n",__FUNCTION__);
	return;
#else
    // divider value is clear and set to given divider value
    divider = divider & 0xF;
    __raw_write32(((__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP01) & 0xFFFFFFF0) | divider), REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP01);

    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP01) & DIV_CLK_USI_CMGP01_IS_BUSY);
#endif
}

static void cmuDrvSetDivCmgpUSI02(IN uint32_t divider)
{
#if 1
	CSP_PRINTF_INFO("[CMU] %s NOT ready! \n",__FUNCTION__);
#else
    // divider value is clear and set to given divider value
    divider = divider & 0xF;
    __raw_write32(((__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP02) & 0xFFFFFFF0) | divider), REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP02);

    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP02) & DIV_CLK_USI_CMGP02_IS_BUSY);
#endif
}

static void cmuDrvSetDivCmgpUSI03(IN uint32_t divider)
{
#if 1
	CSP_PRINTF_INFO("[CMU] %s NOT ready! \n",__FUNCTION__);
#else
    // divider value is clear and set to given divider value
    divider = divider & 0xF;
    __raw_write32(((__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP03) & 0xFFFFFFF0) | divider), REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP03);

    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP03) & DIV_CLK_USI_CMGP03_IS_BUSY);
#endif
}

//
uint32_t cmuDrvGetSpeed(IN CmuIpType ip)
{
    switch(ip) {
        case CMU_CLK_OSCCLK_RCO:
            return CLKCMU_RCO;
        break;

        case CMU_CLK_RTCCLK:
            return CLKCMU_RTC;
        break;

        case CMU_CLK_OUTPUT_CMUAPM:
            return tChubMainClk;
        break;

        case CMU_CLK_OUTPUT_CPU:
            return cmuDrvGetCpuSpeed();
        break;

        case CMU_CLK_CHUB_TIMER:
            return cmuDrvGetMuxClkChubTimer();
        break;

        case CMU_CLK_CHUB_BUS:
            return cmuDrvGetMuxClkChubBus();
        break;

        case CMU_CLK_CHUB_I2C:
        case CMU_CLK_I2C01:
        case CMU_CLK_I2C03:
        case CMU_CLK_I2C05:
            return cmuDrvGetMuxClkChubI2C();
        break;

        case CMU_CLK_CHUB_USI00:
        case CMU_CLK_I2C00:
            return cmuDrvGetMuxClkChubUSI00();
        break;

        case CMU_CLK_CHUB_USI01:
        case CMU_CLK_I2C02:
            return cmuDrvGetMuxClkChubUSI01();
        break;

        case CMU_CLK_CHUB_USI02:
        case CMU_CLK_I2C04:
            return cmuDrvGetMuxClkChubUSI02();
        break;

        case CMU_CLK_CHUB_WDT:
            return CLKCMU_RCO;
        break;

        case CMU_CLK_CMGP_I2C:
        case CMU_CLK_I2C07:
        case CMU_CLK_I2C09:
        case CMU_CLK_I2C11:
        case CMU_CLK_I2C13:
            return cmuDrvGetMuxClkCmgpI2C();
        break;

        case CMU_CLK_CMGP_USI00:
        case CMU_CLK_I2C06:
            return cmuDrvGetMuxClkCmgpUSI00();
        break;

        case CMU_CLK_CMGP_USI01:
        case CMU_CLK_I2C08:
            return cmuDrvGetMuxClkCmgpUSI01();
        break;

        case CMU_CLK_CMGP_USI02:
        case CMU_CLK_I2C10:
            return cmuDrvGetMuxClkCmgpUSI02();
        break;

        case CMU_CLK_CMGP_USI03:
        case CMU_CLK_I2C12:
            return cmuDrvGetMuxClkCmgpUSI03();
        break;

        default:
            CSP_ASSERT(0);
        break;
    }

    return 0;
}

//
bool cmuDrvSetDivider(IN CmuIpType ip, IN uint32_t divider)
{
    switch(ip) {

        case CMU_CLK_CHUB_BUS:
            cmuDrvSetDivChubBus(divider);
        break;

        case CMU_CLK_CHUB_I2C:
            cmuDrvSetDivChubI2C(divider);
        break;

        case CMU_CLK_CHUB_USI00:
            cmuDrvSetDivChubUSI00(divider);
        break;

        case CMU_CLK_CHUB_USI01:
            cmuDrvSetDivChubUSI01(divider);
        break;

        case CMU_CLK_CHUB_USI02:
            cmuDrvSetDivChubUSI02(divider);
        break;

        case CMU_CLK_CMGP_I2C:
            cmuDrvSetDivCmgpI2C(divider);
        break;

        case CMU_CLK_CMGP_USI00:
            cmuDrvSetDivCmgpUSI00(divider);
        break;

        case CMU_CLK_CMGP_USI01:
            cmuDrvSetDivCmgpUSI01(divider);
        break;

        case CMU_CLK_CMGP_USI02:
            cmuDrvSetDivCmgpUSI02(divider);
        break;

        case CMU_CLK_CMGP_USI03:
            cmuDrvSetDivCmgpUSI03(divider);
        break;

        default:
            CSP_ASSERT(0);
            return false;
        break;
    }

    return true;
}

static bool isDrcgHwacgSet = 0;

//
bool cmuDrvControlHwacg(IN CmuHwacgControlType enable)
{
    // Dynamic Root Clock Gating can be set once at boot time
    if(isDrcgHwacgSet) {
        CSP_ASSERT(0);
        return false;
    }

    uint32_t val = 0;

    // CHUB_CMU_CHUB_CONTROLLER_OPTION in CMU_CHUB
    if(enable) {
        val = 0;
        val |= CHUB_CONTROLLER_OPTION_PM_ENABLE;
        val |= CHUB_CONTROLLER_OPTION_HWACG_ENABLE;
        val |= CHUB_CONTROLLER_OPTION_MEMPG_ENABLE;
        __raw_write32(val, REG_CMU_CHUB_CMU_CHUB_CONTROLLER_OPTION);
    }
    else {
        // Disable HWACG and power management
        val = 0;
        val |= CHUB_CONTROLLER_OPTION_PM_DISABLE;
        val |= CHUB_CONTROLLER_OPTION_HWACG_DISABLE;
        val |= CHUB_CONTROLLER_OPTION_MEMPG_DISABLE;
        __raw_write32(val, REG_CMU_CHUB_CMU_CHUB_CONTROLLER_OPTION);
    }

    // BUS_COMPONENT_DRCG_EN in SYSREG_CHUB
    sysregDrvSetHWACG(enable);

    isDrcgHwacgSet = 1;

    return true;
}

bool cmuDrvControlHwacgIP(IN CmuHwacgIpType ip, IN CmuHwacgControlType enable)
{
#if 0
    uint32_t val = 0;

    // Dynamic Root Clock Gating must be set once at boot time before setting the hwacg enablement of each ip
    if(!isDrcgHwacgSet) {
        CSP_ASSERT(0);
        return false;
    }

    switch(ip) {
        case CMU_HWACG_IP_CM4:
            // QCH_CON_xxx
            val = __raw_read32(REG_CMU_QCH_CON_CM4_CHUB_QCH);
            val |= ((enable) ? QCH_CON_xxx_ENABLE : QCH_CON_xxx_IGNORE_FORCE_PM_EN);
            __raw_write32(val, REG_CMU_QCH_CON_CM4_CHUB_QCH);

            // CLK_CON_xxx
            val = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_CM4_CHUB_IPCLKPORT_FCLK);
            if(enable)
                val |= CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            else
                val &= (uint32_t)~CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            __raw_write32(val, REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_CM4_CHUB_IPCLKPORT_FCLK);
        break;

        case CMU_HWACG_IP_GPIO:
            // QCH_CON_xxx
            val = __raw_read32(REG_CMU_QCH_CON_GPIO_CHUB_QCH);
            val |= ((enable) ? QCH_CON_xxx_ENABLE : QCH_CON_xxx_IGNORE_FORCE_PM_EN);
            __raw_write32(val, REG_CMU_QCH_CON_GPIO_CHUB_QCH);

            // CLK_CON_xxx
            val = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_GPIO_CHUB_IPCLKPORT_PCLK);
            if(enable)
                val |= CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            else
                val &= (uint32_t)~CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            __raw_write32(val, REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_GPIO_CHUB_IPCLKPORT_PCLK);
        break;

        case CMU_HWACG_IP_I2C_CHUB00:
            // QCH_CON_xxx
            val = __raw_read32(REG_CMU_QCH_CON_I2C_CHUB00_QCH);
            val |= ((enable) ? QCH_CON_xxx_ENABLE : QCH_CON_xxx_IGNORE_FORCE_PM_EN);
            __raw_write32(val, REG_CMU_QCH_CON_I2C_CHUB00_QCH);

            // CLK_CON_xxx
            val = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_I2C_CHUB00_IPCLKPORT_IPCLK);
            if(enable)
                val |= CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            else
                val &= (uint32_t)~CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            __raw_write32(val, REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_I2C_CHUB00_IPCLKPORT_IPCLK);
            val = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_I2C_CHUB00_IPCLKPORT_PCLK);
            if(enable)
                val |= CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            else
                val &= (uint32_t)~CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            __raw_write32(val, REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_I2C_CHUB00_IPCLKPORT_PCLK);
        break;

        case CMU_HWACG_IP_PDMA:
            // QCH_CON_xxx
            val = __raw_read32(REG_CMU_QCH_CON_PDMA_CHUB_QCH);
            val |= ((enable) ? QCH_CON_xxx_ENABLE : QCH_CON_xxx_IGNORE_FORCE_PM_EN);
            __raw_write32(val, REG_CMU_QCH_CON_PDMA_CHUB_QCH);

            // CLK_CON_xxx
            val = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_PDMA_CHUB_IPCLKPORT_ACLK);
            if(enable)
                val |= CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            else
                val &= (uint32_t)~CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            __raw_write32(val, REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_PDMA_CHUB_IPCLKPORT_ACLK);
        break;

        case CMU_HWACG_IP_PWM:
            // QCH_CON_xxx
            val = __raw_read32(REG_CMU_QCH_CON_PWM_CHUB_QCH);
            val |= ((enable) ? QCH_CON_xxx_ENABLE : QCH_CON_xxx_IGNORE_FORCE_PM_EN);
            __raw_write32(val, REG_CMU_QCH_CON_PWM_CHUB_QCH);

            // CLK_CON_xxx
            val = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_PWM_CHUB_IPCLKPORT_I_PCLK_S0);
            if(enable)
                val |= CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            else
                val &= (uint32_t)~CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            __raw_write32(val, REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_PWM_CHUB_IPCLKPORT_I_PCLK_S0);
        break;

        case CMU_HWACG_IP_TIMER:
            // QCH_CON_xxx
            val = __raw_read32(REG_CMU_QCH_CON_TIMER_CHUB_QCH);
            val |= ((enable) ? QCH_CON_xxx_ENABLE : QCH_CON_xxx_IGNORE_FORCE_PM_EN);
            __raw_write32(val, REG_CMU_QCH_CON_TIMER_CHUB_QCH);

            val = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_TIMER_CHUB_IPCLKPORT_PCLK);
            if(enable)
                val |= CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            else
                val &= (uint32_t)~CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            __raw_write32(val, REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_TIMER_CHUB_IPCLKPORT_PCLK);
        break;

        case CMU_HWACG_IP_USI_CHUB00:
            // QCH_CON_xxx
            val = __raw_read32(REG_CMU_QCH_CON_USI_CHUB00_QCH);
            val |= ((enable) ? QCH_CON_xxx_ENABLE : QCH_CON_xxx_IGNORE_FORCE_PM_EN);
            __raw_write32(val, REG_CMU_QCH_CON_USI_CHUB00_QCH);

            // CLK_CON_xxx
            val = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_USI_CHUB00_IPCLKPORT_IPCLK);
            if(enable)
                val |= CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            else
                val &= (uint32_t)~CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            __raw_write32(val, REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_USI_CHUB00_IPCLKPORT_IPCLK);
            val = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_USI_CHUB00_IPCLKPORT_PCLK);
            if(enable)
                val |= CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            else
                val &= (uint32_t)~CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            __raw_write32(val, REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_USI_CHUB00_IPCLKPORT_PCLK);
        break;

        case CMU_HWACG_IP_WDT:
            // QCH_CON_xxx
            val = __raw_read32(REG_CMU_QCH_CON_WDT_CHUB_QCH);
            val |= ((enable) ? QCH_CON_xxx_ENABLE : QCH_CON_xxx_IGNORE_FORCE_PM_EN);
            __raw_write32(val, REG_CMU_QCH_CON_WDT_CHUB_QCH);

            // CLK_CON_xxx
            val = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_WDT_CHUB_IPCLKPORT_PCLK);
            if(enable)
                val |= CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            else
                val &= (uint32_t)~CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            __raw_write32(val, REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_WDT_CHUB_IPCLKPORT_PCLK);
        break;

        default:
            CSP_ASSERT(0);
            return false;
        break;
    }
#endif
    return true;
}

// Need to increase if more SFRs need to be kept. Currently it is 27 SFRs of CMU
//#define E9630_CMUSFR_NUM 30
//static uint32_t buf_CMUSFR[E9630_CMUSFR_NUM];
void cmuDrvSaveState(void)
{
#if 1
		CSP_PRINTF_INFO("[CMU] cmuDrvSaveState NOT ready! \n");
#else
    uint32_t idx = 0;

    // SFRs set by cmdDrvInit
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_PLL_CON0_MUX_CLKCMU_CHUB_BUS_USER);
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_I2C);
	buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI00);
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_I2C);
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_USI00);

    // SFR set by cmuDrvSetDivCmgpI2C
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_I2C_CMGP);

    // SFR set by cmuDrvSetDivCmgpUSI00
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP00);

	// SFR set by cmuDrvSetDivCmgpUSI01
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP01);

    // SFR set by cmuDrvSetDivCmgpUSI02
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP02);

    // SFR set by cmuDrvSetDivCmgpUSI03
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP03);

    // SFR set by cmuDrvControlHwacg
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CHUB_CMU_CHUB_CONTROLLER_OPTION);

    // SFR set by sysregDrvSetHWACG
    //buf_CMUSFR[idx++] = __raw_read32(REG_SYSREG_BUS_COMPONENET_DRCG_EN);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_QCH_CON_CM4_CHUB_QCH);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_CM4_CHUB_IPCLKPORT_FCLK);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_QCH_CON_GPIO_CHUB_QCH);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_GPIO_CHUB_IPCLKPORT_PCLK);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_QCH_CON_I2C_CHUB00_QCH);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_I2C_CHUB00_IPCLKPORT_IPCLK);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_I2C_CHUB00_IPCLKPORT_PCLK);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_I2C_CHUB00_IPCLKPORT_PCLK);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_PDMA_CHUB_IPCLKPORT_ACLK);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_PWM_CHUB_IPCLKPORT_I_PCLK_S0);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_TIMER_CHUB_IPCLKPORT_PCLK);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_USI_CHUB00_IPCLKPORT_IPCLK);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_USI_CHUB00_IPCLKPORT_PCLK);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_QCH_CON_WDT_CHUB_QCH);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_WDT_CHUB_IPCLKPORT_PCLK);

    CSP_PRINTF_INFO("[CMU] %d SFRs saved\n", (int)idx);
#endif
}

void cmuDrvRestoreState(void)
{
#if 1
    CSP_PRINTF_INFO("[CMU] cmuDrvRestoreState NOT ready! \n");
#else
    uint32_t idx = 0;

    // The seuqence to restore should match with cmuDrvSaveState !!!

    __raw_write32(buf_CMUSFR[idx++], REG_CMU_PLL_CON0_MUX_CLKCMU_CHUB_BUS_USER);
    // Wait while mux is changing
    while(__raw_read32(REG_CMU_PLL_CON0_MUX_CLKCMU_CHUB_BUS_USER) & MUX_CLKCMU_CHUB_BUS_USER_IS_BUSY);

    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_BUS);
    // Wait while mux is changing
    while(__raw_read32(REG_CMU_PLL_CON0_MUX_CLKCMU_CHUB_BUS_USER) & MUX_CLKCMU_CHUB_BUS_USER_IS_BUSY);

    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_I2C);
    // Wait while mux is changing
    while(__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_I2C) & MUX_CLK_CHUB_I2C_IS_BUSY);

    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_TIMER);
    // Wait while mux is changing
    while(__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_I2C) & MUX_CLK_CHUB_I2C_IS_BUSY);

	__raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI00);
    // Wait while mux is changing
    while(__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_USI00) & MUX_CLK_CHUB_USI00_IS_BUSY);

    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_I2C);
    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_I2C) & DIV_CLK_CHUB_I2C_IS_BUSY);

    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_USI00);
    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_USI00) & DIV_CLK_CHUB_USI00_IS_BUSY);

    // SFR set by cmuDrvSetDivCmgpI2C
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_DIV_DIV_CLK_I2C_CMGP);
    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_I2C_CMGP) & DIV_CLK_I2C_CMGP_IS_BUSY);

    // SFR set by cmuDrvSetDivCmgpUSI00
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP00);
    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP00) & DIV_CLK_USI_CMGP00_IS_BUSY);


	// SFR set by cmuDrvSetDivCmgpUSI01
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP01);
    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP01) & DIV_CLK_USI_CMGP01_IS_BUSY);

    // SFR set by cmuDrvSetDivCmgpUSI02
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP02);
    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP02) & DIV_CLK_USI_CMGP02_IS_BUSY);

    // SFR set by cmuDrvSetDivCmgpUSI03
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP03);
    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_USI_CMGP03) & DIV_CLK_USI_CMGP03_IS_BUSY);

    // SFR set by cmuDrvControlHwacg
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CHUB_CMU_CHUB_CONTROLLER_OPTION);

    // SFR set by sysregDrvSetHWACG
    //buf_CMUSFR[idx++] = __raw_read32(REG_SYSREG_BUS_COMPONENET_DRCG_EN);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_QCH_CON_CM4_CHUB_QCH);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_CM4_CHUB_IPCLKPORT_FCLK);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_QCH_CON_GPIO_CHUB_QCH);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_GPIO_CHUB_IPCLKPORT_PCLK);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_QCH_CON_I2C_CHUB00_QCH);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_I2C_CHUB00_IPCLKPORT_IPCLK);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_I2C_CHUB00_IPCLKPORT_PCLK);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_I2C_CHUB00_IPCLKPORT_PCLK);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_PDMA_CHUB_IPCLKPORT_ACLK);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_PWM_CHUB_IPCLKPORT_I_PCLK_S0);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_TIMER_CHUB_IPCLKPORT_PCLK);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_USI_CHUB00_IPCLKPORT_IPCLK);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_USI_CHUB00_IPCLKPORT_PCLK);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_QCH_CON_WDT_CHUB_QCH);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_WDT_CHUB_IPCLKPORT_PCLK);

    CSP_PRINTF_INFO("[CMU] %d SFRs are restored\n", (int)idx);

#endif
}

