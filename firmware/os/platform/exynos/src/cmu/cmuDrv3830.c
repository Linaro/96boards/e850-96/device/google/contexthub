/*----------------------------------------------------------------------------
 *      Exynos SoC  -  CMU
 *----------------------------------------------------------------------------
 *      Name:    cmuDrv3830.c
 *      Purpose: To implement CMU driver functions
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cmu.h>
#include <cmuDrv.h>
#include <sysreg.h>
#include <sysregDrv.h>

static uint32_t tChubMainClk = CHUB_MAIN_CLK;
//
void cmuDrvInit(uint32_t mainclk)
{
    uint32_t val = 0;

    tChubMainClk = mainclk;

    // Initializes mux and divider
    // !!! Make sure that clock source for CHUB, CLKCMU_CHUB_BUS_USER is 360Mhz and provided at this initializing time

    // MUX_CLKCMU_CHUB_BUS_USER Mux
    val = 0;
    val |= SELECT_CLKCMU_CHUB_BUS;
    __raw_write32(val, REG_CMU_PLL_CON0_MUX_CLKCMU_CHUB_BUS_USER);

    // Wait while mux is changing
    while(__raw_read32(REG_CMU_PLL_CON0_MUX_CLKCMU_CHUB_BUS_USER) & MUX_CLKCMU_CHUB_BUS_USER_IS_BUSY);


    // MUX_CLK_RCO_CHUB_USER Mux
    val = 0;
    val |= SELECT_CLK_RCO_CHUB__ALV;
    __raw_write32(val, REG_CMU_PLL_CON0_MUX_CLK_RCO_CHUB_USER);

    // Wait while mux is changing
    while(__raw_read32(REG_CMU_PLL_CON0_MUX_CLK_RCO_CHUB_USER) & MUX_CLK_RCO_CHUB_BUS_USER_IS_BUSY);

    // CHUB_CONTROLLER_OPTION
    val = 0;
    val |= 0xF1000000;
    __raw_write32(val, REG_CMU_CHUB_CMU_CHUB_CONTROLLER_OPTION);

    // MUX_CLK_CHUB_BUS Mux
    val = 0;
    val |= SELECT_MUX_CLKCMU_CHUB_BUS_USER;
    __raw_write32(val, REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_BUS);

    // Wait while mux is changing
    while(__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_BUS) & MUX_CLK_CHUB_BUS_IS_BUSY);


    // MUX_CLK_CHUB_TIMER_FCLK Mux
    val = 0;
    val |= SELECT_CLK_CHUB_RTCCLK;
    __raw_write32(val, REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_TIMER_FCLK);

    // Wait while mux is changing
    while(__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_TIMER_FCLK) & MUX_CLK_CHUB_TIMER_FCLK_IS_BUSY);


    // CLK_CON_DIV_DIV_CLK_CHUB_BUS
    val = 0;
    val |= CLK_CON_DIV_DIV_CLK_CHUB_BUS_DEFAULT_DIVRATIO;
    __raw_write32(val, REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_BUS);

    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_BUS) & DIV_CLK_CHUB_BUS_IS_BUSY);

    
    // MUX_CLK_CMGP_USI0 Mux
    val = 0;
    val |= SELECT_CLK_CMGP_USI0_CLK_RCO;
    __raw_write32(val, REG_CUM_CLK_CON_MUX_MUX_CLK_CMGP_USI_CMGP0);

    // Wait while mux is changing
    while(__raw_read32(REG_CUM_CLK_CON_MUX_MUX_CLK_CMGP_USI_CMGP0) & DIV_CLK_CMGP_USI_CMGP0_IS_BUSY);


    // MUX_CLK_CMGP_USI1 Mux
    val = 0;
    val |= SELECT_CLK_CMGP_USI1_CLK_RCO;
    __raw_write32(val, REG_CUM_CLK_CON_MUX_MUX_CLK_CMGP_USI_CMGP1);

    // Wait while mux is changing
    while(__raw_read32(REG_CUM_CLK_CON_MUX_MUX_CLK_CMGP_USI_CMGP1) & DIV_CLK_CMGP_USI_CMGP1_IS_BUSY);

}

static uint32_t cmuDrvGetMuxClkChubTimer(void)
{
    return OSCCLK_RCO;
}

static uint32_t cmuDrvGetMuxClkCmuChubBusUser(void)
{
    return (__raw_read32(REG_CMU_PLL_CON0_MUX_CLKCMU_CHUB_BUS_USER) & SELECT_CLKCMU_CHUB_BUS)? tChubMainClk : OSCCLK_RCO;
}

static uint32_t cmuDrvGetMuxClkChubBus(void)
{
    uint32_t speed = 0;

    speed = cmuDrvGetMuxClkCmuChubBusUser();

    return speed;
}

static uint32_t cmuDrvGetCpuSpeed(void)
{
    uint32_t speed = 0;

    speed = cmuDrvGetMuxClkCmuChubBusUser();

    return speed;
}

static void cmuDrvSetDivChubBus(IN uint32_t divider)
{
    divider = divider & 0x7;
    __raw_write32(((__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_BUS) & 0xFFFFFFF8) | divider), REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_BUS);

    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_BUS) & DIV_CLK_CHUB_BUS_IS_BUSY);
}

static uint32_t cmuDrvGetMuxClkCmgpBus(void)
{
    uint32_t speed = 0;

    speed = CLKCMU_CMGP_BUS;

    return speed;
}

static uint32_t cmuDrvGetMuxClkCmgpUSI0(void)
{
    uint32_t speed = 0;

    speed = (__raw_read32(REG_CUM_CLK_CON_MUX_MUX_CLK_CMGP_USI_CMGP0) & 0x1) ? cmuDrvGetMuxClkCmgpBus() : CLK_RCO;
    speed /= (__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP0) & 0x1F) + 1;

    return speed;
}

static uint32_t cmuDrvGetMuxClkCmgpUSI1(void)
{
    uint32_t speed = 0;

    speed = (__raw_read32(REG_CUM_CLK_CON_MUX_MUX_CLK_CMGP_USI_CMGP1) & 0x1) ? cmuDrvGetMuxClkCmgpBus() : CLK_RCO;
    speed /= (__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP1) & 0x1F) + 1;

    return speed;
}

static void cmuDrvSetDivCmgpUSI0(IN uint32_t divider)
{
    // divider value is clear and set to given divider value
    divider = divider & 0x1F;
    __raw_write32(((__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP0) & 0xFFFFFFE0) | divider), REG_CMU_CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP0);

    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP0) & DIV_CLK_CMGP_USI_CMGP0_IS_BUSY);
}

static void cmuDrvSetDivCmgpUSI1(IN uint32_t divider)
{
    // divider value is clear and set to given divider value
    divider = divider & 0x1F;
    __raw_write32(((__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP1) & 0xFFFFFFE0) | divider), REG_CMU_CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP1);

    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP1) & DIV_CLK_CMGP_USI_CMGP1_IS_BUSY);
}

uint32_t cmuDrvGetSpeed(IN CmuIpType ip)
{
    switch(ip) {
        case CMU_CLK_OSCCLK_RCO:
            return OSCCLK_RCO;
        break;

        case CMU_CLK_RTCCLK:
            return RTCCLK;
        break;

        case CMU_CLK_OUTPUT_CMUAPM:
            return tChubMainClk;
        break;

        case CMU_CLK_OUTPUT_CPU:
            return cmuDrvGetCpuSpeed();
        break;

        case CMU_CLK_CHUB_TIMER:
            return cmuDrvGetMuxClkChubTimer();
        break;

        case CMU_CLK_CHUB_BUS:
            return cmuDrvGetMuxClkChubBus();
        break;

        case CMU_CLK_CHUB_WDT:
            return OSCCLK_RCO;
        break;

        case CMU_CLK_CMGP_USI0:
        case CMU_CLK_I2C0:
            return cmuDrvGetMuxClkCmgpUSI0();
        break;

        case CMU_CLK_CMGP_USI1:
        case CMU_CLK_I2C1:
            return cmuDrvGetMuxClkCmgpUSI1();
        break;

        default:
            CSP_ASSERT(0);
        break;
    }

    return 0;
}

//
bool cmuDrvSetDivider(IN CmuIpType ip, IN uint32_t divider)
{
    switch(ip) {

        case CMU_CLK_CHUB_BUS:
            cmuDrvSetDivChubBus(divider);
        break;

        case CMU_CLK_CMGP_USI0:
            cmuDrvSetDivCmgpUSI0(divider);
        break;

        case CMU_CLK_CMGP_USI1:
            cmuDrvSetDivCmgpUSI1(divider);
        break;

        default:
            CSP_ASSERT(0);
            return false;
        break;
    }

    return true;
}

static bool isDrcgHwacgSet = 0;

//
bool cmuDrvControlHwacg(IN CmuHwacgControlType enable)
{
    // Dynamic Root Clock Gating can be set once at boot time
    if(isDrcgHwacgSet) {
        CSP_ASSERT(0);
        return false;
    }

    uint32_t val = 0;

    // CHUB_CMU_CHUB_CONTROLLER_OPTION in CMU_CHUB
    if(enable) {
        val = 0;
        val |= CHUB_CONTROLLER_OPTION_PM_ENABLE;
        val |= CHUB_CONTROLLER_OPTION_HWACG_ENABLE;
        __raw_write32(val, REG_CMU_CHUB_CMU_CHUB_CONTROLLER_OPTION);
    }
    else {
        // Disable HWACG and power management
        val = 0;
        val |= CHUB_CONTROLLER_OPTION_PM_DISABLE;
        val |= CHUB_CONTROLLER_OPTION_HWACG_DISABLE;
        __raw_write32(val, REG_CMU_CHUB_CMU_CHUB_CONTROLLER_OPTION);
    }

    // BUS_COMPONENT_DRCG_EN in SYSREG_CHUB
    sysregDrvSetHWACG(enable);

    isDrcgHwacgSet = 1;

    return true;
}

bool cmuDrvControlHwacgIP(IN CmuHwacgIpType ip, IN CmuHwacgControlType enable)
{
    uint32_t val = 0;

    // Dynamic Root Clock Gating must be set once at boot time before setting the hwacg enablement of each ip
    if(!isDrcgHwacgSet) {
        CSP_ASSERT(0);
        return false;
    }

    switch(ip) {
        case CMU_HWACG_IP_CM4:
            // QCH_CON_xxx
            val = __raw_read32(REG_CMU_QCH_CON_CM4_CHUB_QCH);
            val |= ((enable) ? QCH_CON_xxx_ENABLE : QCH_CON_xxx_IGNORE_FORCE_PM_EN);
            __raw_write32(val, REG_CMU_QCH_CON_CM4_CHUB_QCH);

            // CLK_CON_xxx
            val = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_CM4_CHUB_IPCLKPORT_FCLK);
            if(enable)
                val |= CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            else
                val &= (uint32_t)~CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            __raw_write32(val, REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_CM4_CHUB_IPCLKPORT_FCLK);
        break;

        case CMU_HWACG_IP_PWM:
            // QCH_CON_xxx
            val = __raw_read32(REG_CMU_QCH_CON_PWM_CHUB_QCH);
            val |= ((enable) ? QCH_CON_xxx_ENABLE : QCH_CON_xxx_IGNORE_FORCE_PM_EN);
            __raw_write32(val, REG_CMU_QCH_CON_PWM_CHUB_QCH);

            // CLK_CON_xxx
            val = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_PWM_CHUB_IPCLKPORT_I_PCLK_S0);
            if(enable)
                val |= CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            else
                val &= (uint32_t)~CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            __raw_write32(val, REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_PWM_CHUB_IPCLKPORT_I_PCLK_S0);
        break;

        case CMU_HWACG_IP_TIMER:
            // QCH_CON_xxx
            val = __raw_read32(REG_CMU_QCH_CON_TIMER_CHUB_QCH);
            val |= ((enable) ? QCH_CON_xxx_ENABLE : QCH_CON_xxx_IGNORE_FORCE_PM_EN);
            __raw_write32(val, REG_CMU_QCH_CON_TIMER_CHUB_QCH);

            val = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_TIMER_CHUB_IPCLKPORT_PCLK);
            if(enable)
                val |= CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            else
                val &= (uint32_t)~CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            __raw_write32(val, REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_TIMER_CHUB_IPCLKPORT_PCLK);
        break;

        case CMU_HWACG_IP_WDT:
            // QCH_CON_xxx
            val = __raw_read32(REG_CMU_QCH_CON_WDT_CHUB_QCH);
            val |= ((enable) ? QCH_CON_xxx_ENABLE : QCH_CON_xxx_IGNORE_FORCE_PM_EN);
            __raw_write32(val, REG_CMU_QCH_CON_WDT_CHUB_QCH);

            // CLK_CON_xxx
            val = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_WDT_CHUB_IPCLKPORT_PCLK);
            if(enable)
                val |= CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            else
                val &= (uint32_t)~CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING;
            __raw_write32(val, REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_WDT_CHUB_IPCLKPORT_PCLK);
        break;

        default:
            CSP_ASSERT(0);
            return false;
        break;
    }

    return true;
}

// Need to increase if more SFRs need to be kept. Currently it is 27 SFRs of CMU
#define E3830_CMUSFR_NUM 30
static uint32_t buf_CMUSFR[E3830_CMUSFR_NUM];
void cmuDrvSaveState(void)
{
    uint32_t idx = 0;

    // SFRs set by cmdDrvInit
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_PLL_CON0_MUX_CLKCMU_CHUB_BUS_USER);
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_PLL_CON0_MUX_CLK_RCO_CHUB_USER);
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_BUS);
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_TIMER_FCLK);
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_BUS);

    // SFR set by cmuDrvSetDivChubBus
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_BUS);

    // SFR set by cmuDrvSetDivCmgpUSI0
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP0);
    
    // SFR set by cmuDrvSetDivCmgpUSI1
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP1);

    // SFR set by cmuDrvControlHwacg
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CHUB_CMU_CHUB_CONTROLLER_OPTION);

    // SFR set by sysregDrvSetHWACG
    //buf_CMUSFR[idx++] = __raw_read32(REG_SYSREG_BUS_COMPONENET_DRCG_EN);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_QCH_CON_CM4_CHUB_QCH);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_CM4_CHUB_IPCLKPORT_FCLK);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_QCH_CON_PWM_CHUB_QCH);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_PWM_CHUB_IPCLKPORT_I_PCLK_S0);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_QCH_CON_TIMER_CHUB_QCH);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_TIMER_CHUB_IPCLKPORT_PCLK);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_QCH_CON_WDT_CHUB_QCH);

    // SFR set by cmuDrvControlHwacgIP
    buf_CMUSFR[idx++] = __raw_read32(REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_WDT_CHUB_IPCLKPORT_PCLK);

    CSP_PRINTF_INFO("[CMU] %d SFRs saved\n", (int)idx);
}

void cmuDrvRestoreState(void)
{
    uint32_t idx = 0;

    // The seuqence to restore should match with cmuDrvSaveState !!!

    __raw_write32(buf_CMUSFR[idx++], REG_CMU_PLL_CON0_MUX_CLKCMU_CHUB_BUS_USER);
    // Wait while mux is changing
    while(__raw_read32(REG_CMU_PLL_CON0_MUX_CLKCMU_CHUB_BUS_USER) & MUX_CLKCMU_CHUB_BUS_USER_IS_BUSY);

    __raw_write32(buf_CMUSFR[idx++], REG_CMU_PLL_CON0_MUX_CLK_RCO_CHUB_USER);
    // Wait while mux is changing
    while(__raw_read32(REG_CMU_PLL_CON0_MUX_CLK_RCO_CHUB_USER) & MUX_CLK_RCO_CHUB_BUS_USER_IS_BUSY);

	__raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_BUS);
    // Wait while mux is changing
    while(__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_BUS) & MUX_CLK_CHUB_BUS_IS_BUSY);

    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_TIMER_FCLK);
    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_TIMER_FCLK) & MUX_CLK_CHUB_TIMER_FCLK_IS_BUSY);

    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_BUS);
    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_BUS) & DIV_CLK_CHUB_BUS_IS_BUSY);

    // SFR set by cmuDrvSetDivChubBus
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_BUS);
    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_BUS) & DIV_CLK_CHUB_BUS_IS_BUSY);

    // SFR set by cmuDrvSetDivCmgpUSI0
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP0);
    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP0) & DIV_CLK_CMGP_USI_CMGP0_IS_BUSY);


	// SFR set by cmuDrvSetDivCmgpUSI1
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP1);
    // Wait while divider is changing
    while(__raw_read32(REG_CMU_CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP1) & DIV_CLK_CMGP_USI_CMGP1_IS_BUSY);

    // SFR set by cmuDrvControlHwacg
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CHUB_CMU_CHUB_CONTROLLER_OPTION);

    // SFR set by sysregDrvSetHWACG
    //buf_CMUSFR[idx++] = __raw_read32(REG_SYSREG_BUS_COMPONENET_DRCG_EN);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_QCH_CON_CM4_CHUB_QCH);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_CM4_CHUB_IPCLKPORT_FCLK);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_QCH_CON_PWM_CHUB_QCH);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_PWM_CHUB_IPCLKPORT_I_PCLK_S0);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_QCH_CON_TIMER_CHUB_QCH);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_TIMER_CHUB_IPCLKPORT_PCLK);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_QCH_CON_WDT_CHUB_QCH);

    // SFR set by cmuDrvControlHwacgIP
    __raw_write32(buf_CMUSFR[idx++], REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_WDT_CHUB_IPCLKPORT_PCLK);

    CSP_PRINTF_INFO("[CMU] %d SFRs are restored\n", (int)idx);

}
