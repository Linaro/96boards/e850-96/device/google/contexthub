/*----------------------------------------------------------------------------
 *      Exynos SoC  -  GPIO
 *----------------------------------------------------------------------------
 *      Name:    gpio.c
 *      Purpose: To implement GPIO APIs
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <plat/gpio/gpio.h>
#include <gpioOS.h>
#include <gpioDrv.h>

// Public API to initialize GPIO. This should be called when OS starts
void gpioInit(void)
{
    gpioDrvInit();
}

// Public API to configure GPIO function
void gpioConfig(IN GpioPinNumType gpioPinNum, IN uint32_t gpioFunction)
{
    // Check if given GPIO PIN number is valid
    if (gpioPinNum >= GPIO_MAX_PIN_NUM) {
        CSP_ASSERT(0);
        return;
    }
    gpioDrvConfig(gpioPinNum, gpioFunction);
}

uint32_t gpioGetConfig(IN GpioPinNumType gpioPinNum)
{
    if (gpioPinNum >= GPIO_MAX_PIN_NUM) {
        CSP_ASSERT(0);
        return -1;
    }
    return gpioDrvGetConfig(gpioPinNum);
}

// Public API to set the level of GPIO Pad
void gpioSetData(IN GpioPinNumType gpioPinNum, IN uint32_t gpioDataValue)
{
    // Check if given GPIO PIN number is valid
    if (gpioPinNum >= GPIO_MAX_PIN_NUM) {
        CSP_ASSERT(0);
        return;
    }
    gpioDrvSetData(gpioPinNum, gpioDataValue);
}


// Public API to read the level of GPIO
uint32_t gpioGetData(IN GpioPinNumType gpioPinNum)
{
    // Check if given GPIO PIN number is valid
    if (gpioPinNum >= GPIO_MAX_PIN_NUM) {
        CSP_ASSERT(0);
        return 0xffffffff;
    }
    return gpioDrvGetData(gpioPinNum);
}


// Public API to set pull up, down setting
void gpioSetPud(IN GpioPinNumType gpioPinNum, IN uint32_t gpioPudValue)
{
    // Check if given GPIO PIN number is valid
    if (gpioPinNum >= GPIO_MAX_PIN_NUM) {
        CSP_ASSERT(0);
        return;
    }
    gpioDrvSetPud(gpioPinNum, gpioPudValue);
}


// Public API to set drive-strength
void gpioSetDrvStrength(IN GpioPinNumType gpioPinNum,
                        IN uint32_t gpioDrvValue)
{
    // Check if given GPIO PIN number is valid
    if (gpioPinNum >= GPIO_MAX_PIN_NUM) {
        CSP_ASSERT(0);
        return;
    }
    gpioDrvSetDrvStrength(gpioPinNum, gpioDrvValue);
}


// Public API to configure GPIO function for system power mode
void gpioConfigPdn(IN GpioPinNumType gpioPinNum, IN uint32_t gpioFunction)
{
    // Check if given GPIO PIN number is valid
    if (gpioPinNum >= GPIO_MAX_PIN_NUM) {
        CSP_ASSERT(0);
        return;
    }
    gpioDrvConfigPdn(gpioPinNum, gpioFunction);
}


// Public API to configure GPIO function for system power mode
void gpioSetPudPdn(IN GpioPinNumType gpioPinNum, IN uint32_t gpioPudValue)
{
    // Check if given GPIO PIN number is valid
    if (gpioPinNum >= GPIO_MAX_PIN_NUM) {
        CSP_ASSERT(0);
        return;
    }
    gpioDrvSetPudPdn(gpioPinNum, gpioPudValue);
}


//
void gpioSetExtInterrupt(IN GpioEintNumType gpioEintNum,
                         IN IntTriggerType intTrigger,
                         IN IntFilterType intFilter, IN uint32_t intFilterWidth,
                         void (*callbackFunction) (uint32_t))
{
    // Check if given GPIO EING number is valid
    if (gpioEintNum >= GPIO_MAX_EINT_NUM) {
        CSP_ASSERT(0);
        return;
    }

    if (intTrigger > Both_Edge) {
        CSP_ASSERT(0);
        return;
    }

    if ((intFilter == eEnFLT) && (intFilterWidth > 0x7F)) {
        CSP_ASSERT(0);
        return;
    }

    if (callbackFunction == NULL) {
        return;
    }

    gpioOSSetExtInterrupt(gpioEintNum, intTrigger, intFilter, intFilterWidth, callbackFunction);
}


//
void gpioUnsetExtInterrupt(IN GpioEintNumType gpioEintNum)
{
    // Check if given GPIO EING number is valid
    if (gpioEintNum >= GPIO_MAX_EINT_NUM) {
        CSP_ASSERT(0);
        return;
    }
    gpioOSUnsetExtInterrupt(gpioEintNum);
}

//
void gpioSetExtInterruptFilterSel(IN GpioEintNumType gpioEintNum, IN uint32_t intFilterSel)
{
    // Check if given GPIO EING number is valid
    if (gpioEintNum >= GPIO_MAX_EINT_NUM) {
        CSP_ASSERT(0);
        return;
    }

    if ((intFilterSel != 0) && (intFilterSel != 1)) {
        CSP_ASSERT(0);
        return;
    }

    gpioDrvSetExtInterruptFilterSel(gpioEintNum, intFilterSel);
}


//
uint32_t gpioGetDataEINT(GpioEintNumType gpioEintNum)
{
    // Check if given GPIO EING number is valid
    if (gpioEintNum >= GPIO_MAX_EINT_NUM) {
        CSP_ASSERT(0);
        return 0xffffffff;
    }
    return gpioDrvGetDataEINT(gpioEintNum);
}

#if defined(SFR_SAVE_RESTORE)
void gpioSaveState(void)
{

    // To save GPIO SFRs before entering system power mode
}

void gpioRestoreState(void)
{

    // To restore GPIO SFRs after exiting system power mode
}
#endif

// Public API shutdown GPIO
void gpioDeinit(void)
{
    gpioDrvDeinit();
}

// Wrapper layer for nanohub kernel
#if defined(SEOS)
struct Gpio *gpioRequest(uint32_t gpioNum)
{
    return (struct Gpio *)gpioNum;
}

void gpioRelease(struct Gpio *__restrict gpio)
{
    (void)gpio;
}

void gpioConfigInput(const struct Gpio *__restrict gpio,
                           int32_t gpioSpeed, enum GpioPullMode pull)
{
    gpioSetDrvStrength((GpioPinNumType) gpio, (uint32_t) gpioSpeed);
    gpioSetPud((GpioPinNumType) gpio, (uint32_t) pull);
    gpioConfig((GpioPinNumType) gpio, GPIO_CON_INPUT);
}

void gpioConfigOutput(const struct Gpio *__restrict gpio,
                            int32_t gpioSpeed, enum GpioPullMode pull,
                            enum GpioOpenDrainMode odrMode, bool value)
{
    gpioSetDrvStrength((GpioPinNumType) gpio, (uint32_t) gpioSpeed);
    gpioSetPud((GpioPinNumType) gpio, (uint32_t) pull);
    gpioSetData((GpioPinNumType) gpio,
                 ((value == 0) ? GPIO_DAT_LOW : GPIO_DAT_HIGH));
    gpioConfig((GpioPinNumType) gpio, GPIO_CON_OUTPUT);
}

void gpioConfigAlt(const struct Gpio *__restrict gpio, int32_t gpioSpeed,
                         enum GpioPullMode pull, enum GpioOpenDrainMode odrMode,
                         uint32_t altFunc)
{
    gpioSetDrvStrength((GpioPinNumType) gpio, (uint32_t) gpioSpeed);
    gpioSetPud((GpioPinNumType) gpio, (uint32_t) pull);
    gpioConfig((GpioPinNumType) gpio, (uint32_t) altFunc);
}

void gpioConfigAnalog(const struct Gpio *__restrict gpio)
{
    return;
}

bool gpioGet(const struct Gpio * __restrict gpio)
{
    return (bool) gpioGetData((GpioPinNumType) gpio);
}

void gpioSet(const struct Gpio *__restrict gpio, bool value)
{
    gpioSetData((GpioPinNumType) gpio,
                 ((value == 0) ? GPIO_DAT_LOW : GPIO_DAT_HIGH));
}

bool gpioIsPending(IN GpioEintNumType gpioEintNum)
{
    // Check if given GPIO EING number is valid
    if ( gpioEintNum >= GPIO_MAX_EINT_NUM) {
        CSP_ASSERT(0);
        return false;
    }

    return gpioDrvIsPending(gpioEintNum);
}

void gpioClearPending(IN GpioEintNumType gpioEintNum)
{
    // Check if given GPIO EING number is valid
    if ( gpioEintNum >= GPIO_MAX_EINT_NUM) {
        CSP_ASSERT(0);
        return;
    }
    gpioDrvClearPend(gpioEintNum);
}

#endif

