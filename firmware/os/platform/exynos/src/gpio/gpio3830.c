/*----------------------------------------------------------------------------
 *      Exynos SoC  -  GPIO
 *----------------------------------------------------------------------------
 *      Name:    gpio3830.c
 *      Purpose: To implement GPIO driver functions
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __GPIO3830_C__
#define __GPIO3830_C__

#define    REG_GPIO_GPM00CON    (GPIO_CMGP_BASE_ADDRESS + 0x00)
#define    REG_GPIO_GPM01CON    (GPIO_CMGP_BASE_ADDRESS + 0x20)
#define    REG_GPIO_GPM02CON    (GPIO_CMGP_BASE_ADDRESS + 0x40)
#define    REG_GPIO_GPM03CON    (GPIO_CMGP_BASE_ADDRESS + 0x60)
#define    REG_GPIO_GPM04CON    (GPIO_CMGP_BASE_ADDRESS + 0x80)
#define    REG_GPIO_GPM05CON    (GPIO_CMGP_BASE_ADDRESS + 0xA0)
#define    REG_GPIO_GPM06CON    (GPIO_CMGP_BASE_ADDRESS + 0xC0)
#define    REG_GPIO_GPM07CON    (GPIO_CMGP_BASE_ADDRESS + 0xE0)


#define GPIO_REG_ADDR(base, dat, pud, drv) \
    (REG_GPIO_##base##CON), \
    (REG_GPIO_##base##CON + dat), \
    (REG_GPIO_##base##CON + pud), \
    (REG_GPIO_##base##CON + drv), \

#define GPIO_REG_NWEINT_ADDR(group, flt) \
    REG_GPIO_NWEINT_##group##_CON, \
    REG_GPIO_NWEINT_##group##_FLTCON##flt, \
    REG_GPIO_NWEINT_##group##_MASK, \
    REG_GPIO_NWEINT_##group##_PEND

static const GpioPinInfoType mGpioPinInfo[GPIO_MAX_PIN_NUM] = {
    // GPIO_CMGP ======================

    /* GPIO_M00_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM00, 0x4, 0x8, 0xC)},
    /* GPIO_M01_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM01, 0x4, 0x8, 0xC)},
    /* GPIO_M02_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM02, 0x4, 0x8, 0xC)},
    /* GPIO_M03_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM03, 0x4, 0x8, 0xC)},
    /* GPIO_M04_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM04, 0x4, 0x8, 0xC)},
    /* GPIO_M05_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM05, 0x4, 0x8, 0xC)},
    /* GPIO_M06_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM06, 0x4, 0x8, 0xC)},
    /* GPIO_M07_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM07, 0x4, 0x8, 0xC)},
};

#define    REG_GPIO_NWEINT_GPM00_CON       (GPIO_CMGP_BASE_ADDRESS + 0x700)
#define    REG_GPIO_NWEINT_GPM01_CON       (GPIO_CMGP_BASE_ADDRESS + 0x704)
#define    REG_GPIO_NWEINT_GPM02_CON       (GPIO_CMGP_BASE_ADDRESS + 0x708)
#define    REG_GPIO_NWEINT_GPM03_CON       (GPIO_CMGP_BASE_ADDRESS + 0x70C)
#define    REG_GPIO_NWEINT_GPM04_CON       (GPIO_CMGP_BASE_ADDRESS + 0x710)
#define    REG_GPIO_NWEINT_GPM05_CON       (GPIO_CMGP_BASE_ADDRESS + 0x714)
#define    REG_GPIO_NWEINT_GPM06_CON       (GPIO_CMGP_BASE_ADDRESS + 0x718)
#define    REG_GPIO_NWEINT_GPM07_CON       (GPIO_CMGP_BASE_ADDRESS + 0x71C)

#define    REG_GPIO_NWEINT_GPM00_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x800)
#define    REG_GPIO_NWEINT_GPM01_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x804)
#define    REG_GPIO_NWEINT_GPM02_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x808)
#define    REG_GPIO_NWEINT_GPM03_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x80C)
#define    REG_GPIO_NWEINT_GPM04_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x810)
#define    REG_GPIO_NWEINT_GPM05_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x814)
#define    REG_GPIO_NWEINT_GPM06_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x818)
#define    REG_GPIO_NWEINT_GPM07_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x81C)

#define    REG_GPIO_NWEINT_GPM00_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x900)
#define    REG_GPIO_NWEINT_GPM01_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x904)
#define    REG_GPIO_NWEINT_GPM02_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x908)
#define    REG_GPIO_NWEINT_GPM03_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x90C)
#define    REG_GPIO_NWEINT_GPM04_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x910)
#define    REG_GPIO_NWEINT_GPM05_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x914)
#define    REG_GPIO_NWEINT_GPM06_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x918)
#define    REG_GPIO_NWEINT_GPM07_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x91C)

#define    REG_GPIO_NWEINT_GPM00_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA00)
#define    REG_GPIO_NWEINT_GPM01_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA04)
#define    REG_GPIO_NWEINT_GPM02_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA08)
#define    REG_GPIO_NWEINT_GPM03_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA0C)
#define    REG_GPIO_NWEINT_GPM04_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA10)
#define    REG_GPIO_NWEINT_GPM05_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA14)
#define    REG_GPIO_NWEINT_GPM06_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA18)
#define    REG_GPIO_NWEINT_GPM07_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA1C)

#define    REG_GPIO_TZPC_CMGP_0            (GPIO_CMGP_BASE_ADDRESS + 0x2000)
#define    REG_GPIO_TZPC_CMGP_1            (GPIO_CMGP_BASE_ADDRESS + 0x2004)
#define    REG_GPIO_TZPC_CMGP_2            (GPIO_CMGP_BASE_ADDRESS + 0x2008)
#define    REG_GPIO_TZPC_CMGP_3            (GPIO_CMGP_BASE_ADDRESS + 0x200C)


const GpioEintInfoType mGpioEintInfo[GPIO_MAX_EINT_NUM] = {

    /* NWEINT_GPM00_EINT_0*/ { 0,  0, 0, 0, GPIO_M00_0, GPIO_REG_NWEINT_ADDR(GPM00,0)},
    /* NWEINT_GPM01_EINT_0*/ { 0,  0, 0, 0, GPIO_M01_0, GPIO_REG_NWEINT_ADDR(GPM01,0)},
    /* NWEINT_GPM02_EINT_0*/ { 0,  0, 0, 0, GPIO_M02_0, GPIO_REG_NWEINT_ADDR(GPM02,0)},
    /* NWEINT_GPM03_EINT_0*/ { 0,  0, 0, 0, GPIO_M03_0, GPIO_REG_NWEINT_ADDR(GPM03,0)},
    /* NWEINT_GPM04_EINT_0*/ { 0,  0, 0, 0, GPIO_M04_0, GPIO_REG_NWEINT_ADDR(GPM04,0)},
    /* NWEINT_GPM05_EINT_0*/ { 0,  0, 0, 0, GPIO_M05_0, GPIO_REG_NWEINT_ADDR(GPM05,0)},
    /* NWEINT_GPM06_EINT_0*/ { 0,  0, 0, 0, GPIO_M06_0, GPIO_REG_NWEINT_ADDR(GPM06,0)},
    /* NWEINT_GPM07_EINT_0*/ { 0,  0, 0, 0, GPIO_M07_0, GPIO_REG_NWEINT_ADDR(GPM07,0)},
};

#endif
