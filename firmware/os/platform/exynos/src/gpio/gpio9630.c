/*----------------------------------------------------------------------------
 *      Exynos SoC  -  GPIO
 *----------------------------------------------------------------------------
 *      Name:    gpio9610.c
 *      Purpose: To implement GPIO driver functions
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __GPIO9630_C__
#define __GPIO9630_C__

#include CSP_HEADER(gpio)


#define    REG_GPIO_GPH0CON     (GPIO_BASE_ADDRESS + 0x00)
#define    REG_GPIO_GPH1CON     (GPIO_BASE_ADDRESS + 0x20)
#define    REG_GPIO_GPH2CON     (GPIO_BASE_ADDRESS + 0x40)

#define    REG_GPIO_GPM00CON    (GPIO_CMGP_BASE_ADDRESS + 0x00)
#define    REG_GPIO_GPM01CON    (GPIO_CMGP_BASE_ADDRESS + 0x20)
#define    REG_GPIO_GPM02CON    (GPIO_CMGP_BASE_ADDRESS + 0x40)
#define    REG_GPIO_GPM03CON    (GPIO_CMGP_BASE_ADDRESS + 0x60)
#define    REG_GPIO_GPM04CON    (GPIO_CMGP_BASE_ADDRESS + 0x80)
#define    REG_GPIO_GPM05CON    (GPIO_CMGP_BASE_ADDRESS + 0xA0)
#define    REG_GPIO_GPM06CON    (GPIO_CMGP_BASE_ADDRESS + 0xC0)
#define    REG_GPIO_GPM07CON    (GPIO_CMGP_BASE_ADDRESS + 0xE0)
#define    REG_GPIO_GPM08CON    (GPIO_CMGP_BASE_ADDRESS + 0x100)
#define    REG_GPIO_GPM09CON    (GPIO_CMGP_BASE_ADDRESS + 0x120)
#define    REG_GPIO_GPM10CON    (GPIO_CMGP_BASE_ADDRESS + 0x140)
#define    REG_GPIO_GPM11CON    (GPIO_CMGP_BASE_ADDRESS + 0x160)
#define    REG_GPIO_GPM12CON    (GPIO_CMGP_BASE_ADDRESS + 0x180)
#define    REG_GPIO_GPM13CON    (GPIO_CMGP_BASE_ADDRESS + 0x1A0)
#define    REG_GPIO_GPM14CON    (GPIO_CMGP_BASE_ADDRESS + 0x1C0)
#define    REG_GPIO_GPM15CON    (GPIO_CMGP_BASE_ADDRESS + 0x1E0)

#define GPIO_REG_ADDR(base, dat, pud, drv ) \
    (REG_GPIO_##base##CON), \
    (REG_GPIO_##base##CON + dat), \
    (REG_GPIO_##base##CON + pud), \
    (REG_GPIO_##base##CON + drv), \

#define GPIO_REG_NWEINT_ADDR(group, flt) \
    REG_GPIO_NWEINT_##group##_CON, \
    REG_GPIO_NWEINT_##group##_FLTCON##flt, \
    REG_GPIO_NWEINT_##group##_MASK, \
    REG_GPIO_NWEINT_##group##_PEND

static const GpioPinInfoType mGpioPinInfo[/*GPIO_MAX_PIN_NUM*/] = {
    // GPIO_CHUB ======================

    /* GPIO_H0_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH0, 0x4, 0x8, 0xC)},
    /* GPIO_H0_1  */ {1, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH0, 0x4, 0x8, 0xC)},
    /* GPIO_H0_2  */ {2, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH0, 0x4, 0x8, 0xC)},
    /* GPIO_H0_3  */ {3, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH0, 0x4, 0x8, 0xC)},
    /* GPIO_H0_4  */ {4, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH0, 0x4, 0x8, 0xC)},
    /* GPIO_H0_5  */ {5, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH0, 0x4, 0x8, 0xC)},
    /* GPIO_H0_6  */ {6, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH0, 0x4, 0x8, 0xC)},
    /* GPIO_H0_7  */ {7, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH0, 0x4, 0x8, 0xC)},

    /* GPIO_H1_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH1, 0x4, 0x8, 0xC)},
    /* GPIO_H1_1  */ {1, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH1, 0x4, 0x8, 0xC)},
    /* GPIO_H1_2  */ {2, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH1, 0x4, 0x8, 0xC)},
    /* GPIO_H1_3  */ {3, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH1, 0x4, 0x8, 0xC)},

    /* GPIO_H2_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH2, 0x4, 0x8, 0xC)},
    /* GPIO_H2_1  */ {1, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH2, 0x4, 0x8, 0xC)},
    /* GPIO_H2_2  */ {2, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH2, 0x4, 0x8, 0xC)},
    /* GPIO_H2_3  */ {3, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH2, 0x4, 0x8, 0xC)},
    /* GPIO_H2_4  */ {4, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH2, 0x4, 0x8, 0xC)},
    /* GPIO_H2_5  */ {5, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPH2, 0x4, 0x8, 0xC)},

    // GPIO_CMGP ======================

    /* GPIO_M00_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM00, 0x4, 0x8, 0xC)},
    /* GPIO_M01_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM01, 0x4, 0x8, 0xC)},
    /* GPIO_M02_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM02, 0x4, 0x8, 0xC)},
    /* GPIO_M03_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM03, 0x4, 0x8, 0xC)},
    /* GPIO_M04_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM04, 0x4, 0x8, 0xC)},
    /* GPIO_M05_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM05, 0x4, 0x8, 0xC)},
    /* GPIO_M06_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM06, 0x4, 0x8, 0xC)},
    /* GPIO_M07_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM07, 0x4, 0x8, 0xC)},
    /* GPIO_M08_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM08, 0x4, 0x8, 0xC)},
    /* GPIO_M09_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM09, 0x4, 0x8, 0xC)},
    /* GPIO_M10_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM10, 0x4, 0x8, 0xC)},
    /* GPIO_M11_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM11, 0x4, 0x8, 0xC)},
    /* GPIO_M12_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM12, 0x4, 0x8, 0xC)},
    /* GPIO_M13_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM13, 0x4, 0x8, 0xC)},
    /* GPIO_M14_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM14, 0x4, 0x8, 0xC)},
    /* GPIO_M15_0  */ {0, 4, 1, 4, 4,
                        GPIO_REG_ADDR(GPM15, 0x4, 0x8, 0xC)},
};

#define    REG_GPIO_NWEINT_GPH0_CON        (GPIO_BASE_ADDRESS + 0x700)
#define    REG_GPIO_NWEINT_GPH1_CON        (GPIO_BASE_ADDRESS + 0x704)
#define    REG_GPIO_NWEINT_GPH2_CON        (GPIO_BASE_ADDRESS + 0x708)
#define    REG_GPIO_NWEINT_GPH0_FLTCON0    (GPIO_BASE_ADDRESS + 0x800)
#define    REG_GPIO_NWEINT_GPH0_FLTCON1    (GPIO_BASE_ADDRESS + 0x804)
#define    REG_GPIO_NWEINT_GPH1_FLTCON0    (GPIO_BASE_ADDRESS + 0x808)
#define    REG_GPIO_NWEINT_GPH2_FLTCON0    (GPIO_BASE_ADDRESS + 0x80C)
#define    REG_GPIO_NWEINT_GPH2_FLTCON1    (GPIO_BASE_ADDRESS + 0x810)
#define    REG_GPIO_NWEINT_GPH0_MASK       (GPIO_BASE_ADDRESS + 0x900)
#define    REG_GPIO_NWEINT_GPH1_MASK       (GPIO_BASE_ADDRESS + 0x904)
#define    REG_GPIO_NWEINT_GPH2_MASK       (GPIO_BASE_ADDRESS + 0x908)
#define    REG_GPIO_NWEINT_GPH0_PEND       (GPIO_BASE_ADDRESS + 0xA00)
#define    REG_GPIO_NWEINT_GPH1_PEND       (GPIO_BASE_ADDRESS + 0xA04)
#define    REG_GPIO_NWEINT_GPH2_PEND       (GPIO_BASE_ADDRESS + 0xA08)
#define    REG_GPIO_TZPC_CHUB_0            (GPIO_BASE_ADDRESS + 0x2000)
#define    REG_GPIO_TZPC_CHUB_1            (GPIO_BASE_ADDRESS + 0x2004)


#define    REG_GPIO_NWEINT_GPM00_CON       (GPIO_CMGP_BASE_ADDRESS + 0x700)
#define    REG_GPIO_NWEINT_GPM01_CON       (GPIO_CMGP_BASE_ADDRESS + 0x704)
#define    REG_GPIO_NWEINT_GPM02_CON       (GPIO_CMGP_BASE_ADDRESS + 0x708)
#define    REG_GPIO_NWEINT_GPM03_CON       (GPIO_CMGP_BASE_ADDRESS + 0x70C)
#define    REG_GPIO_NWEINT_GPM04_CON       (GPIO_CMGP_BASE_ADDRESS + 0x710)
#define    REG_GPIO_NWEINT_GPM05_CON       (GPIO_CMGP_BASE_ADDRESS + 0x714)
#define    REG_GPIO_NWEINT_GPM06_CON       (GPIO_CMGP_BASE_ADDRESS + 0x718)
#define    REG_GPIO_NWEINT_GPM07_CON       (GPIO_CMGP_BASE_ADDRESS + 0x71C)
#define    REG_GPIO_NWEINT_GPM08_CON       (GPIO_CMGP_BASE_ADDRESS + 0x720)
#define    REG_GPIO_NWEINT_GPM09_CON       (GPIO_CMGP_BASE_ADDRESS + 0x724)
#define    REG_GPIO_NWEINT_GPM10_CON       (GPIO_CMGP_BASE_ADDRESS + 0x728)
#define    REG_GPIO_NWEINT_GPM11_CON       (GPIO_CMGP_BASE_ADDRESS + 0x72C)
#define    REG_GPIO_NWEINT_GPM12_CON       (GPIO_CMGP_BASE_ADDRESS + 0x730)
#define    REG_GPIO_NWEINT_GPM13_CON       (GPIO_CMGP_BASE_ADDRESS + 0x734)
#define    REG_GPIO_NWEINT_GPM14_CON       (GPIO_CMGP_BASE_ADDRESS + 0x738)
#define    REG_GPIO_NWEINT_GPM15_CON       (GPIO_CMGP_BASE_ADDRESS + 0x73C)

#define    REG_GPIO_NWEINT_GPM00_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x800)
#define    REG_GPIO_NWEINT_GPM01_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x804)
#define    REG_GPIO_NWEINT_GPM02_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x808)
#define    REG_GPIO_NWEINT_GPM03_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x80C)
#define    REG_GPIO_NWEINT_GPM04_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x810)
#define    REG_GPIO_NWEINT_GPM05_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x814)
#define    REG_GPIO_NWEINT_GPM06_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x818)
#define    REG_GPIO_NWEINT_GPM07_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x81C)
#define    REG_GPIO_NWEINT_GPM08_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x820)
#define    REG_GPIO_NWEINT_GPM09_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x824)
#define    REG_GPIO_NWEINT_GPM10_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x828)
#define    REG_GPIO_NWEINT_GPM11_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x82C)
#define    REG_GPIO_NWEINT_GPM12_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x830)
#define    REG_GPIO_NWEINT_GPM13_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x834)
#define    REG_GPIO_NWEINT_GPM14_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x838)
#define    REG_GPIO_NWEINT_GPM15_FLTCON0   (GPIO_CMGP_BASE_ADDRESS + 0x83C)

#define    REG_GPIO_NWEINT_GPM00_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x900)
#define    REG_GPIO_NWEINT_GPM01_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x904)
#define    REG_GPIO_NWEINT_GPM02_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x908)
#define    REG_GPIO_NWEINT_GPM03_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x90C)
#define    REG_GPIO_NWEINT_GPM04_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x910)
#define    REG_GPIO_NWEINT_GPM05_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x914)
#define    REG_GPIO_NWEINT_GPM06_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x918)
#define    REG_GPIO_NWEINT_GPM07_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x91C)
#define    REG_GPIO_NWEINT_GPM08_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x920)
#define    REG_GPIO_NWEINT_GPM09_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x924)
#define    REG_GPIO_NWEINT_GPM10_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x928)
#define    REG_GPIO_NWEINT_GPM11_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x92C)
#define    REG_GPIO_NWEINT_GPM12_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x930)
#define    REG_GPIO_NWEINT_GPM13_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x934)
#define    REG_GPIO_NWEINT_GPM14_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x938)
#define    REG_GPIO_NWEINT_GPM15_MASK      (GPIO_CMGP_BASE_ADDRESS + 0x93C)

#define    REG_GPIO_NWEINT_GPM00_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA00)
#define    REG_GPIO_NWEINT_GPM01_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA04)
#define    REG_GPIO_NWEINT_GPM02_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA08)
#define    REG_GPIO_NWEINT_GPM03_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA0C)
#define    REG_GPIO_NWEINT_GPM04_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA10)
#define    REG_GPIO_NWEINT_GPM05_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA14)
#define    REG_GPIO_NWEINT_GPM06_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA18)
#define    REG_GPIO_NWEINT_GPM07_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA1C)
#define    REG_GPIO_NWEINT_GPM08_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA20)
#define    REG_GPIO_NWEINT_GPM09_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA24)
#define    REG_GPIO_NWEINT_GPM10_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA28)
#define    REG_GPIO_NWEINT_GPM11_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA2C)
#define    REG_GPIO_NWEINT_GPM12_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA30)
#define    REG_GPIO_NWEINT_GPM13_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA34)
#define    REG_GPIO_NWEINT_GPM14_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA38)
#define    REG_GPIO_NWEINT_GPM15_PEND      (GPIO_CMGP_BASE_ADDRESS + 0xA3C)
#define    REG_GPIO_TZPC_CMGP_0            (GPIO_CMGP_BASE_ADDRESS + 0x2000)
#define    REG_GPIO_TZPC_CMGP_1            (GPIO_CMGP_BASE_ADDRESS + 0x2004)
#define    REG_GPIO_TZPC_CMGP_2            (GPIO_CMGP_BASE_ADDRESS + 0x2008)
#define    REG_GPIO_TZPC_CMGP_3            (GPIO_CMGP_BASE_ADDRESS + 0x200C)
#define    REG_GPIO_TZPC_CMGP_4            (GPIO_CMGP_BASE_ADDRESS + 0x2010)
#define    REG_GPIO_TZPC_CMGP_5            (GPIO_CMGP_BASE_ADDRESS + 0x2014)
#define    REG_GPIO_TZPC_CMGP_6            (GPIO_CMGP_BASE_ADDRESS + 0x2018)
#define    REG_GPIO_TZPC_CMGP_7            (GPIO_CMGP_BASE_ADDRESS + 0x201C)


const GpioEintInfoType mGpioEintInfo[GPIO_MAX_EINT_NUM] = {

    /* NWEINT_GPH0_EINT_0  */ { 0,  0, 0, 0, GPIO_H0_0, GPIO_REG_NWEINT_ADDR(GPH0, 0)},
    /* NWEINT_GPH0_EINT_1  */ { 4,  8, 1, 1, GPIO_H0_1, GPIO_REG_NWEINT_ADDR(GPH0, 0)},
    /* NWEINT_GPH0_EINT_2  */ { 8, 16, 2, 2, GPIO_H0_2, GPIO_REG_NWEINT_ADDR(GPH0, 0)},
    /* NWEINT_GPH0_EINT_3  */ {12, 24, 3, 3, GPIO_H0_3, GPIO_REG_NWEINT_ADDR(GPH0, 0)},
    /* NWEINT_GPH0_EINT_4  */ {16,  0, 4, 4, GPIO_H0_4, GPIO_REG_NWEINT_ADDR(GPH0, 1)},
    /* NWEINT_GPH0_EINT_5  */ {20,  8, 5, 5, GPIO_H0_5, GPIO_REG_NWEINT_ADDR(GPH0, 1)},
    /* NWEINT_GPH0_EINT_6  */ {24, 16, 6, 6, GPIO_H0_6, GPIO_REG_NWEINT_ADDR(GPH0, 1)},
    /* NWEINT_GPH0_EINT_7  */ {28, 24, 7, 7, GPIO_H0_7, GPIO_REG_NWEINT_ADDR(GPH0, 1)},

    /* NWEINT_GPH1_EINT_0  */ { 0,  0, 0, 0, GPIO_H1_0, GPIO_REG_NWEINT_ADDR(GPH1, 0)},
    /* NWEINT_GPH1_EINT_1  */ { 4,  8, 1, 1, GPIO_H1_1, GPIO_REG_NWEINT_ADDR(GPH1, 0)},
    /* NWEINT_GPH1_EINT_2  */ { 8, 16, 2, 2, GPIO_H1_2, GPIO_REG_NWEINT_ADDR(GPH1, 0)},
    /* NWEINT_GPH1_EINT_3  */ {12, 24, 2, 2, GPIO_H1_3, GPIO_REG_NWEINT_ADDR(GPH1, 0)},

    /* NWEINT_GPH2_EINT_0  */ { 0,  0, 0, 0, GPIO_H2_0, GPIO_REG_NWEINT_ADDR(GPH2, 0)},
    /* NWEINT_GPH2_EINT_1  */ { 4,  8, 1, 1, GPIO_H2_1, GPIO_REG_NWEINT_ADDR(GPH2, 0)},
    /* NWEINT_GPH2_EINT_2  */ { 8, 16, 2, 2, GPIO_H2_2, GPIO_REG_NWEINT_ADDR(GPH2, 0)},
    /* NWEINT_GPH2_EINT_3  */ {12, 24, 3, 3, GPIO_H2_3, GPIO_REG_NWEINT_ADDR(GPH2, 0)},
    /* NWEINT_GPH2_EINT_4  */ {16,  0, 4, 4, GPIO_H2_4, GPIO_REG_NWEINT_ADDR(GPH2, 1)},
    /* NWEINT_GPH2_EINT_5  */ {20,  8, 5, 5, GPIO_H2_5, GPIO_REG_NWEINT_ADDR(GPH2, 1)},

    /* NWEINT_GPM00_EINT_0*/ { 0,  0, 0, 0, GPIO_M00_0, GPIO_REG_NWEINT_ADDR(GPM00,0)},
    /* NWEINT_GPM01_EINT_0*/ { 0,  0, 0, 0, GPIO_M01_0, GPIO_REG_NWEINT_ADDR(GPM01,0)},
    /* NWEINT_GPM02_EINT_0*/ { 0,  0, 0, 0, GPIO_M02_0, GPIO_REG_NWEINT_ADDR(GPM02,0)},
    /* NWEINT_GPM03_EINT_0*/ { 0,  0, 0, 0, GPIO_M03_0, GPIO_REG_NWEINT_ADDR(GPM03,0)},
    /* NWEINT_GPM04_EINT_0*/ { 0,  0, 0, 0, GPIO_M04_0, GPIO_REG_NWEINT_ADDR(GPM04,0)},
    /* NWEINT_GPM05_EINT_0*/ { 0,  0, 0, 0, GPIO_M05_0, GPIO_REG_NWEINT_ADDR(GPM05,0)},
    /* NWEINT_GPM06_EINT_0*/ { 0,  0, 0, 0, GPIO_M06_0, GPIO_REG_NWEINT_ADDR(GPM06,0)},
    /* NWEINT_GPM07_EINT_0*/ { 0,  0, 0, 0, GPIO_M07_0, GPIO_REG_NWEINT_ADDR(GPM07,0)},
    /* NWEINT_GPM08_EINT_0*/ { 0,  0, 0, 0, GPIO_M08_0, GPIO_REG_NWEINT_ADDR(GPM08,0)},
    /* NWEINT_GPM09_EINT_0*/ { 0,  0, 0, 0, GPIO_M09_0, GPIO_REG_NWEINT_ADDR(GPM09,0)},
    /* NWEINT_GPM10_EINT_0*/ { 0,  0, 0, 0, GPIO_M10_0, GPIO_REG_NWEINT_ADDR(GPM10,0)},
    /* NWEINT_GPM11_EINT_0*/ { 0,  0, 0, 0, GPIO_M11_0, GPIO_REG_NWEINT_ADDR(GPM11,0)},
    /* NWEINT_GPM12_EINT_0*/ { 0,  0, 0, 0, GPIO_M12_0, GPIO_REG_NWEINT_ADDR(GPM12,0)},
    /* NWEINT_GPM13_EINT_0*/ { 0,  0, 0, 0, GPIO_M13_0, GPIO_REG_NWEINT_ADDR(GPM13,0)},
    /* NWEINT_GPM14_EINT_0*/ { 0,  0, 0, 0, GPIO_M14_0, GPIO_REG_NWEINT_ADDR(GPM14,0)},
    /* NWEINT_GPM15_EINT_0*/ { 0,  0, 0, 0, GPIO_M15_0, GPIO_REG_NWEINT_ADDR(GPM15,0)},
};

#endif

