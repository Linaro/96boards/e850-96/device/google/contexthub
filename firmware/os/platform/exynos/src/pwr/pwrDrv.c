/*----------------------------------------------------------------------------
 *      Exynos SoC  -  PWR
 *----------------------------------------------------------------------------
 *      Name:    pwrDrv.c
 *      Purpose: To implement power modes
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <csp_common.h>
#include <cmsis.h>
#if defined(SEOS)
  #include <platform.h>
#endif
#include <cpu.h>
#include <pwrDrv.h>
#include <mailbox.h>
#include <pwm.h>
#include <pwmDrv.h>
#include <pwmOS.h>
#include <timer.h>
#include <rtc.h>
#include <rtcDrv.h>
#include <rtcOS.h>
#include <wdt.h>
#include <sysreg.h>
#if defined(MEASURE_CPU_UTILIZATION)
#include <cpu/barrier.h>
#endif
#if defined(LOCAL_POWERGATE)
#include <pwrDrvPwrGating.h>
#endif
#include <rtcOS.h>

uint32_t pwrApWakeLock=true;

void pwrDrvDisableApWakeLock(void)
{
   pwrApWakeLock = false;
}

void pwrDrvEnableApWakeLock(void)
{
    pwrApWakeLock = true;
}

uint32_t pwrDrvGetWakeLock(void)
{
    return pwrApWakeLock;
}

static void pwrDrvPrepareSleepSysTickEnabled(uint32_t data)
{
    uint64_t intState;
    intState = cpuIntsOff();

    // Prevent processor to enter deep sleep
    SCB->SCR &=~ SCB_SCR_SLEEPDEEP_Msk;

    (void)data;

    asm volatile ("wfi\n" "nop":::"memory");

#if defined(SYSTICK_DLL)
    // SysTick counts down to 0, which means SysTick gets expired and core wakes up by systick interrupt
    // Right after wakeup up from platSleep, timIntHandler in evtQueueDequeue can process software timer properly with updated os tick (mTimeAccumulated)
    if(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) {
        platSetOSTickVariable(TIME_OF_SYSTICK_IN_MS);
    } else {
        // Other interrupt wakes up core, therefore os tick (mTimeAccumulated) will be updated later when SysTick expires.
    }
#endif

    cpuIntsRestore(intState);
}

static void pwrDrvPrepareSleepSysTickDisabled(uint32_t data)
{
    uint64_t intState;
    intState = cpuIntsOff();

#if defined(SYSTICK_DLL)
    // Need to update the elapsed time of SysTick before disable it !!!!
    platSetOSTickVariable(platGetTicksSinceTheLastTick());
#endif

#if defined(SEOS)
    // Disable SysTick
    platStopSysTick();
#endif

    // Allow processor to enter deep sleep
    SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;

    // Start PWM Timer for wakeup source to serve next software timer.
    // !!! To Do -> need to check side effect on 64bit -> 32bit down scaling of remainingTime
    // CSP_PRINTF_INFO("pwrDrvPrepareSleepSysTickDisabled (%d)\n", (int)data);
    pwmSetTime(PWM_TIMER0, data);
    pwmStartTimer(PWM_TIMER0);

    asm volatile ("wfi\n" "nop":::"memory");

    // Update systick(mTimeAccumulated) and restart SysTick
    pwm0_IRQHandler_Process();

    pwmStopTimer(PWM_TIMER0);

    cpuIntsRestore(intState);
}

#if defined(LOCAL_POWERGATE)
#if defined(SEOS)
static void pwrDrvPrepareSleepPwrGating(uint32_t data)
#else
void pwrDrvPrepareSleepPwrGating(uint32_t data)
#endif

{
    uint64_t intState;
    uint32_t i;
    uint32_t intMask;

#if defined(SEOS)
    intState = cpuIntsOff();
#endif

#if defined(SYSTICK_DLL)
    // Need to update the elapsed time of SysTick before entering into pwr-gating !!!!
    platSetOSTickVariable(platGetTicksSinceTheLastTick());
#endif

    // Disable SysTick
#if defined(SEOS)
    platStopSysTick();
#endif

    // Allow processor to enter deep sleep
    SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;

#if defined(MBAPM_REQUIRED)
    mailboxSetPowerMode(1);
#endif


#if defined(RTC_REQUIRED)
    // Start RTC Tick1 for wakeup source to serve next software timer.
    rtcSetTickTime(0, 0, data, NULL, 0);
#endif

    // Save SFRs before they are lost
    pwrDrvSaveState();

    // Save SCB, NVIC, MPU
    pwrDrvSaveCorePeri();

    // To Do -> need to optimize to reduce the execution time for codes below ...
    for (i = 0; i < NUM_INTERRUPTS; i++) {
        NVIC_DisableIRQ(i);
        //NVIC_ClearPendingIRQ(i);
    }

    /* Set boot mode power gating */
    ipc_set_chub_bootmode(BOOTMODE_PWRGATING, ipc_get_chub_rtlogmode());

    // Save CPU GPRs and call wfi
    pwrDrvPwrGating();
    //asm volatile ("wfi\n" "nop":::"memory");

    // Restore SCB, NVIC, MPU
    pwrDrvRestoreCorePeri();

    // Restore SFRs
    pwrDrvRestoreState();

    sysregSetMaskIrq(intMask);

    // Will get back here from bl by APM to power-up CHUB

#if defined(RTC_REQUIRED)
    // Update OS Tick variable
#if defined(SYSTICK_DLL)
    platSetOSTickVariableAfterPwrGating();
#endif
#endif

    // Re-start SysTick
#if defined(SEOS)
    platStartSysTick();

    cpuIntsRestore(intState);
#endif
}
#endif

#if defined(SUPPORT_RCO_DIVIDING)
static void pwrDrvPrepareSleepRcoDividing(uint32_t data)
{
    uint64_t intState;
    uint32_t intStatReg, intMask;

    intState = cpuIntsOff();
    //CSP_PRINTF_INFO("intState:%llu\n", intState);

    intStatReg = NVIC->ISER[0];
    //CSP_PRINTF_INFO("intStatReg:%lu\n", intStatReg);

    //Disable All Interrupts
    NVIC->ICER[0] = 0xFFFFFFFF;

    //all interrupt mask
    intMask = sysregGetMaskIrq();

    //Mask Irq except APM
    sysregSetMaskIrq(0x3FDFFF);

    //if pending interrupt
    if(intStatReg & NVIC->ISPR[0]) {
        CSP_PRINTF_INFO("pending interrupt!\n");
        sysregClearMaskIrq(0x3FFFFF);
        sysregSetMaskIrq(intMask);

        NVIC->ISER[0] = intStatReg;
        cpuIntsRestore(intState);

        return;
    }

    //Clear All pending interrupt
    NVIC->ICPR[0] = 0xFFFFFFFF;

#if defined(SYSTICK_DLL)
    // Need to update the elapsed time of SysTick before disable it !!!!
    platSetOSTickVariable(platGetTicksSinceTheLastTick());
#endif

#if defined(SEOS)
    // Disable SysTick
    platStopSysTick();
#endif

    // Allow processor to enter deep sleep
    SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;

    // Send RCO divide to APM
    mailboxSetPowerMode(1);

    NVIC_ClearPendingIRQ(MB_APM_IRQn);

    rtcSetTickTime(0, 0, data, NULL, 0);
    NVIC_DisableIRQ(CHUB_RTC_TICK1_IRQn);

    // enable APM mailbox INT
    NVIC_EnableIRQ(MB_APM_IRQn);

    //Time TIC_1
    rtcDrvClearInt(1<<2);

    asm volatile ("wfi\n" "nop":::"memory");

#if defined(SEOS)
#if defined(SYSTICK_DLL)
    // Update OS Tick variable
    platSetOSTickVariableAfterSleep();
#endif

    // Re-start SysTick
    platStartSysTick();
#endif

    //SYSREQ_CHUB_HWACG_CM4_CLKREQ_MASK_IRQ
    sysregClearMaskIrq(0x3FFFFF);
    sysregSetMaskIrq(intMask);

    CSP_PRINTF_INFO("pending bit 0x%x\n", (unsigned int)NVIC->ISPR[0]);

    NVIC->ISER[0] = intStatReg;
    //CSP_PRINTF_INFO("intStatReg:%lu\n", NVIC->ISER[0]);
    cpuIntsRestore(intState);
}
#endif

typedef enum {
    PWR_SLEEP_MODE_CORE_CLOCK_GATING = 0,
    PWR_SLEEP_MODE_CORE_CLOCK_GATING_VARIABLE_SYSTICK,
#if defined(LOCAL_POWERGATE)
    PWR_SLEEP_MODE_PMU_PWR_GATING,
#endif
#if defined(SUPPORT_RCO_DIVIDING)
    PWR_SLEEP_MODE_CORE_CLOCK_GATING_RCO_DIVIDING,
#endif
    PWR_SLEEP_MODE_MAX
} PwrSleepModeType;

struct PwrSleepPrepareFncs {
    void (*prepare)(uint32_t);
    uint32_t data;
} static mPwrSleepPrepareFncs[] = {
    /* PWR_SLEEP_MODE_CORE_CLOCK_GATING */
    {
        .prepare = pwrDrvPrepareSleepSysTickEnabled,
        .data = 0
    },
    /* PWR_SLEEP_MODE_CORE_CLOCK_GATING_VARIABLE_SYSTICK */
    {
        .prepare = pwrDrvPrepareSleepSysTickDisabled,
        .data = 0
    },
#if defined(LOCAL_POWERGATE)
    /* PWR_SLEEP_PMU_PWR_GATING */
    {
        .prepare = pwrDrvPrepareSleepPwrGating,
        .data = 0
    }
#endif
#if defined(SUPPORT_RCO_DIVIDING)
    {
        .prepare = pwrDrvPrepareSleepRcoDividing,
        .data = 0
    }
#endif
};

#define TIME_TO_ENTER_VARIABLE_SYSTICK 1000000 // 1ms
//#define TIME_TO_ENTER_VARIABLE_SYSTICK 1000000000 // 1s
static PwrSleepModeType pwrDrvDecideSleepMode(void)
{
    // Till CTS timestamp failures get fixed, Variable Tick and Power gating will be disabled !!!!
    //return PWR_SLEEP_MODE_CORE_CLOCK_GATING;

    uint64_t nextTimeToWakeup;

#if defined(SEOS)
    nextTimeToWakeup = platGetNextWakeupTime();
#endif

    // Neither pending software timer nor event in queue exists, we enter into pwr gating
    if(nextTimeToWakeup == 0)
    {
        // Checking SleepReadyReq. APM may or may not allow us to enter power-gating
#if defined(LOCAL_POWERGATE)
#if defined(MBAPM_REQUIRED)
        if (mailboxGetPowerMode()) { // <= Will test in EVT1
#else
        if(!pwrDrvGetWakeLock()){
#endif
            mPwrSleepPrepareFncs[PWR_SLEEP_MODE_PMU_PWR_GATING].data = PWM_MAX_TIME_TO_EXPIRE_IN_MICROSECOND; // 300 seconds
            return PWR_SLEEP_MODE_PMU_PWR_GATING;
        }
        else
#endif
        {
#if defined(SUPPORT_RCO_DIVIDING)
            if(!pwrDrvGetWakeLock()){
                mPwrSleepPrepareFncs[PWR_SLEEP_MODE_CORE_CLOCK_GATING_RCO_DIVIDING].data = PWM_MAX_TIME_TO_EXPIRE_IN_MICROSECOND;
                return PWR_SLEEP_MODE_CORE_CLOCK_GATING_RCO_DIVIDING;
            }
#endif
            mPwrSleepPrepareFncs[PWR_SLEEP_MODE_CORE_CLOCK_GATING_VARIABLE_SYSTICK].data = PWM_MAX_TIME_TO_EXPIRE_IN_MICROSECOND; // 300 seconds
            return PWR_SLEEP_MODE_CORE_CLOCK_GATING_VARIABLE_SYSTICK;
        }
    }
    // Software timer will expire in some time later, we enter into clock gating with systick set to fire
    else if(nextTimeToWakeup <= TIME_TO_ENTER_VARIABLE_SYSTICK)
    {
        mPwrSleepPrepareFncs[PWR_SLEEP_MODE_CORE_CLOCK_GATING].data = nextTimeToWakeup; // not used
        return PWR_SLEEP_MODE_CORE_CLOCK_GATING;
    }
    // We enter into clock gating with sys tick disabled
    else
    {
#if defined(SUPPORT_RCO_DIVIDING)
        if(!pwrDrvGetWakeLock()){
            mPwrSleepPrepareFncs[PWR_SLEEP_MODE_CORE_CLOCK_GATING_RCO_DIVIDING].data = (uint32_t)nextTimeToWakeup / 1000;
            return PWR_SLEEP_MODE_CORE_CLOCK_GATING_RCO_DIVIDING;
        }
#endif
        mPwrSleepPrepareFncs[PWR_SLEEP_MODE_CORE_CLOCK_GATING_VARIABLE_SYSTICK].data = (uint32_t)nextTimeToWakeup / 1000;
        return PWR_SLEEP_MODE_CORE_CLOCK_GATING_VARIABLE_SYSTICK;
    }

    //return PWR_SLEEP_MODE_CORE_CLOCK_GATING;
}


#if defined(MEASURE_CPU_UTILIZATION)
static uint64_t timeToStayInIdle;
static uint32_t isMeasure = 0;

void pwrDrvStartCpuUtiliztion(void)
{
    timeToStayInIdle = 0;
    isMeasure = 1;
}

void pwrDrvRestartCpuUtiliztion(void)
{
    timeToStayInIdle = 0;
}

uint64_t pwrDrvGetCpuUtiliztion(void)
{
    return timeToStayInIdle;
}

void pwrDrvStopCpuUtiliztion(void)
{
    isMeasure = 0;
}

#if defined(SYSTICK_DLL)
// evtQueueDequeue disable irq. SysTick Handler is called later when cpuIntsRestore is called by evtQueueDequeue.
// Therefore, platGetTicks() is not proper to calculate cpu utilization. Instead, we use SysTick->COUNTFLAG to check Systick counted to 0.
static uint64_t pwrDrvGetTicks(void)
{
    uint64_t ret;
    uint32_t val;

    mem_reorder_barrier();

    ret = platGetOSTickVariable();
    val = SysTick->VAL;

    if(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) {
        ret += TIME_OF_SYSTICK_IN_MS;
    }

    mem_reorder_barrier();

    return platSystickTicksToNs(val) + ret;
}
#endif
#endif

static void pwrDrvPrepareSleep(PwrSleepModeType mode)
{
#if defined(MEASURE_CPU_UTILIZATION)
    uint64_t t1 = 0, t2 = 0;
    if(isMeasure)
    {
        t1 = platGetTicks();
        //CSP_PRINTF_INFO("t1 (%lld), STVAL (%ld)\n", t1, SysTick->VAL);
    }
#endif
    //SCB->ICSR = 1UL << 25;
    //NVIC_ClearPendingIRQ(SysTick_IRQn);

    //CSP_PRINTF_INFO("SCB->ICSR (0x%x), SysTick->CTRL (0x%x)\n", SCB->ICSR, SysTick->CTRL);

    mPwrSleepPrepareFncs[mode].prepare(mPwrSleepPrepareFncs[mode].data);

#if defined(MEASURE_CPU_UTILIZATION)
    if(isMeasure)
    {
        uint64_t tt;
#if defined(SYSTICK_DLL)
        t2 = pwrDrvGetTicks();
#elif defined(SYSTICK_RTC)
        t2 = platGetTicks();
#else
#error
#endif
        //CSP_PRINTF_INFO("t2 (%lld), STVAL (%ld)\n", t2, SysTick->VAL);
        //CSP_PRINTF_INFO("SCB->ICSR (0x%x), SysTick->CTRL (0x%x)\n", SCB->ICSR, SysTick->CTRL);
        tt = (t2 - t1);
        timeToStayInIdle += tt;
    }
#endif
}

static void pwrDrvPostSleep(void)
{
    // Do something if needed
}

void pwrDrvSleep(void)
{
#ifdef EXYNOS_CONTEXTHUB
    wdtDisableClk();
#endif
    pwrDrvPrepareSleep(pwrDrvDecideSleepMode());
    pwrDrvPostSleep();
#ifdef EXYNOS_CONTEXTHUB
    wdtSetTime(WDT_DEFAULT_TIMEOUT);
    wdtEnableClk();
#endif

}
