/*----------------------------------------------------------------------------
 *      Exynos SoC  -  SYSREG
 *----------------------------------------------------------------------------
 *      Name:    sysregDrv9610.c
 *      Purpose: To implement SYSREG driver functions
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <sysreg.h>
#include <sysregDrv.h>

#define SYSREG_CHUB_DRCG_ENABLE	    0x7F
#define SYSREG_CHUB_DRCG_DISABLE    0x0
#define SYSREQ_CHUB_HWACG_CM4_CLKREQ_MASK_IRQ   0x1fffffff

static const volatile uint32_t * mSWCONFTable[SYSREG_SWCONF_PORT_MAX] = {
    /* SWCONF_USI_CHUB00_SW_CONF */ (volatile uint32_t *)REG_SYSREG_USI_CHUB00_SW_CONF,
    /* SWCONF_I2C_CHUB00_SW_CONF */ (volatile uint32_t *)REG_SYSREG_I2C_CHUB00_SW_CONF,
    /* SWCONF_USI_CMGP00_SW_CONF */ (volatile uint32_t *)REG_SYSREG_USI_CMGP00_SW_CONF,
    /* SWCONF_I2C_CMGP00_SW_CONF */ (volatile uint32_t *)REG_SYSREG_I2C_CMGP00_SW_CONF,
    /* SWCONF_USI_CMGP01_SW_CONF */ (volatile uint32_t *)REG_SYSREG_USI_CMGP01_SW_CONF,
    /* SWCONF_I2C_CMGP01_SW_CONF */ (volatile uint32_t *)REG_SYSREG_I2C_CMGP01_SW_CONF,
    /* SWCONF_USI_CMGP02_SW_CONF */ (volatile uint32_t *)REG_SYSREG_USI_CMGP02_SW_CONF,
    /* SWCONF_I2C_CMGP02_SW_CONF */ (volatile uint32_t *)REG_SYSREG_I2C_CMGP02_SW_CONF,
    /* SWCONF_USI_CMGP03_SW_CONF */ (volatile uint32_t *)REG_SYSREG_USI_CMGP03_SW_CONF,
    /* SWCONF_I2C_CMGP03_SW_CONF */ (volatile uint32_t *)REG_SYSREG_I2C_CMGP03_SW_CONF,
    /* SWCONF_USI_CMGP04_SW_CONF */ (volatile uint32_t *)REG_SYSREG_USI_CMGP04_SW_CONF,
    /* SWCONF_I2C_CMGP04_SW_CONF */ (volatile uint32_t *)REG_SYSREG_I2C_CMGP04_SW_CONF,
};


//
void sysregDrvSetSwConf(IN SysregSwConfPortType port, IN SysregSwConfProtocolType protocol)
{
    __raw_writel((uint32_t)protocol, mSWCONFTable[port]);
}

//
void sysregDrvSetHWACG(IN uint32_t enable)
{
    if(enable) {
        __raw_writel((uint32_t)SYSREG_CHUB_DRCG_ENABLE, (volatile uint32_t *)REG_SYSREG_BUS_COMPONENET_DRCG_EN);
    }
    else {
        __raw_writel((uint32_t)SYSREG_CHUB_DRCG_DISABLE, (volatile uint32_t *)REG_SYSREG_BUS_COMPONENET_DRCG_EN);
    }
}

void sysregDrvSetClkreqMaskIrq(IN uint32_t irq)
{
    uint32_t reg;

    reg = __raw_readl((volatile uint32_t *)REG_SYSREG_HWACG_CM4_CLKREQ);
    reg |= irq&SYSREQ_CHUB_HWACG_CM4_CLKREQ_MASK_IRQ;
    __raw_writel((uint32_t)reg, (volatile uint32_t *)REG_SYSREG_HWACG_CM4_CLKREQ);
}

void sysregDrvClearClkreqMaskIrq(IN uint32_t irq)
{
    uint32_t reg;

    reg = __raw_readl((volatile uint32_t *)REG_SYSREG_HWACG_CM4_CLKREQ);
    reg = (reg & ~SYSREQ_CHUB_HWACG_CM4_CLKREQ_MASK_IRQ);
    reg = reg | (SYSREQ_CHUB_HWACG_CM4_CLKREQ_MASK_IRQ & ~irq);
    __raw_writel((uint32_t)reg, (volatile uint32_t *)REG_SYSREG_HWACG_CM4_CLKREQ);
}

uint32_t sysregDrvGetClkreqMaskIrq(void)
{
    uint32_t reg;

    reg = __raw_readl((volatile uint32_t *)REG_SYSREG_HWACG_CM4_CLKREQ);
    reg &= SYSREQ_CHUB_HWACG_CM4_CLKREQ_MASK_IRQ;

    return reg;
}

void sysregDrvSetOscEn(void)
{
}

void sysregDrvSetOscDis(void)
{
}

void sysregDrvSaveState(void)
{
}

void sysregDrvRestoreState(void)
{
}

