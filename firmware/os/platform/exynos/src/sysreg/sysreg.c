/*----------------------------------------------------------------------------
 *      Exynos SoC  -  SYSREG
 *----------------------------------------------------------------------------
 *      Name:    sysreg.c
 *      Purpose: To implement SYSREG APIs
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <sysreg.h>
#include <sysregDrv.h>

//
void sysregSetSwConf(IN SysregSwConfPortType port, IN SysregSwConfProtocolType protocol)
{
    // Check if port and protocol are valid
    if (port >= SYSREG_SWCONF_PORT_MAX) {
        CSP_ASSERT(0);
        return;
    }
    if (protocol >= SYSREG_SWCONF_PROTOCOL_MAX) {
        CSP_ASSERT(0);
        return;
    }

    sysregDrvSetSwConf(port, protocol);
}

//Public API to mask IRQs for CHUB CM4 clock request
void sysregSetMaskIrq(IN uint32_t irq)
{
     sysregDrvSetClkreqMaskIrq(irq);
}

void sysregClearMaskIrq(IN uint32_t irq)
{
     sysregDrvClearClkreqMaskIrq(irq);
}

uint32_t sysregGetMaskIrq(void)
{
     return sysregDrvGetClkreqMaskIrq();
}

void sysregSetOscEn(void)
{
    sysregDrvSetOscEn();
}

void sysregSetOscDis(void)
{
    sysregDrvSetOscDis();
}

#include CSP_SOURCE(sysreg)
