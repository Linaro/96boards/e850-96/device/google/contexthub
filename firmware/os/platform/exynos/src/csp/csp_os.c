/*----------------------------------------------------------------------------
 *      Exynos SoC  -  CSP
 *----------------------------------------------------------------------------
 *      Name:    csp_os.c
 *      Purpose: To implement os api
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <csp_common.h>
#ifdef SEOS
#include <heap.h>
#include <timer.h>
#include <mailbox.h>
#else
#include <rtos.h>
#endif
#include <csp_os.h>
#include <cmu.h>

void *cspHeapAlloc(uint32_t sz)
{
#ifdef SEOS
    return heapAlloc(sz);
#else
    return OS_malloc(sz);
#endif
}

void *cspHeapRealloc(void *ptr, uint32_t newSz)
{
#ifdef SEOS
    // Not supported.
    // Caller should allocate new sized memory chunk, move contents to new memory chunk, and then free old memory
    return NULL;
#else
    return OS_realloc(ptr, newSz);
#endif
}

void cspHeapFree(void *ptr)
{
#ifdef SEOS
    heapFree(ptr);
#else
    OS_free(ptr);
#endif
}

/* times */
uint64_t cspTimeGetTime(void)
{
#ifdef SEOS
    return timGetTime();
#else
    return 0;
#endif
}

uint32_t cspTimeSetTime(uint64_t length, uint32_t jitterPpm, uint32_t driftPpm,
                        void *cookie, bool oneShot)
{
#ifdef SEOS
    return timTimerSet(length, jitterPpm, driftPpm, 0, cookie, oneShot);
#else
    (void)length; // To avoid compiler complaint
    (void)jitterPpm;
    (void)driftPpm;
    (void)cookie;
    (void)oneShot;
    return 0;
#endif
}

bool cspTimeCancelTime(uint32_t timerId)
{
#ifdef SEOS
    return timTimerCancel(timerId);
#else
    (void)timerId; // To avoid compiler complaint
    return 0;
#endif
}

void uSleep(uint32_t us)
{
    uint32_t unit = cmuGetSpeed(CMU_CLK_OUTPUT_CPU) / 5000000;
    volatile uint32_t time = us * unit;
    while(time--);
}

void mSleep(uint32_t ms)
{
    volatile uint32_t time = ms * (cmuGetSpeed(CMU_CLK_OUTPUT_CPU) / 5000)/*unit*/;
    while (time--);
}

void cspNotify(enum error_type err)
{
#ifdef SEOS
	if (err == ERR_ASSERT)
		mailboxDrvWriteEvent(MAILBOX_EVT_DEBUG, IPC_DEBUG_CHUB_ASSERT);
	else if (err == ERR_FAULT)
		mailboxDrvWriteEvent(MAILBOX_EVT_DEBUG, IPC_DEBUG_CHUB_FAULT);
	else if (err == ERR_ERROR)
		mailboxDrvWriteEvent(MAILBOX_EVT_DEBUG, IPC_DEBUG_CHUB_ERROR);
	else if (err == ERR_REBOOT) {
		ipc_hw_write_shared_reg(AP, CHUB_REBOOT_REQ, SR_3);
		ipc_hw_gen_interrupt(AP, IRQ_EVT_CHUB_ALIVE);
	} else if (err == ERR_PANIC) {
	    int *hanger = NULL;

            CSP_PRINTF_INFO("%s: killing chub...\n", __func__);
            *hanger = -1;
            CSP_PRINTF_INFO("%s: not killed chub...\n", __func__);
	}
#endif
}

#ifdef USE_CSP_SNAPSHOT
#include <rtc.h>
#define CSP_LOQ_QUEUE_MAX (80)

struct csp_log_s {
	uint64_t time[CSP_LOQ_QUEUE_MAX];
	int index;
};

struct csp_log_s csp_log[csp_log_max];

void cspSetLogging(enum csp_log_id id)
{
	struct csp_log_s *log = &csp_log[id];

	if (log->index > CSP_LOQ_QUEUE_MAX)
		CSP_PRINTF_ERROR("%s: error : index:%d\n", __func__, log->index);
	else {
		log->time[log->index] = rtcGetTimeStampNS();
		log->index = (log->index + 1) % CSP_LOQ_QUEUE_MAX;
	}
}

void cspShowLogging(enum csp_log_id id)
{
	struct csp_log_s *log = &csp_log[id];
	int i;

	if (log->index > CSP_LOQ_QUEUE_MAX)
		CSP_PRINTF_ERROR("%s: error : index:%d\n", __func__, log->index);
	else {
		CSP_PRINTF_INFO("%s: id:%d, index:%d\n", __func__, id, log->index);
		for (i = log->index; i < CSP_LOQ_QUEUE_MAX; i++)
			CSP_PRINTF_INFO("[%d-%2d] %lld\n", id, i, log->time[i]);
		for (i = 0; i < log->index; i++)
			CSP_PRINTF_INFO("[%d-%2d] %lld\n", id, i, log->time[i]);
	}
}
#endif
