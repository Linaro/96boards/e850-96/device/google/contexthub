/*----------------------------------------------------------------------------
 *      Exynos SoC  -  CPU MPU
 *----------------------------------------------------------------------------
 *      Name:    mpu.c
 *      Purpose: To implement MPU driver
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <csp_common.h>
#include <mpu.h>
#if defined(SEOS)
#include <cmsis.h>
#endif

// region id - Cortex-M4 MPU has 8 regions and a background region. The highest number prioritize over lower one
#define MPU_REGION_0    0
#define MPU_REGION_1    1
#define MPU_REGION_2    2
#define MPU_REGION_3    3
#define MPU_REGION_4    4
#define MPU_REGION_5    5
#define MPU_REGION_6    6
#define MPU_REGION_7    7

// region executable atrribute
#define MPU_NOT_EXECUTABLE (1UL << 28) /* no execute */

// region accessible attribute
#define MPU_AP_NA          (0UL << 24) /* Privileged: no access   Unprivileged: no access */
#define MPU_AP_U_NA_P_RW   (1UL << 24) /* Privileged: RW          Unprivileged: no access */
#define MPU_AP_U_RO_P_RW   (2UL << 24) /* Privileged: RW          Unprivileged RO        */
#define MPU_AP_RW          (3UL << 24) /* Privileged: RW          Unprivileged: RW        */
#define MPU_AP_U_NA_P_RO   (5UL << 24) /* Privileged: RO          Unprivileged: no access */
#define MPU_AP_U_RO_P_RO   (6UL << 24) /* Privileged: RO          Unprivileged: RO        */

// region size attribute
#define MPU_SIZE_32B    (4UL << 1)
#define MPU_SIZE_1KB    (9UL << 1)
#define MPU_SIZE_1MB    (19UL << 1)
#define MPU_SIZE_1GB    (29UL << 1)
#define MPU_SIZE_4GB    (31UL << 1)

// region enable bit
#define MPU_REGION_ENABLE  1UL

// subregion disabled bits
#define MPU_SUBREGION_DISABLE    0xFF00UL

// TEX(19), C(17), B(16), S(18)
#define MPU_SO_TEX_C_B_NS       ((0UL << 19) | (0UL << 17) | (0UL << 16) | (0 << 18)) /* Strongly ordered */
#define MPU_DEVICE_TEX_C_B_NS   ((2UL << 19) | (0UL << 17) | (0UL << 16) | (0 << 18)) /* Device */
#define MPU_DEVICE_TEX_C_B_S   ((0UL << 19) | (0UL << 17) | (1UL << 16) | (1 << 18)) /* Device, shareable */
#define MPU_NORMAL_WT_NOWA_TEX_C_B_NS   ((0UL << 19) | (1UL << 17) | (0UL << 16) | (0 << 18)) /* Normal, Write-through, No write-alloc */
#define MPU_NORMAL_WT_NOWA_TEX_C_B_S   ((0UL << 19) | (1UL << 17) | (0UL << 16) | (1 << 18)) /* Normal, Write-through, No write-alloc, shareable */
#define MPU_NORMAL_WB_NOWA_TEX_C_B_NS   ((0UL << 19) | (1UL << 17) | (1UL << 16) | (0 << 18)) /* Normal, Write-back, No write-alloc */
#define MPU_NORMAL_NC_TEX_C_B_NS   ((1UL << 19) | (0UL << 17) | (0UL << 16) | (0 << 18)) /* Normal, noncacheable */
#define MPU_NORMAL_WB_WA_TEX_C_B_NS   ((1UL << 19) | (1UL << 17) | (1UL << 16) | (0 << 18)) /* Normal, Write-back, Write-alloc */
#define MPU_NORMAL_WB_WA_TEX_C_B_S   ((1UL << 19) | (1UL << 17) | (1UL << 16) | (1 << 18)) /* Normal, Write-back, Write-alloc, shareable */

typedef struct {
    uint32_t region_id;
    uint32_t region_baddr;
    uint32_t region_attr;
    uint32_t region_size;
    uint32_t valid;
} MpuRegionType;


#include CSP_SOURCE(mpu)

// MPU control
#define MPU_ENABLE                    (1UL << 0)
#define MPU_ENABLE_DURING_HF_NMI      (1UL << 1)
#define MPU_ENABLE_DEFAULT_MEM_MAP    (1UL << 2)
#define MPU_CONTROL_ENABLE            MPU_ENABLE | MPU_ENABLE_DURING_HF_NMI | MPU_ENABLE_DEFAULT_MEM_MAP /* MPU enabled during hard fault */
#define MPU_CONTROL_DISABLE           0

static void mpuSetupRegions(void)
{
    uint32_t i;

    for(i = 0; i < sizeof(regions) / sizeof(regions[0]); i++) {
        if(regions[i].valid) {
            uint32_t attr;

            // Select region
            MPU->RNR = 0xFF & regions[i].region_id;

            attr = regions[i].region_attr | regions[i].region_size;
            // Disable region
            MPU->RASR = attr & 0xFFFFFFFE;

            // Set base address
            MPU->RBAR = 0xFFFFFFE0 & regions[i].region_baddr;

            // Enable region
            MPU->RASR = attr | MPU_REGION_ENABLE;
        }
    }
}

static void mpuEnable(void)
{
    MPU->CTRL = MPU_CONTROL_ENABLE;
}

/*
static void mpuDisable(void)
{
    MPU->CTRL = MPU_CONTROL_DISABLE;
}
*/

// Setup regions and start MPU
void mpuStart(void)
{
    __disable_irq();

    __ISB();
	__DSB();
	__DMB();

    mpuSetupRegions();

    __ISB();
    __DSB();
    __DMB();

    mpuEnable();

    __enable_irq();
}

//
void mpuAllowRamExecution(bool allowSvcExecute)
{
    return;
}

//
void mpuAllowRomWrite(bool allowSvcWrite)
{
    return;
}

