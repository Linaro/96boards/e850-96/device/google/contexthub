/*----------------------------------------------------------------------------
 *      Exynos SoC  -  CPU Utilization
 *----------------------------------------------------------------------------
 *      Name:    cpuUtilization.c
 *      Purpose: To measure CPU utilization for a given period time, which is configurable
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if defined(MEASURE_CPU_UTILIZATION)

#include <cpuUtilization.h>
#include <pwrDrv.h>

// Thread ID
static uint32_t mTid;

void cpuUtilizationCommand(uint32_t cmd, void* cookie)
{
    osEnqueuePrivateEvt(cmd, cookie, NULL, mTid);
}

// Time period (in second) to mesaure
#define UTILIZATION_MEASURE_PERIOD 1

// Timer ID
static uint32_t cpuUtilizationTimerId = 0;

static void cpuUtilizationTimerCb(uint32_t timerId, void *cookie)
{
    CSP_PRINTF_INFO("CPU idle time (%lld ns) for 1 second\n", pwrDrvGetCpuUtiliztion());
    pwrDrvRestartCpuUtiliztion();
}

static void cpuUtilizationHandleEvent(uint32_t evtType, const void* evtData)
{
    switch (evtType) {
        case EVT_APP_START:
        break;

        case EVT_UTILIZATION_EVENT_START:
            if(cpuUtilizationTimerId != 0) {
                CSP_PRINTF_INFO("Measurement of cpu utilization already started... \n");
                break;
            }

            CSP_PRINTF_INFO("Measurement of cpu utilization starts... every %d secs \n", UTILIZATION_MEASURE_PERIOD);
            cpuUtilizationTimerId = timTimerSet((uint64_t)UTILIZATION_MEASURE_PERIOD * 1000000000, 0, 0, cpuUtilizationTimerCb, NULL, false);
            if(cpuUtilizationTimerId == 0) {
                CSP_PRINTF_ERROR("[%s], no free timer is available\n", __func__);
                CSP_ASSERT(0);
            }

            pwrDrvStartCpuUtiliztion();
        break;

        case EVT_UTILIZATION_EVENT_STOP:
	    if (cpuUtilizationTimerId) {
                timTimerCancel(cpuUtilizationTimerId);
                cpuUtilizationTimerId = 0;
                pwrDrvStopCpuUtiliztion();
                CSP_PRINTF_INFO("Measurement of cpu utilization stops... \n");
	    }
        break;

        default:
            CSP_PRINTF_ERROR("[%s], Unknown command (%u)\n", __func__, (int)evtType);
        break;
    }
}

static bool cpuUtilizationTaskStart(uint32_t taskId)
{
    mTid = taskId;

    /* Kicking UTC Task to start */
    //osEventSubscribe(taskId, EVT_APP_START);

    CSP_PRINTF_INFO("cpuUtilizationTask starts... \n");

    return true;
}

static void cpuUtilizationTaskEnd(void)
{

}

INTERNAL_APP_INIT(APP_ID_MAKE(NANOHUB_VENDOR_EXYNOS, 1), 0, cpuUtilizationTaskStart, cpuUtilizationTaskEnd, cpuUtilizationHandleEvent);

#endif

