/*
 * Copyright (C) 2017 Samsung Electronics Co., Ltd.
 *
 * Contact: Boojin Kim <boojin.kim@samsung.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <mailboxOS.h>
#include <mailboxDrv.h>
#include <mailboxApmDrv.h>
#if defined(MBGNSS_REQUIRED)
#include <mailboxGnssDrv.h>
#endif
#include <csp_common.h>
#include <csp_os.h>
#include <csp_printf.h>
#include <csp_assert.h>
#include <chub_ipc.h>
#include <pwrDrv.h>
#include <sysreg.h>
#if defined(UTC_REQUIRED)
#include <exynos_test.h>
#endif

#if defined(SEOS)
#include <platform.h>
#include <nanohubCommand.h>
#include <hostIntf.h>
#include <cmsis.h>
#include <rtc.h>
#include <rtcOS.h>
#include <timer.h>
#include <algos/ap_hub_sync.h>
#if defined(MEASURE_CPU_UTILIZATION)
#include <cpuUtilization.h>
#endif
#if defined(HEAP_DEBUG)
#include <heapDebug.h>
#endif
#endif

#if defined(SEOS)
extern uint8_t __bl_start[];
#endif

#ifdef LOWLEVEL_DEBUG
#define DEBUG_PRINT(lv, fmt, ...)	\
	((DEBUG_LEVEL == (0)) ? (CSP_PRINTF_INFO(fmt, ##__VA_ARGS__)) : \
	((DEBUG_LEVEL == (lv)) ? (CSP_PRINTF_INFO(fmt, ##__VA_ARGS__)) : (NULL)))
#else
#define DEBUG_PRINT(level, fmt, ...)
#endif


static void ipc_hang_test(enum ipc_debug_event event)
{
	struct ipc_evt *ipc_evt_c2a = ipc_get_base(IPC_REG_IPC_EVT_C2A);
	struct ipc_evt *ipc_evt_a2c = ipc_get_base(IPC_REG_IPC_EVT_A2C);
	struct ipc_buf *ipc_buf_c2a = ipc_get_base(IPC_REG_IPC_C2A);
	struct ipc_buf *ipc_buf_a2c = ipc_get_base(IPC_REG_IPC_A2C);
	struct ipc_logbuf *logbuf = ipc_get_base(IPC_REG_LOG);
	char tmpbuf[16];
	int val;

	CSP_PRINTF_INFO("%s: with %d\n", __func__, event);
	msleep(100);
	switch (event) {
		case IPC_DEBUG_UTC_HANG_IPC_C2A_FULL:
			ipc_evt_c2a->ctrl.eq = 3;
			ipc_evt_c2a->ctrl.dq = 4;
			CSP_PRINTF_INFO("%s: c2a ipcfull. sent event\n", __func__);
			val = ipc_add_evt(IPC_EVT_C2A, IRQ_EVT_C2A_DEBUG);
			CSP_PRINTF_INFO("%s: c2a send evt ipcfull: val:%d\n", __func__, val);
			break;
		case IPC_DEBUG_UTC_HANG_IPC_C2A_CRASH:
			ipc_evt_c2a->ctrl.eq = 1000;
			ipc_evt_c2a->ctrl.dq = 4;
			CSP_PRINTF_INFO("%s: c2a flash. sent event\n", __func__);
			val = ipc_add_evt(IPC_EVT_C2A, IRQ_EVT_C2A_DEBUG);
			CSP_PRINTF_INFO("%s: c2a send evt ipcfull: val:%d\n", __func__, val);
			break;
		case IPC_DEBUG_UTC_HANG_IPC_C2A_DATA_FULL:
			ipc_buf_c2a->eq = 3;
			ipc_buf_c2a->dq = 4;
			CSP_PRINTF_INFO("%s: ipc_buf c2a full. sent data\n", __func__);
			ipc_write_data(IPC_DATA_C2A, tmpbuf, sizeof(tmpbuf));
			break;
		case IPC_DEBUG_UTC_HANG_IPC_C2A_DATA_CRASH:
			ipc_buf_c2a->eq = 3;
			ipc_buf_c2a->dq = 1000;
			CSP_PRINTF_INFO("%s: ipc_buf c2a crash (dq:1000). sent data\n", __func__);
			ipc_write_data(IPC_DATA_C2A, tmpbuf, sizeof(tmpbuf));
			break;
		case IPC_DEBUG_UTC_HANG_IPC_A2C_FULL:
			CSP_PRINTF_INFO("%s: a2c ipc full\n", __func__);
			ipc_evt_a2c->ctrl.eq = 3;
			ipc_evt_a2c->ctrl.dq = 4;
			break;
		case IPC_DEBUG_UTC_HANG_IPC_A2C_CRASH:
			CSP_PRINTF_INFO("%s: a2c ipc crash\n", __func__);
			ipc_evt_a2c->ctrl.eq = 3;
			ipc_evt_a2c->ctrl.dq = 1000;
			break;
		case IPC_DEBUG_UTC_HANG_IPC_A2C_DATA_FULL:
			CSP_PRINTF_INFO("%s: a2c data ipc full\n", __func__);
			ipc_buf_a2c->eq = 3;
			ipc_buf_a2c->dq = 4;
			break;
		case IPC_DEBUG_UTC_HANG_IPC_A2C_DATA_CRASH:
			CSP_PRINTF_INFO("%s: a2c data ipc crash\n", __func__);
			ipc_buf_a2c->eq = 3;
			ipc_buf_a2c->dq = 1000;
			break;
		case IPC_DEBUG_UTC_HANG_IPC_LOGBUF_EQ_CRASH:
			CSP_PRINTF_INFO("%s:EQ_CRASH\n", __func__);
			logbuf->eq = 0x10000;
			CSP_PRINTF_INFO("%s:EQ_CRASH: %d, %d\n", __func__, logbuf->eq, logbuf->dq);
			break;
		case IPC_DEBUG_UTC_HANG_IPC_LOGBUF_DQ_CRASH:
			CSP_PRINTF_INFO("%s: DQ_CRASH\n", __func__);
			logbuf->dq = 0x10000;
			CSP_PRINTF_INFO("%s:DQ_CRASH: %d, %d\n", __func__, logbuf->eq, logbuf->dq);
			break;
		case IPC_DEBUG_UTC_HANG_INVAL_INT:
			CSP_PRINTF_INFO("%s: invalid interrupt generation to AP. clear c2a evt\n", __func__);
			memset(ipc_evt_c2a->data, 0, sizeof(ipc_evt_c2a->data));
			ipc_hw_gen_interrupt_all(AP);
			break;
		default:
			break;
	}
}

static void handleDebug(void)
{
    enum ipc_debug_event event = (enum ipc_debug_event)ipc_read_debug_event(AP);
    static bool flag = 0;
    u32 val;

    CSP_PRINTF_INFO("%s: event:%d\n", __func__, event);
    switch (event) {
        case IPC_DEBUG_UTC_IPC_TEST_START:
        case IPC_DEBUG_UTC_IPC_TEST_END:
            mailboxDrvSetLoopbackTestMode(event);
            return;
        case IPC_DEBUG_UTC_SENSOR_CHIPID:
            val = getSensorId(ipc_read_debug_val(IPC_DATA_A2C));
            CSP_PRINTF_INFO("%s: get chipid: 0x%x for %d sensor \n", __func__, val,
                            (int)ipc_read_debug_val(IPC_DATA_A2C));
            ipc_write_debug_val(IPC_DATA_C2A, val);
            return;
        case IPC_DEBUG_DUMP_STATUS:
            CSP_PRINTF_INFO("%s: dump ipc\n", __func__);
            ipc_dump();
            return;
        default:
            break;
    }

#if defined(UTC_REQUIRED)
    switch (event) {
    case IPC_DEBUG_UTC_STOP:
#if defined(MEASURE_CPU_UTILIZATION)
        cpuUtilizationCommand(EVT_UTILIZATION_EVENT_STOP, NULL);
#endif
        utcCommand(UTC_COMMAND_STOP);
        break;
    case IPC_DEBUG_UTC_CHECK_CPU_UTIL:
#if defined(MEASURE_CPU_UTILIZATION)
        cpuUtilizationCommand(EVT_UTILIZATION_EVENT_START, NULL);
#endif
        break;
    case IPC_DEBUG_UTC_HEAP_DEBUG:
#if defined(HEAP_DEBUG)
        heapHistoryPrint();
#endif
        break;
    case IPC_DEBUG_UTC_WDT:
        utcCommand(UTC_COMMAND_WDT);
        break;
    case IPC_DEBUG_UTC_MEM:
        utcCommand(UTC_COMMAND_MEM);
        break;
    case IPC_DEBUG_UTC_TIMER:
        utcCommand(UTC_COMMAND_TIMER);
        break;
    case IPC_DEBUG_UTC_GPIO:
        utcCommand(UTC_COMMAND_GPIO);
        break;
    case IPC_DEBUG_UTC_SPI:
        utcCommand(UTC_COMMAND_SPI);
        break;
    case IPC_DEBUG_UTC_CMU:
        utcCommand(UTC_COMMAND_CMU);
        break;
    case IPC_DEBUG_UTC_TIME_SYNC:
        utcCommand(UTC_COMMAND_TIMESYNC);
        break;
    case IPC_DEBUG_UTC_RTC:
        if (!flag) {
            CSP_PRINTF_INFO("%s: RTC TEST START\n", __func__);
            rtcSetHandler(RtcTickId1, (void *)rtc_callback);
            rtcSetTickTime(1, 0, 0, NULL, 1);
            flag = 1;
        } else {
            CSP_PRINTF_INFO("%s: RTC TEST %llu END\n", __func__,
                            rtcGetTickTime());
            rtcStopTick();
            flag = 0;
        }
        break;
    case IPC_DEBUG_UTC_HANG:
        CSP_PANIC(0);
        break;
    case IPC_DEBUG_UTC_AGING:
        utcCommand(UTC_COMMAND_AGING);
        break;
    case IPC_DEBUG_UTC_FAULT:
        CSP_FAULT(0);
        break;
    case IPC_DEBUG_UTC_ASSERT:
        CSP_ASSERT(0);
        break;
    case IPC_DEBUG_UTC_CHECK_STATUS:
        utcCommand(UTC_COMMAND_STATUS);
        break;
    default:
        break;
    }
#else
    switch (event) {
    case IPC_DEBUG_UTC_RTC:
        if (!flag) {
            CSP_PRINTF_INFO("%s: RTC TEST START: with 100ms\n", __func__);
            rtcSetHandler(RtcTickId1, (void *)rtc_callback);
            rtcSetTickTime(0, 100, 0, NULL, 1);
           flag = 1;
        } else {
            CSP_PRINTF_INFO("%s: RTC TEST START: with 1ms\n", __func__);
            rtcSetHandler(RtcTickId1, (void *)rtc_callback);
            rtcSetTickTime(0, 1, 0, NULL, 1);
            flag = 0;
        }
        break;
    case IPC_DEBUG_UTC_ASSERT:
        CSP_PRINTF_INFO("%s: Assert\n", __func__);
        CSP_ASSERT(0);
        break;
    case IPC_DEBUG_UTC_FAULT:
        CSP_PRINTF_INFO("%s: Fault\n", __func__);
        CSP_FAULT(0);
        break;
    case IPC_DEBUG_UTC_HANG:
        CSP_PRINTF_INFO("%s: panic\n", __func__);
        CSP_PANIC(0);
        break;
    case IPC_DEBUG_UTC_HANG_ITMON:
        CSP_PRINTF_INFO("%s: itmon\n", __func__);
        memset((void *)0xfffffffe, 0, 1024);
        break;
    case IPC_DEBUG_UTC_REBOOT:
        CSP_PRINTF_INFO("%s: reboot\n", __func__);
        CSP_REBOOT(0);
        break;
    case IPC_DEBUG_UTC_HANG_IPC_C2A_FULL:
    case IPC_DEBUG_UTC_HANG_IPC_C2A_CRASH:
    case IPC_DEBUG_UTC_HANG_IPC_C2A_DATA_FULL:
    case IPC_DEBUG_UTC_HANG_IPC_C2A_DATA_CRASH:
    case IPC_DEBUG_UTC_HANG_IPC_A2C_FULL:
    case IPC_DEBUG_UTC_HANG_IPC_A2C_CRASH:
    case IPC_DEBUG_UTC_HANG_IPC_A2C_DATA_FULL:
    case IPC_DEBUG_UTC_HANG_IPC_A2C_DATA_CRASH:
    case IPC_DEBUG_UTC_HANG_IPC_LOGBUF_EQ_CRASH:
    case IPC_DEBUG_UTC_HANG_IPC_LOGBUF_DQ_CRASH:
    case IPC_DEBUG_UTC_HANG_INVAL_INT:
        ipc_hang_test(event);
        break;
    default:
        CSP_PRINTF_INFO("%s: event:%d doesn't supported.\n", __func__, event);
        break;
    }
#endif
}

static void handleIRQ(enum irq_evt_chub evt, int irq_num)
{
    switch (evt) {
    case IRQ_EVT_A2C_RESET:
        CSP_PRINTF_ERROR("%s: Reset\n", __func__);
        ipc_hw_clear_int_pend_reg(AP, irq_num);
#if defined(SEOS)
        platReset(1);
#endif
        break;
    case IRQ_EVT_A2C_SHUTDOWN:
        CSP_PRINTF_ERROR("%s: Shutdown\n", __func__);
        ipc_hw_clear_int_pend_reg(AP, irq_num);
#if defined(SEOS)
        platReset(0);
#endif
        break;
    case IRQ_EVT_A2C_WAKEUP:
#if defined(LOCAL_POWERGATE)
        pwrDrvEnableApWakeLock();
#endif
#if defined(SEOS)
        hostIntfRxPacket(true);
#endif
        break;
    case IRQ_EVT_A2C_WAKEUP_CLR:
#if defined(LOCAL_POWERGATE)
        pwrDrvDisableApWakeLock();
#endif
#if defined(SEOS)
        hostIntfRxPacket(false);
#endif
        break;
    case IRQ_EVT_A2C_DEBUG:
        handleDebug();
        break;
    default:
        mailboxDrvHandleRxMessage(evt);
        break;
    }
}

void mailboxAP_IRQHandler(void)
{
    unsigned int status = ipc_hw_read_int_status_reg(AP);
    int start_index = ipc_hw_read_int_start_index(AP);
    enum irq_evt_chub evt;
    struct ipc_evt_buf *cur_evt;
    int irq_num = IRQ_EVT_CHUB_ALIVE + start_index;
    int log;
    u32 status_org = status;
    struct ipc_evt *ipc_evt = ipc_get_base(IPC_REG_IPC_EVT_A2C);

    /* chub alive interrupt handle */
    if (status & (1 << irq_num)) {
        status &= ~(1 << irq_num);
        ipc_hw_clear_int_pend_reg(AP, irq_num);
        log = ipc_hw_read_shared_reg(AP, SR_3);
        if(log) {
            ipc_set_ap_wake(log);
            if (log == AP_SLEEP){
                apHubSyncReset(NULL);
                pwrDrvDisableApWakeLock();
            }
            else
                pwrDrvEnableApWakeLock();
        }
        ipc_hw_write_shared_reg(AP, 0, SR_3);
        osLog(LOG_INFO, "%s: ap enter:%d, %s\n",
                MAILBOX_DRV, (int)log, (ipc_get_ap_wake() == AP_WAKE) ? "wake":"sleep");
        if (log != AP_SLEEP)
            ipc_hw_gen_interrupt(AP, IRQ_EVT_CHUB_ALIVE);
    }

#ifdef CHECK_HW_TRIGGER
    /* ipc interrupt handle */
    while (status) {
        cur_evt = ipc_get_evt(IPC_EVT_A2C);
        if (cur_evt) {
            evt = (enum irq_evt_chub)cur_evt->evt;
            irq_num = cur_evt->irq + start_index;

            if (!ipc_evt->ctrl.pending[cur_evt->irq])
                CSP_PRINTF_ERROR
                    ("%s: no-sw-trigger irq:%d(%d + %d), evt:%d, status:0x%x->0x%x(SR:0x%x)\n", __func__,
                    irq_num, cur_evt->irq, start_index, evt, status_org, status, ipc_hw_read_int_status_reg(AP));

            if (!(status & (1 << irq_num))) {
                if (evt == IRQ_EVT_A2C_RESET) {
                    CSP_PRINTF_ERROR
                    ("%s: IRQ_EVT_A2C_RESET\n", __func__);
                    return;
                } else {
                    CSP_PRINTF_ERROR
                    ("%s: no-hw-trigger irq:%d(%d + %d), evt:%d, status:0x%x->0x%x(SR:0x%x)\n", __func__,
                    irq_num, cur_evt->irq, start_index, evt, status_org, status, ipc_hw_read_int_status_reg(AP));
                    ipc_print_evt(IPC_EVT_A2C);
                }
            }
        } else {
            mailboxDrvEnable();
            CSP_PRINTF_ERROR("%s: eventq is empty, status:0x%x->0x%x(SR:0x%x)\n",
            __func__, status_org, status, ipc_hw_read_int_status_reg(AP));
            ipc_print_evt(IPC_EVT_A2C);
            ipc_hw_clear_all_int_pend_reg(AP);
            return;
        }
        ipc_hw_clear_int_pend_reg(AP, irq_num);
        ipc_evt->ctrl.pending[cur_evt->irq] = 0;
        handleIRQ(evt, irq_num); /* bboot set it first */
        status &= ~(1 << irq_num);
    }
    if (status)
    CSP_PRINTF_ERROR("%s: remain status:0x%x -> 0x%x, end:%d~%d\n",
    __func__, status_org, status, start_index, irq_num);
#else
    if (status) {
        int i;

        for (i = start_index; i < irq_num; i++) {
            if (status & (1 << i)) {
                cur_evt = ipc_get_evt(IPC_EVT_A2C);
                if (cur_evt) {
                    evt = (enum irq_evt_chub)cur_evt->evt;
                    handleIRQ(evt, i);
					ipc_hw_clear_int_pend_reg(AP, i);
					status &= ~(1 << i);
                } else {
                    CSP_PRINTF_ERROR
                        ("%s(%d): eventq is empty, status:0x%x(SR:0x%x)\n",
                         __func__, i, status, ipc_hw_read_int_status_reg(AP));
                    mailboxDrvEnable();
                    ipc_hw_clear_all_int_pend_reg(AP);
                    return;
                }
            }
        }
    }
#endif

}

#if defined(MBAPM_REQUIRED)
void mailboxApm2Chub_IRQHandler(void)
{
    uint32_t num, i;

    num = ipc_hw_read_int_status_reg(APM);
    int start_index = ipc_hw_read_int_start_index(APM);

    for (i = start_index; i < start_index + IRQ_EVT_CHUB_MAX; i++) {
        if (num & (1 << i)) {
            if ((i - start_index) == IRQ_EVT_CH0) {
                ipc_hw_clear_int_pend_reg(APM, IRQ_EVT_CH0);
                handleApmRxMessage(IRQ_EVT_CH0);
            }
        }
    }
}
#endif

#if defined(MBGNSS_REQUIRED)
void MailboxGnss2Chub_IRQHandler(void)
{
    uint32_t num, i;

    CSP_PRINTF_INFO("gnssIpc_MailboxGnss2Chub_IRQHandler\n");

    if (pwrDrvGetPowerMode() == POWER_MODE_SLEEP) {
        sysregSetAPMReq(TRUE);
        while (1) {
            if (sysregGetAPMAck() == 0x110)
                break;
        }
    }

    num = ipc_hw_read_int_status_reg(GNSS);
    int start_index = ipc_hw_read_int_start_index(GNSS);

    for (i = start_index; i < start_index + IRQ_EVT_END; i++) {
        if (num & (1 << i)) {
            mailboxGnssDrvhandleIrq((i - start_index));
        }
    }
}
#endif

void mailboxOsEnable(void)
{
#if defined(SEOS)
    ipc_set_base(__bl_start);
#else
    ipc_set_base(0);
#endif

    if (!ipc_get_chub_map())
        return;

    NVIC_ClearPendingIRQ(MB_AP_IRQn);
    NVIC_EnableIRQ(MB_AP_IRQn);
#if defined(MBGNSS_REQUIRED)
    NVIC_ClearPendingIRQ(MB_GNSS_IRQn);
    NVIC_EnableIRQ(MB_GNSS_IRQn);
#endif
#if defined(APM_HANDSHAKING)
    NVIC_EnableIRQ(MB_APM_IRQn);
#endif
}
