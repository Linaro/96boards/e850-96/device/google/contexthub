/*
 * Copyright (C) 2017 Samsung Electronics Co., Ltd.
 *
 * Contact: Boojin Kim <boojin.kim@samsung.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>
#include <mailbox.h>
#include <mailboxDrv.h>
#include <csp_os.h>
#include <csp_common.h>
#include <csp_printf.h>
#include <chub_ipc.h>

struct RxCBInfo {
    mailboxRxCB callback;
    void *buf;
    void *cookie;
} mRxCB;

#ifdef DEBUG
#define SUPPORT_LOOPBACKTEST
#endif

#ifdef SUPPORT_LOOPBACKTEST
static bool mLoopbackTest;
#endif

#ifdef PACKET_LOW_DEBUG
static void debugDumpBuf(uint8_t * buf, uint32_t len)
{
    int i;

    for (i = 0; i < len; i += 16)
        CSP_PRINTF_INFO
            ("0x%x: %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x\n",
             (uint32_t) & buf[i], buf[i], buf[i + 1], buf[i + 2], buf[i + 3],
             buf[i + 4], buf[i + 5], buf[i + 6], buf[i + 7], buf[i + 8],
             buf[i + 9], buf[i + 10], buf[i + 11], buf[i + 12], buf[i + 13],
             buf[i + 14], buf[i + 15]);
}
#else
#define debugDumpBuf(a, b) {}
#endif

#ifdef LOWLEVEL_DEBUG
#define DEBUG_PRINT(lv, fmt, ...)	\
	((DEBUG_LEVEL == (0)) ? (CSP_PRINTF_INFO(fmt, ##__VA_ARGS__)) : \
	((DEBUG_LEVEL == (lv)) ? (CSP_PRINTF_INFO(fmt, ##__VA_ARGS__)) : (NULL)))
#else
#define DEBUG_PRINT(level, fmt, ...)
#endif

void mailboxDrvHandleRxMessage(int irq_num)
{
#ifdef SUPPORT_LOOPBACKTEST
    if (mLoopbackTest) {
        uint8_t buf[PACKET_SIZE_MAX];
        u32 size;
        void *tmpbuf;

        memset(buf, 0, PACKET_SIZE_MAX);

        tmpbuf = ipc_read_data(IPC_DATA_A2C, &size);
        if (tmpbuf)
            memcpy(buf, tmpbuf, size);
        if (size <= PACKET_SIZE_MAX)
            mailboxDrvWrite(buf, size);
        else
            CSP_PRINTF_ERROR("%s: invalid size: %d\n", MAILBOX_DRV, size);

        return;
    }
#endif

    if (mRxCB.buf) {
        void *rxbuf = NULL;
        u32 ret;

        rxbuf = ipc_read_data(IPC_DATA_A2C, &ret);
        if (rxbuf) {
            memcpy(mRxCB.buf, rxbuf, ret);
            ret = 0;
        } else {
            ret = -1;
        }
        if (mRxCB.callback)
            mRxCB.callback(mRxCB.cookie, mRxCB.buf, ret);
        else
            CSP_PRINTF_ERROR("%s: no CB: %p, %p\n", MAILBOX_DRV, mRxCB.buf, mRxCB.callback);
    }
}

int32_t mailboxDrvRead(void *buf, uint32_t size, mailboxRxCB callback,
                       void *cookie)
{
    (void)size;
    mRxCB.callback = callback;
    mRxCB.cookie = cookie;
    mRxCB.buf = buf;

    return 0;
}

void mailboxDrvWriteEvent(enum mailbox_event event, uint32_t val)
{
    enum irq_evt_chub irq;

    switch (event) {
    case MAILBOX_EVT_WAKEUP:
        irq = val ? IRQ_EVT_C2A_INT : IRQ_EVT_C2A_INTCLR;
        DEBUG_PRINT(LOG_INFO, "<-AI%d\n", irq);
        break;
    case MAILBOX_EVT_DEBUG:
        ipc_write_debug_event(AP, (enum ipc_debug_event)val);
        irq = IRQ_EVT_C2A_DEBUG;
        break;
    default:
        osLog(LOG_DEBUG, "%s: invalid event %d, val %u\n", __func__, event,
              (u32) val);
        return;
    };

    ipc_add_evt(IPC_EVT_C2A, irq);
}

int32_t mailboxDrvWrite(const void *buf, uint32_t size)
{
    return ipc_write_data(IPC_DATA_C2A, (void *)buf, size);
}

void mailboxDrvEnable(void)
{
    ipc_init();
    ipc_set_owner(AP, (void *)MAILBOX_AP_BASE_ADDRESS, IPC_DST);
    ipc_hw_set_mcuctrl(AP, 0x1);
    if (ipc_get_offset(IPC_REG_SHARED))
        memset(ipc_get_base(IPC_REG_SHARED), 0x0, ipc_get_offset(IPC_REG_SHARED));
}

void mailboxDrvDisable(void)
{
    ipc_hw_set_mcuctrl(AP, 0x1);
    ipc_hw_clear_all_int_pend_reg(AP);
}

#ifdef SUPPORT_LOOPBACKTEST
void mailboxDrvSetLoopbackTestMode(enum ipc_debug_event event)
{
    if (event == IPC_DEBUG_UTC_IPC_TEST_START)
        mLoopbackTest = 1;
    else if (event == IPC_DEBUG_UTC_IPC_TEST_START)
        mLoopbackTest = 0;
    CSP_PRINTF_ERROR("%s: on:%d\n", __func__, mLoopbackTest);
}
#else
void mailboxDrvSetLoopbackTestMode(enum ipc_debug_event event)
{
    return;
}
#endif
