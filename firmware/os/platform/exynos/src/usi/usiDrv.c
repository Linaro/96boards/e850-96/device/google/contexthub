/*----------------------------------------------------------------------------
 *      Exynos SoC  -  USI
 *----------------------------------------------------------------------------
 *      Name:    usiDrv.c
 *      Purpose: To implement USI driver functions
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <csp_common.h>
#if defined(SEOS)
    #include <errno.h>
#else
    #include <Device.h>
    #define EINVAL  22
#endif
#include <usi.h>
#include <usiDrv.h>
#include <usiSfrBase.h>
#include <sysreg.h>


// USI_OPTION

// -- USI_HWACG
#define CLKREQ_ON_HIGH                  (1 << 1)
#define CLKREQ_ON_LOW                   (0 << 1)
#define CLKSTOP_ON_HIGH                 (1 << 2)
#define CLKSTOP_ON_LOW                  (0 << 2)
#define USI_HWACG_OFF                   (CLKREQ_ON_HIGH | CLKSTOP_ON_LOW)
#define USI_HWACG_ON                    (CLKREQ_ON_LOW | CLKSTOP_ON_HIGH)

#define USI_HWACG                       USI_HWACG_OFF

// -- USI_MASTER
#define MASTER                          (1 << 0)

// USI_CON

// -- USI_RESET
#define USI_RESET_HIGH                  (1 << 0)
#define USI_RESET_LOW                   (0 << 0)

#define USI_RESET_ASSERT                (1 << 0)
#define USI_RESET_DEASSERT              (0 << 0)

#define USI_STATE_OPEN                  (1)
#define USI_STATE_CLOSE                 (0)

#define USI_PROTOCOL_MASK               (0x7)

typedef struct {
    UsiSfrBaseType  *base;
    uint32_t        protocol;
    uint32_t        state;
}UsiInfoType;

UsiInfoType     mUsiInfo[USI_CHANNEL_MAX];
//

static void usiDrvReset( IN uint32_t port, IN uint32_t assert)
{
    if (assert == USI_RESET_ASSERT || assert == USI_RESET_DEASSERT)
        __raw_writel( assert , mUsiInfo[port].base->USI + REG_USI_CON);
}

int32_t usiDrvOpen(IN uint32_t port, IN UsiProtocolType protocol, IN CmgpHwacgControlType hwacg)
{
    int32_t ret = 0;

    if( mUsiInfo[port].state == USI_STATE_OPEN ) return 0;

    mUsiInfo[port].base = &mUsiSfrBase[port];

    mUsiInfo[port].state = USI_STATE_OPEN;

    if (protocol != 0)
        ret = usiDrvSetProtocol(port, protocol, hwacg);

    if ( ret < 0 ) {
        mUsiInfo[port].state = USI_STATE_CLOSE;
        usiDrvReset( port, USI_RESET_ASSERT );
        return ret;
    }

    mUsiInfo[port].protocol = protocol;

    return 0;
}

//
int32_t usiDrvSetProtocol(IN uint32_t port, IN UsiProtocolType protocol, IN CmgpHwacgControlType hwacg)
{
    if( mUsiInfo[port].state != USI_STATE_OPEN )
    {
        CSP_PRINTF_ERROR("%s : USI port %d is not opened\n", __func__, (int)port);
        return -EINVAL;
    }

    usiDrvReset( port, USI_RESET_ASSERT);

    switch(protocol)
    {
        case USI_PROTOCOL_UART:
        case USI_PROTOCOL_SPI:
        case USI_PROTOCOL_I2C:
            __raw_writel( protocol, mUsiInfo[port].base->SW_CONF );
            break;
        default:
            CSP_PRINTF_ERROR("%s : invalid protocol %d\n", __func__, (int)protocol);
            return -EINVAL;
    }

    // Set HWACG
    __raw_writel(((hwacg == CMGP_HWACG_CONTROL_ENABLE) ? USI_HWACG_ON: USI_HWACG_OFF) | MASTER,
                 mUsiInfo[port].base->USI + REG_USI_OPTION);

    // Deassert USI_RESET by setting it LOW
    usiDrvReset( port, USI_RESET_DEASSERT);

    return 0;
}

UsiProtocolType usiDrvGetProtocol(IN uint32_t port)
{
    uint32_t regValue;

    regValue = __raw_readl( mUsiInfo[port].base->SW_CONF ) & USI_PROTOCOL_MASK;

    return (UsiProtocolType)regValue;
}

//
void usiDrvClose(uint32_t port)
{
    if( mUsiInfo[port].state != USI_STATE_OPEN)
        return;

    // Assert USI_RESET by setting it HIGH
    usiDrvReset( port, USI_RESET_ASSERT );

    mUsiInfo[port].state = USI_STATE_CLOSE;
}

