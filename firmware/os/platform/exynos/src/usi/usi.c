/*----------------------------------------------------------------------------
 *      Exynos SoC  -  USI
 *----------------------------------------------------------------------------
 *      Name:    usi.c
 *      Purpose: To implement USI APIs
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <csp_common.h>
#if defined(SEOS)
    #include <errno.h>
#endif
#include <usi.h>
#include <usiDrv.h>
#include <usiOS.h>

/* save the protocol of usi channel which is used */
static uint8_t mUsiChannelProtocol[USI_CHANNEL_MAX];

// Public API to initialize USI. This should be called when OS starts
int32_t usiOpen(IN uint32_t port, IN UsiProtocolType protocol, IN CmgpHwacgControlType hwacg)
{
    int32_t ret;

    if ((port >= USI_CHANNEL_MAX) || (protocol >= USI_PROTOCOL_MAX) || (hwacg > CMGP_HWACG_CONTROL_ENABLE))
    {
        CSP_PRINTF_ERROR("%s, invalid parameter (%d, %d, %d)\n", __func__, (int)port, (int)protocol, (int)hwacg);
        return -EINVAL;
    }

    ret = usiDrvOpen(port, protocol, hwacg);
    if (ret)
        return ret;

    mUsiChannelProtocol[port] = (uint8_t)protocol;

    return 0;
}

// To change protocol, hwacg
int32_t usiSetProtocol(IN uint32_t port, IN UsiProtocolType protocol, IN CmgpHwacgControlType hwacg)
{
    if ((port >= USI_CHANNEL_MAX) || (protocol >= USI_PROTOCOL_MAX) || (hwacg > CMGP_HWACG_CONTROL_ENABLE))
    {
        CSP_PRINTF_ERROR("%s, invalid parameter (%d, %d, %d)\n", __func__, (int)port, (int)protocol, (int)hwacg);
        return -EINVAL;
    }

    return usiDrvSetProtocol(port, protocol, hwacg);
}

UsiProtocolType usiGetProtocol(IN uint32_t port)
{
    if (port >= USI_CHANNEL_MAX) {
        CSP_PRINTF_ERROR("%s, invalid port (%d)\n", __func__, (int)port);
        return USI_PROTOCOL_MAX;
    }

    return usiDrvGetProtocol(port);
}

#if defined(UART_REQUIRED)

//
int32_t usiSetUartDbg(void)
{
    return usiOpen(UART_CHANNEL_DEBUG, USI_PROTOCOL_UART, CMGP_DEFAULT_HWACG);
}

#endif

// Public API to save USI SFRs before entering system power mode
void usiSaveState(void)
{
    int i;

    for (i = 0 ; i < USI_CHUB_CHANNEL_MAX ; i++){
        if (mUsiChannelProtocol[i])
            usiDrvClose(i);
    }
}

// Public API to restore USI SFRs after exiting system power mode
void usiRestoreState(void)
{
    int i;

    for (i = 0 ; i < USI_CHUB_CHANNEL_MAX ; i++){
        if (mUsiChannelProtocol[i])
            usiOpen(i, (UsiProtocolType)mUsiChannelProtocol[i], CMGP_DEFAULT_HWACG);
    }
}

void usiCmgpSaveState(void)
{
    int i;

    for (i = USI_CHUB_CHANNEL_MAX ; i < USI_CHANNEL_MAX ; i++){
        if (mUsiChannelProtocol[i])
            usiDrvClose(i);
    }
}

void usiCmgpRestoreState(void)
{
    int i;

    for (i = USI_CHUB_CHANNEL_MAX ; i < USI_CHANNEL_MAX ; i++){
        if (mUsiChannelProtocol[i])
            usiOpen(i, (UsiProtocolType)mUsiChannelProtocol[i], CMGP_DEFAULT_HWACG);
    }
}

// Public API to shutdown USI
void usiClose(uint32_t port)
{
    if (port >= USI_CHANNEL_MAX) {
        CSP_PRINTF_ERROR("%s, invalid port (%d)\n", __func__, (int)port);
        return;
    }

    usiDrvClose(port);

    mUsiChannelProtocol[port] = 0;
}
