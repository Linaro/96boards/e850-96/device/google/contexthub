/*----------------------------------------------------------------------------
 *      Exynos SoC  -  WDT
 *----------------------------------------------------------------------------
 *      Name:    wdt.c
 *      Purpose: To implement WDT APIs
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <csp_common.h>
#if defined(SEOS)
    #include <cmsis.h>
#endif
#include <wdt.h>
#include <wdtDrv.h>
#include <wdtOS.h>

static uint32_t mWdtTime;

void wdtInit(void)
{
    NVIC_EnableIRQ(WDT_CHUB_IRQn);
    wdtDrvInit(WDT_DEFAULT_TIMEOUT, 1, 0);
    mWdtTime = WDT_DEFAULT_TIMEOUT;
    CSP_PRINTF_INFO("%s with %d ms\n",
	    __func__, (u32)mWdtTime);
}

void wdtEnableClk(void)
{
    wdtEnable();
}

void wdtDisableClk(void)
{
    wdtDisable();
}

void wdtEnable(void)
{
    wdtDrvEnable();
}

void wdtDisable(void)
{
    wdtDrvDisable();
}

void wdtSetTime(uint32_t time_msec)
{
    if( time_msec == 0 )
        return;

    wdtDrvSetTime(time_msec);
    mWdtTime = time_msec;
}

void wdtPing(void)
{
    if( mWdtTime == 0 )
        return;

    wdtSetTime( mWdtTime );
}

void wdtEnableInterrupt(void)
{
    wdtDrvEnableInterrupt();
}

void wdtDisableInterrupt(void)
{
    wdtDrvDisableInterrupt();
}

void wdtEnableReset(void)
{
    wdtDrvEnableReset();
}

void wdtDisableReset(void)
{
    wdtDrvDisableReset();
}

#if defined(SFR_SAVE_RESTORE)
// Public API to save WDT SFRs before entering system power mode
void wdtSaveState(void)
{

}

// Public API to restore WDT SFRs after exiting system power mode
void wdtRestoreState(void)
{

}
#endif

void wdtDeinit(void)
{
    mWdtTime = 0;
    wdtDrvClose();
    NVIC_DisableIRQ(WDT_CHUB_IRQn);
}

