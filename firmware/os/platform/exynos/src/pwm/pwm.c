/*----------------------------------------------------------------------------
 *      Exynos SoC  -  PWM
 *----------------------------------------------------------------------------
 *      Name:    pwm.c
 *      Purpose: To implement PWM APIs
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <csp_common.h>
//#include <toolchain.h>
#if defined(SEOS)
    #include <errno.h>
    #include <cmsis.h>
#endif

#include <pwm.h>
#include <pwmDrv.h>
#include <pwmOS.h>

#define PWM_INVALID_ID      0
#define PWM_INVALID_MODE    1
#define PWM_INVALID_DUTY    2

// Public API to initialize PWM. This should be called when OS starts
int pwmInit( PwmTimerNumType timer, uint32_t uSecond,
             PwmTimerModeType mode, uint32_t intEn )
{
    int ret;

    PwmRequestType request;

    if( timer >= PWM_TIMER_NUM_MAX )
    {
        CSP_ASSERT(0);
        return -EINVAL;
    }

    if( mode >= PWM_TIMER_MODE_MAX )
    {
        CSP_ASSERT(0);
        return -EINVAL;
    }

    request.timer = timer;
    request.period = uSecond;
    request.mode = mode;
    request.interruptEn = intEn;

    ret = pwmDrvOpen( &request );
    if( ret < 0)
        return ret;

    if( intEn )
        NVIC_EnableIRQ((IRQn_Type)(PWM0_CHUB_IRQn +(IRQn_Type)timer));

    return 0;
}

#if defined(PWM_REQUIRED)
int pwmEnableInterrut( PwmTimerNumType timer )
{
    if( timer >= PWM_TIMER_NUM_MAX )
    {
        CSP_ASSERT(0);
        return -EINVAL;
    }

    NVIC_EnableIRQ((IRQn_Type)(PWM0_CHUB_IRQn +(IRQn_Type)timer));
    return pwmDrvEnableInterrupt( timer );
}

int pwmDisableInterrupt( PwmTimerNumType timer )
{
    if( timer >= PWM_TIMER_NUM_MAX )
    {
        CSP_ASSERT(0);
        return -EINVAL;
    }

    NVIC_DisableIRQ((IRQn_Type)(PWM0_CHUB_IRQn +(IRQn_Type)timer));
    return pwmDrvDisableInterrupt( timer );
}

int pwmStopTimer( PwmTimerNumType timer )
{
    if( timer >= PWM_TIMER_NUM_MAX )
    {
        CSP_ASSERT(0);
        return -EINVAL;
    }

    return pwmDrvStopTimer( timer );
}

void pwmStopAllTimer( void )
{
    pwmDrvStopAllTimer();
}

int pwmSetDuty( PwmTimerNumType timer, uint32_t duty )
{
    if( timer >= PWM_TIMER_NUM_MAX )
    {
        CSP_ASSERT(0);
        return -EINVAL;
    }

    if( duty == 0 || duty >= 100 )
    {
        CSP_ASSERT(0);
        return -EINVAL;
    }

    return pwmDrvSetDuty( timer, duty );
}

int pwmSetMode( PwmTimerNumType timer, PwmTimerModeType mode )
{
    if( timer >= PWM_TIMER_NUM_MAX )
    {
        CSP_ASSERT(0);
        return -EINVAL;
    }

    return pwmDrvSetMode( timer, mode );
}

int pwmSetDefaultLevel( PwmTimerNumType timer, uint32_t level )
{
    if( timer >= PWM_TIMER_NUM_MAX )
    {
        CSP_ASSERT(0);
        return -EINVAL;
    }

    return pwmDrvSetDefaultLevel( timer, level );
}

uint32_t pwmGetCount( PwmTimerNumType timer)
{
    if( timer >= PWM_TIMER_NUM_MAX )
    {
        CSP_ASSERT(0);
        return 0;
    }

    return pwmDrvGetCount( timer );
}

uint32_t pwmGetCurrentTimeInTick( PwmTimerNumType timer)
{
    return pwmGetCurrentTime(timer) / 1000 / TICK_INTERNAL_IN_MS;
}

uint32_t pwmGetRemainTime( PwmTimerNumType timer)
{
    if( timer >= PWM_TIMER_NUM_MAX )
    {
        CSP_ASSERT(0);
        return 0;
    }

    return pwmDrvGetRemainTime( timer );
}

#if defined(SFR_SAVE_RESTORE)
// Public API to save PWM SFRs before entering system power mode
void pwmSaveState(void)
{

}

// Public API to restore PWM SFRs after exiting system power mode
void pwmRestoreState(void)
{

}
#endif

// Public API to shutdown PWM
int pwmDeinit( PwmTimerNumType timer )
{
    if( timer >= PWM_TIMER_NUM_MAX )
    {
        CSP_ASSERT(0);
        return -EINVAL;
    }

    return pwmDrvClose(timer);
}
#endif

int pwmStartTimer( PwmTimerNumType timer )
{
    if( timer >= PWM_TIMER_NUM_MAX )
    {
        CSP_ASSERT(0);
        return -EINVAL;
    }

    return pwmDrvStartTimer( timer );
}

int pwmSetTime( PwmTimerNumType timer, uint32_t uSeconds )
{
    if( timer >= PWM_TIMER_NUM_MAX )
    {
        CSP_ASSERT(0);
        return -EINVAL;
    }

    return pwmDrvSetTime( timer, uSeconds );
}

uint32_t pwmGetCurrentTime( PwmTimerNumType timer)
{
    if( timer >= PWM_TIMER_NUM_MAX )
    {
        CSP_ASSERT(0);
        return 0;
    }

    return pwmDrvGetCurrentTime( timer );
}


