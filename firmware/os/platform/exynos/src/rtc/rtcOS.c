/*----------------------------------------------------------------------------
 *      Exynos SoC  -  RTC
 *----------------------------------------------------------------------------
 *      Name:    rtcOS.c
 *      Purpose: OS-dependent part for RTC driver
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if defined(RTC_REQUIRED)

#include <csp_common.h>
#include <csp_assert.h>
#include <csp_printf.h>

#include <rtc.h>
#include <rtcDrv.h>
#include <rtcOS.h>
#if defined(SEOS)
    #include <cmsis.h>
    #include <chub_ipc.h>
#endif

void rtc_IRQHandler(void)
{
    uint32_t intPend;

    intPend = rtcDrvWhichInt();

#if (RTC_ALARM_SUPPORT)
    if ( intPend & RTC_ALARM_INTERRUPT ) {
        rtcIRQHandler( RTC_ALARM_INTERRUPT );
    }
#endif

    if ( intPend & RTC_TICK_INTERRUPT) {
        rtcIRQHandler( RTC_TICK_INTERRUPT );
    }
}

struct RtcTickIsr RtcTickIsrList[RtcTickIdMax];

void rtcTick0_IRQHandler(void)
{
    osLog(LOG_INFO, "rtcT0H\n");
    rtcStopTick();
    CSP_ASSERT(rtcDrvWhichInt() & 0x1);
    NVIC_ClearPendingIRQ(CHUB_RTC_TICK0_IRQn);

    // Do Something
    if(RtcTickIsrList[RtcTickId0].func) RtcTickIsrList[RtcTickId0].func();
    if(rtcGetTickRepeat())
        rtcStartTick();
}

void rtcTick1_IRQHandler(void)
{
    osLog(LOG_INFO, "rtcT1H\n");
    rtcStopTick();
    CSP_ASSERT(rtcDrvWhichInt() & 0x4);
    NVIC_ClearPendingIRQ(CHUB_RTC_TICK1_IRQn);

    // Do Something
    if(RtcTickIsrList[RtcTickId1].func) RtcTickIsrList[RtcTickId1].func();

    if(rtcGetTickRepeat())
        rtcStartTick();
}
void rtcSetHandler(uint8_t rtcNum, void *isr)
{
    if((rtcNum < RtcTickId0) || (rtcNum>=RtcTickIdMax) || (isr == NULL)){
        CSP_PRINTF_ERROR("%s: bad parameter",__FUNCTION__);
        return;
     }

    RtcTickIsrList[rtcNum].func = (void (*)(void))isr;
}
void rtcUnsetHandler(uint8_t rtcNum)
{
    if((rtcNum < RtcTickId0) || (rtcNum>=RtcTickIdMax)){
        CSP_PRINTF_ERROR("%s: bad parameter",__FUNCTION__);
        return;
     }

    RtcTickIsrList[rtcNum].func = NULL;
}

void rtc_callback(void)
{
    CSP_PRINTF_INFO("%s called\n", __func__);
}

void logbuf_callback(void)
{
    CSP_PRINTF_INFO("%s logbuf set level 1\n", __func__);
    ipc_logbuf_loglevel(CHUB_RT_LOG_DUMP, 1);
}

#endif//end defined(RTC_REQUIRED)

