/*----------------------------------------------------------------------------
 *      Exynos SoC  -  RTC
 *----------------------------------------------------------------------------
 *      Name:    rtcUtil.c
 *      Purpose: To Translate System time into value of seconds
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <rtc.h>
#include <rtcUtil.h>

/// Internal function for rtcUtilCountDate
static uint8_t rtcUtilIsLeapYear(uint32_t year)
{
    if(!(year % 4) && (year % 100))
    {
        return TRUE;
    }

    return FALSE;
}

/// Internal function for rtcUtilCountDate
static uint32_t rtcUtilCountDateWithinYear(uint32_t start_mon, uint32_t start_date, uint32_t end_mon, uint32_t end_date, uint8_t leap_year)
{
    uint32_t  i_mon;
    uint32_t  num_date;

    uint8_t num_date_of_each_month[13] =
    {
        0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    };

    if( start_mon > 12 ) {
        CSP_PRINTF_ERROR("rtcUtilCountDateWithinYear() - start_mon(%d) is invalid\n", (int)start_mon);
        CSP_ASSERT(0);
    }

    if( end_mon > 12 ){
        CSP_PRINTF_ERROR("rtcUtilCountDateWithinYear() - end_mon(%d) is invalid\n", (int)end_mon);
        CSP_ASSERT(0);
    }

    if( leap_year )
        num_date_of_each_month[2] += 1; // February

    if(!( start_date <= num_date_of_each_month[start_mon] )) {
        CSP_PRINTF_ERROR("rtcUtilCountDateWithinYear() - start_date(%d), start_mon (%d)is invalid\n", (int)start_date, (int)start_mon);
        CSP_ASSERT(0);
    }

    if(!( end_date <= num_date_of_each_month[end_mon] )) {
        CSP_PRINTF_ERROR("rtcUtilCountDateWithinYear() - end_date(%d), end_mon (%d)is invalid\n", (int)end_date, (int)end_mon);
        CSP_ASSERT(0);
    }

    num_date = 0;

    if( start_mon == end_mon )
    {
        num_date = end_date - start_date + 1;
    }
    else
    {
        num_date += num_date_of_each_month[start_mon] - start_date + 1;

        if( (start_mon + 1) <= (end_mon - 1) )
        {
            for( i_mon = (start_mon + 1); i_mon <= (end_mon - 1); i_mon++ )
            {
                num_date += num_date_of_each_month[i_mon];
            }
        }

        num_date += end_date;
    }

    return num_date;
}

/// Internal function for rtcUtilGetSystemTimeSec
static uint32_t rtcUtilCountDate( uint32_t start_year, uint32_t start_mon, uint32_t start_date,
                             uint32_t end_year, uint32_t end_mon, uint32_t end_date )
{
    uint32_t  i_year;
    uint32_t  num_leap_year;
    uint32_t  num_year;
    uint32_t  num_date;

    if((end_year - start_year) >= 1000) {
        CSP_PRINTF_ERROR("rtcUtilCountDate() - end_year(%d), start_year (%d)is invalid\n", (int)end_year, (int)start_date);
        CSP_ASSERT(0);
    }

    num_date = 0;

    if( start_year == end_year )
    {
        num_date += rtcUtilCountDateWithinYear( start_mon, start_date,
                                                  end_mon, end_date,
                                                  rtcUtilIsLeapYear( start_year ) );
    }
    else
    {
        num_date += rtcUtilCountDateWithinYear( start_mon, start_date,
                                             12, 31,
                                             rtcUtilIsLeapYear( start_year ) );

        if( (start_year + 1) <= (end_year - 1) )
        {
            num_year = (end_year - 1) - (start_year + 1);
            num_leap_year = 0;
            for( i_year = 0; i_year <= num_year; i_year++ )
            {
                if( rtcUtilIsLeapYear( i_year ) )
                    num_leap_year++;
            }

            num_date += (num_year * 365);
            num_date += num_leap_year;
        }

        num_date += rtcUtilCountDateWithinYear( 1, 1,
                                                  end_mon, end_date,
                                                  rtcUtilIsLeapYear( end_year ) );
    }

    return num_date;
}

// Public API

/// Translate system time (year, month, date, hour, minute and second) into the value of seconds
uint32_t rtcUtilGetSystemTimeSec(IN uint32_t year, IN uint32_t mon, IN uint32_t date, IN uint32_t hour, IN uint32_t min, IN uint32_t sec)
{
    uint32_t  num_date;
    uint32_t  system_time_sec;

    num_date = rtcUtilCountDate( RTC_MIN_YEAR, 1, 1, year, mon, date );

    num_date -= 1;

    system_time_sec = num_date * 86400; // 86400 = 60sec * 60min * 24hour

    system_time_sec += (hour * 3600);
    system_time_sec += (min * 60);
    system_time_sec += (sec);

    return system_time_sec;
}

uint64_t rtcMkTime64(IN uint32_t year0, IN uint32_t mon0, IN uint32_t date, IN uint32_t hour, IN uint32_t min, IN uint32_t sec)
{
    unsigned int mon = mon0, year = year0;

    /* 1..12 -> 11,12,1..10 */
    if (0 >= (int) (mon -= 2)) {
        mon += 12;	/* Puts Feb last since it has leap day */
        year -= 1;
    }

	return ((((uint64_t)
		  (year/4 - year/100 + year/400 + 367*mon/12 + date) +
		  year*365 - 719499
	    )*24 + hour /* now have hours */
	  )*60 + min /* now have minutes */
	)*60 + sec; /* finally seconds */
}

