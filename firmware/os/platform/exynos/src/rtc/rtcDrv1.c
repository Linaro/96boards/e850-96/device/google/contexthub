/*----------------------------------------------------------------------------
 *      Exynos SoC  -  RTC
 *----------------------------------------------------------------------------
 *      Name:    rtcDrv9610.c
 *      Purpose: To implement RTC driver functions
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if defined(RTC_REQUIRED)

#include <csp_printf.h>
#include <rtcUtil.h>
#include <rtcDrv.h>
#if defined(SEOS)
    #include <atomic.h>
#endif

#define REG_RTC_INTP            (RTC_BASE_ADDRESS + 0x30)
#define REG_RTC_RTCCON          (RTC_BASE_ADDRESS + 0x40)
#define REG_RTC_TICCNT0         (RTC_BASE_ADDRESS + 0x44)
#define REG_RTC_TICCNT1         (RTC_BASE_ADDRESS + 0x48)
#define REG_RTC_RTCALM          (RTC_BASE_ADDRESS + 0x50)
#define REG_RTC_ALMSEC          (RTC_BASE_ADDRESS + 0x54)
#define REG_RTC_ALMMIN          (RTC_BASE_ADDRESS + 0x58)
#define REG_RTC_ALMHOUR         (RTC_BASE_ADDRESS + 0x5C)
#define REG_RTC_ALMDAY          (RTC_BASE_ADDRESS + 0x60)
#define REG_RTC_ALMMON          (RTC_BASE_ADDRESS + 0x64)
#define REG_RTC_ALMYEAR         (RTC_BASE_ADDRESS + 0x68)
#define REG_RTC_BCDSEC          (RTC_BASE_ADDRESS + 0x70)
#define REG_RTC_BCDMIN          (RTC_BASE_ADDRESS + 0x74)
#define REG_RTC_BCDHOUR         (RTC_BASE_ADDRESS + 0x78)
#define REG_RTC_BCDDAY          (RTC_BASE_ADDRESS + 0x7C)
#define REG_RTC_BCDDAYWEEK      (RTC_BASE_ADDRESS + 0x80)
#define REG_RTC_BCDMON          (RTC_BASE_ADDRESS + 0x84)
#define REG_RTC_BCDYEAR         (RTC_BASE_ADDRESS + 0x88)
#define REG_RTC_CURTICCNT0      (RTC_BASE_ADDRESS + 0x90)
#define REG_RTC_CURTICCNT1      (RTC_BASE_ADDRESS + 0x94)

#define TICEN_BIT1      (14)
#define TICEN_BIT0      (8)
#define TICCKSEL_BIT1   (10)
#define TICCKSEL_BIT0   (7)
#define CLKOUTEN_BIT    (9)
#define CLKRST_BIT      (3)
#define CNTSEL_BIT      (2)
#define CLKSEL_BIT      (1)
#define CTLEN_BIT       (0)

#define INT_MASK        (0x1<<2)
#define TICK_INT_MASK   (0x1<<2)
#define ALM_INT_MASK    (0)

#define ALMEN_BIT       (6)
#define YEAREN_BIT      (5)
#define MONEN_BIT       (4)
#define DAYEN_BIT       (3)
#define HOUREN_BIT      (2)
#define MINEN_BIT       (1)
#define SECEN_BIT       (0)

#define RTC_32768KHZ    32768

#define NS_PER_S        1000000000ULL

#define RTC_TICK_COUNT  0xFFFFFFFFULL
#define RTC_TICK_CYCLE  ( (RTC_TICK_COUNT+1) * NS_PER_S / RTC_32768KHZ )

// Delta time of AP & CHUB is managed in nanohubcommand & hostintf. Thus we do not need this
#undef INITIAL_SYSTEM_TIME

#if defined(INITIAL_SYSTEM_TIME)
static uint64_t initial_total_sec_ns;

// Calculate the elapsed time in second since RTC clock timer
static uint64_t rtcDrvGetBCDTimeSec(uint32_t *curTick0)
{
    uint32_t year, mon, date, hour, min, sec;
    uint32_t bcdValueY, bcdValueMo, bcdValueD, bcdValueH, bcdValueMi, bcdValueS;

    // Read BCD time and current tick SFRs
    bcdValueY = __raw_readl(REG_RTC_BCDYEAR) & 0xFFF;
    bcdValueMo = __raw_readl(REG_RTC_BCDMON) & 0x1F;
    bcdValueD = __raw_readl(REG_RTC_BCDDAY) & 0x3F;
    bcdValueH = __raw_readl(REG_RTC_BCDHOUR) & 0x3F;
    bcdValueMi = __raw_readl(REG_RTC_BCDMIN) & 0x7F;
    bcdValueS = __raw_readl(REG_RTC_BCDSEC) & 0x7F;
    if(curTick0 != NULL) *curTick0 = __raw_readl(REG_RTC_CURTICCNT0);

    year = 2000 + (bcdValueY >> 8) * 100 + ((bcdValueY >> 4) & 0xF) * 10 + (bcdValueY & 0xF);
    mon = (bcdValueMo >> 4) * 10 + (bcdValueMo & 0xF);
    date = (bcdValueD >> 4) * 10 + (bcdValueD & 0xF);
    hour = (bcdValueH >> 4) * 10 + (bcdValueH & 0xF);
    min = (bcdValueMi >> 4) * 10 + (bcdValueMi & 0xF);
    sec = (bcdValueS >> 4) * 10 + (bcdValueS & 0xF);

    return rtcMkTime64(year, mon, date, hour, min, sec);
}
#endif

static void rtcDrvControlEnable(IN bool en)
{
    uint32_t regValue = __raw_readl(REG_RTC_RTCCON);

    if(en)
        __raw_writel( regValue | (0x1 << CTLEN_BIT) , REG_RTC_RTCCON);
    else
        __raw_writel( regValue & (uint32_t)(~(0x1 << CTLEN_BIT)) , REG_RTC_RTCCON);
}

// Driver API to initialize RTC
void rtcDrvInit(void)
{
    uint32_t regValue;

    // Set tick0 as infinite up-counter. It lasts 36Hr before wrap down to 0
    __raw_writel(RTC_TICK_COUNT, REG_RTC_TICCNT0);

    // To start RTC system timer and RTC Tick0 counter at the same time
    regValue = (0x1 << CTLEN_BIT) | (1 << CLKRST_BIT);
    __raw_writel(regValue, REG_RTC_RTCCON);
    regValue = (1 << TICEN_BIT0);
    __raw_writel(regValue, REG_RTC_RTCCON);

#if defined(INITIAL_SYSTEM_TIME)
    // Remember the system time at startup
    initial_total_sec_ns = rtcDrvGetBCDTimeSec(NULL) * NS_PER_S;
#endif

    // Exit from RTC control mode
    rtcDrvControlEnable(false);
}

// Driver API to set system time
void rtcDrvSetSystemTime(IN uint32_t year, IN uint32_t mon, IN uint32_t date,
        IN uint32_t day, IN uint32_t hour, IN uint32_t min,
        IN uint32_t sec, IN uint32_t ignoreVal)
{
    uint32_t regValue;

    rtcDrvControlEnable(true);

    if (year != ignoreVal) {
        year %= 1000;
        regValue = ((year / 100) << 8) | (((year % 100) / 10) << 4) | (year % 10);
        __raw_writel( regValue, REG_RTC_BCDYEAR );
    }

    if (mon != ignoreVal) {
        regValue = ((mon / 10) << 4) | (mon % 10);
        __raw_writel( regValue, REG_RTC_BCDMON );
    }

    if (date != ignoreVal) {
        regValue = ((date / 10) << 4) | (date % 10);
        __raw_writel( regValue, REG_RTC_BCDDAY );
    }

    if (day != ignoreVal) {
        __raw_writel( day, REG_RTC_BCDDAYWEEK );
    }

    if (hour != ignoreVal) {
        regValue = ((hour / 10) << 4) | (hour % 10);
        __raw_writel( regValue, REG_RTC_BCDHOUR );
    }

    if (min != ignoreVal) {
        regValue = ((min / 10) << 4) | (min % 10);
        __raw_writel( regValue, REG_RTC_BCDMIN );
    }

    if (sec != ignoreVal) {
        regValue = ((sec / 10) << 4) | (sec % 10);
        __raw_writel( regValue, REG_RTC_BCDSEC );
    }

    rtcDrvControlEnable(false);
}


// Driver API to get system time
void rtcDrvGetSystemTime(OUT uint32_t * year, OUT uint32_t * mon,
        OUT uint32_t * date, OUT uint32_t * day,
        OUT uint32_t * hour, OUT uint32_t * min,
        OUT uint32_t * sec)
{
    uint32_t bcdValue;

    bcdValue = __raw_readl(REG_RTC_BCDYEAR) & 0xFFF;
    *year = 2000 + (bcdValue >> 8) * 100 + ((bcdValue >> 4) & 0xF) * 10 +
            (bcdValue & 0xF);

    bcdValue = __raw_readl(REG_RTC_BCDMON) & 0x1F;
    *mon = (bcdValue >> 4) * 10 + (bcdValue & 0xF);

    bcdValue = __raw_readl(REG_RTC_BCDDAY) & 0x3F;
    *date = (bcdValue >> 4) * 10 + (bcdValue & 0xF);

    *day = __raw_readl(REG_RTC_BCDDAYWEEK) & 0x7;

    bcdValue = __raw_readl(REG_RTC_BCDHOUR) & 0x3F;
    *hour = (bcdValue >> 4) * 10 + (bcdValue & 0xF);

    bcdValue = __raw_readl(REG_RTC_BCDMIN) & 0x7F;
    *min = (bcdValue >> 4) * 10 + (bcdValue & 0xF);

    bcdValue = __raw_readl(REG_RTC_BCDSEC) & 0x7F;
    *sec = (bcdValue >> 4) * 10 + (bcdValue & 0xF);
}

// Driver API to calculate the elapsed time in nano-second
uint64_t rtcDrvGetTimeStampNS(void)
{
    static uint64_t adjustTime = 0;
    static uint32_t lastTick0 = 0;
    uint32_t curTick0;
    uint64_t result;

    mem_reorder_barrier();

    curTick0 = __raw_readl(REG_RTC_CURTICCNT0);

    // Update adjust-time when tick0 counter wraps down to 0
    if(lastTick0 > curTick0) {
        adjustTime += RTC_TICK_CYCLE;
    }

    result = ((uint64_t)curTick0 * NS_PER_S) / RTC_32768KHZ + adjustTime;

#if defined(INITIAL_SYSTEM_TIME)
    result += initial_total_sec_ns;
#endif

    mem_reorder_barrier();

    lastTick0 = curTick0;

    return result;
}

// Driver API to set alarm
void rtcDrvSetAlarmTime(IN uint32_t year, IN uint32_t mon, IN uint32_t date,
        IN uint32_t hour, IN uint32_t min, IN uint32_t sec,
        IN uint32_t ignoreVal)
{
    uint32_t alarmControl = 0;
    uint32_t regValue;

    regValue = __raw_readl( REG_RTC_RTCALM );
    if (regValue & (1 << ALMEN_BIT)) {
        CSP_PRINTF_INFO("RTC Alarm is running \n");
    }

    if (year != ignoreVal) {
        year %= 1000;
        regValue = ((year / 100) << 8) | (((year % 100) / 10) << 4) | (year % 10);
        __raw_writel( regValue, REG_RTC_ALMYEAR);

        alarmControl |= (1 << YEAREN_BIT);
    }

    if (mon != ignoreVal) {
        regValue = ((mon / 10) << 4) | (mon % 10);
        __raw_writel( regValue, REG_RTC_ALMMON );

        alarmControl |= (1 << MONEN_BIT);
    }

    if (date != ignoreVal) {
        regValue = ((date / 10) << 4) | (date % 10);
        __raw_writel( regValue, REG_RTC_ALMDAY );

        alarmControl |= (1 << DAYEN_BIT);
    }

    if (hour != ignoreVal) {
        regValue = ((hour / 10) << 4) | (hour % 10);
        __raw_writel( regValue, REG_RTC_ALMHOUR );

        alarmControl |= (1 << HOUREN_BIT);
    }

    if (min != ignoreVal) {
        regValue = ((min / 10) << 4) | (min % 10);
        __raw_writel( regValue, REG_RTC_ALMMIN );

        alarmControl |= (1 << MINEN_BIT);
    }

    if (sec != ignoreVal) {
        regValue = ((sec / 10) << 4) | (sec % 10);
        __raw_writel( regValue, REG_RTC_ALMSEC );

        alarmControl |= (1 << SECEN_BIT);
    }

    __raw_writel( alarmControl, REG_RTC_RTCALM );
}


// Driver API to get already-set alarm time
void rtcDrvGetAlarmTime(OUT uint32_t * year, OUT uint32_t * mon,
        OUT uint32_t * date, OUT uint32_t * hour,
        OUT uint32_t * min, OUT uint32_t * sec)
{
    uint32_t bcdValue;
    uint32_t alarmControl = 0;

    alarmControl = __raw_readl( REG_RTC_RTCALM );
    if ((alarmControl & (1 << 6)) == 0)
        return;

    if (alarmControl & (1 << 5)) {
        bcdValue = __raw_readl( REG_RTC_ALMYEAR ) & 0xFFF;
        *year =
            2000 + (bcdValue >> 8) * 100 + ((bcdValue >> 4) & 0xF) * 10 +
            (bcdValue & 0xF);
    }

    if (alarmControl & (1 << 4)) {
        bcdValue = __raw_readl( REG_RTC_ALMMON ) & 0x1F;
        *mon = (bcdValue >> 4) * 10 + (bcdValue & 0xF);
    }

    if (alarmControl & (1 << 3)) {
        bcdValue = __raw_readl( REG_RTC_ALMDAY ) & 0x3F;
        *date = (bcdValue >> 4) * 10 + (bcdValue & 0xF);
    }

    if (alarmControl & (1 << 2)) {
        bcdValue = __raw_readl( REG_RTC_ALMHOUR ) & 0x3F;
        *hour = (bcdValue >> 4) * 10 + (bcdValue & 0xF);
    }

    if (alarmControl & (1 << 1)) {
        bcdValue = __raw_readl( REG_RTC_ALMMIN ) & 0x7F;
        *min = (bcdValue >> 4) * 10 + (bcdValue & 0xF);
    }

    if (alarmControl & (1 << 0)) {
        bcdValue = __raw_readl( REG_RTC_ALMSEC ) & 0x7F;
        *sec = (bcdValue >> 4) * 10 + (bcdValue & 0xF);
    }
}


// Driver API to set tick time
void rtcDrvSetTickTime(IN uint32_t sec, IN uint32_t mSec, IN uint32_t uSec)
{
    uint32_t tickTimerSubClock;
    uint32_t tickCountValue;
    uint32_t regValue;


    if ( __raw_readl(REG_RTC_RTCCON) & (0x1<<TICEN_BIT1)) {
        CSP_PRINTF_INFO("RTC Tick is running \n");
    }

    if (sec >= 0x7FFFFFFF) {
        tickTimerSubClock = 0xF;    // 1hz
        tickCountValue = sec;
    }
    else if (sec > 0x3FFFFFFF) {
        tickTimerSubClock = 0xE;    // 2hz
        tickCountValue = (uSec * 2) / 1000;
        tickCountValue = ((mSec * 2) + tickCountValue) / 1000;
        tickCountValue = (sec * 2) + tickCountValue;
    }
    else if (sec > 0x1FFFFFFF) {
        tickTimerSubClock = 0xD;    // 4hz
        tickCountValue = (uSec * 4) / 1000;
        tickCountValue = ((mSec * 4) + tickCountValue) / 1000;
        tickCountValue = (sec * 4) + tickCountValue;
    }
    else if (sec > 0x0FFFFFFF) {
        tickTimerSubClock = 0xC;    // 8hz
        tickCountValue = (uSec * 8) / 1000;
        tickCountValue = ((mSec * 8) + tickCountValue) / 1000;
        tickCountValue = (sec * 8) + tickCountValue;
    }
    else if (sec > 0x07FFFFFF) {
        tickTimerSubClock = 0xB;    // 16hz
        tickCountValue = (uSec * 16) / 1000;
        tickCountValue = ((mSec * 16) + tickCountValue) / 1000;
        tickCountValue = (sec * 16) + tickCountValue;
    }
    else if (sec > 0x03FFFFFF) {
        tickTimerSubClock = 0xA;    // 32hz
        tickCountValue = (uSec * 32) / 1000;
        tickCountValue = ((mSec * 32) + tickCountValue) / 1000;
        tickCountValue = (sec * 32) + tickCountValue;
    }
    else if (sec > 0x01FFFFFF) {
        tickTimerSubClock = 0x9;    // 64hz
        tickCountValue = (uSec * 64) / 1000;
        tickCountValue = ((mSec * 64) + tickCountValue) / 1000;
        tickCountValue = (sec * 64) + tickCountValue;
    }
    else if (sec > 0x00FFFFFF) {
        tickTimerSubClock = 0x8;    // 128hz
        tickCountValue = (uSec * 128) / 1000;
        tickCountValue = ((mSec * 128) + tickCountValue) / 1000;
        tickCountValue = (sec * 128) + tickCountValue;
    }
    else if (sec > 0x007FFFFF) {
        tickTimerSubClock = 0x7;    // 256hz
        tickCountValue = (uSec * 256) / 1000;
        tickCountValue = ((mSec * 256) + tickCountValue) / 1000;
        tickCountValue = (sec * 256) + tickCountValue;
    }
    else if (sec > 0x003FFFFF) {
        tickTimerSubClock = 0x6;    // 512hz
        tickCountValue = (uSec * 512) / 1000;
        tickCountValue = ((mSec * 512) + tickCountValue) / 1000;
        tickCountValue = (sec * 512) + tickCountValue;
    }
    else if (sec > 0x001FFFFF) {
        tickTimerSubClock = 0x5;    // 1024hz
        tickCountValue = (uSec * 1024) / 1000;
        tickCountValue = ((mSec * 1024) + tickCountValue) / 1000;
        tickCountValue = (sec * 1024) + tickCountValue;
    }
    else if (sec > 0x000FFFFF) {
        tickTimerSubClock = 0x4;    // 2048hz
        tickCountValue = (uSec * 2048) / 1000;
        tickCountValue = ((mSec * 2048) + tickCountValue) / 1000;
        tickCountValue = (sec * 2048) + tickCountValue;
    }
    else if (sec > 0x0007FFFF) {
        tickTimerSubClock = 0x3;    // 4094hz
        tickCountValue = (uSec * 4094) / 1000;
        tickCountValue = ((mSec * 4094) + tickCountValue) / 1000;
        tickCountValue = (sec * 4094) + tickCountValue;
    }
    else if (sec > 0x0003FFFF) {
        tickTimerSubClock = 0x2;    // 8192hz
        tickCountValue = (uSec * 8192) / 1000;
        tickCountValue = ((mSec * 8192) + tickCountValue) / 1000;
        tickCountValue = (sec * 8192) + tickCountValue;
    }
    else if (sec > 0x0001FFFF) {
        tickTimerSubClock = 0x1;    // 16384hz
        tickCountValue = (uSec * 16384) / 1000;
        tickCountValue = ((mSec * 16384) + tickCountValue) / 1000;
        tickCountValue = (sec * 16384) + tickCountValue;
    }
    else {
        tickTimerSubClock = 0x0;    // 32768hz
        tickCountValue = (uSec * 32768) / 1000;
        tickCountValue = ((mSec * 32768) + tickCountValue) / 1000;
        tickCountValue = (sec * 32768) + tickCountValue;
    }

    __raw_writel(tickCountValue, REG_RTC_TICCNT1);

    regValue = __raw_readl(REG_RTC_RTCCON);
    regValue &= (uint32_t)(~(0xF << TICCKSEL_BIT1));
    regValue |= (tickTimerSubClock << TICCKSEL_BIT1);
    __raw_writel(regValue, REG_RTC_RTCCON);
}


// Driver API to get current tick time (usec)
uint64_t rtcDrvGetCurrentTickTime(void)
{
    uint64_t currentTime = 0;
    uint32_t ticCkSel, i;
    uint32_t regValue;

    regValue = __raw_readl(REG_RTC_RTCCON);

    if ( regValue & (1 << TICEN_BIT1)) {
        ticCkSel = ~((regValue >> TICCKSEL_BIT1) & 0xF);
        ticCkSel &= 0xF;

        // calculate elapsed time(usec)
        currentTime = (uint64_t)(__raw_readl(REG_RTC_CURTICCNT1)) * 1000000;
        for (i = 0; i < ticCkSel; i++)
            currentTime /= 2;
    }

    return currentTime;
}


// Driver API to stop alarm
void rtcDrvAlarmOnOff(IN bool on)
{
    uint32_t regValue = __raw_readl(REG_RTC_RTCALM);

    if( on == true)
        __raw_writel((regValue | (1 << ALMEN_BIT)), REG_RTC_RTCALM);
    else
        __raw_writel((regValue & (uint32_t)(~(1 << ALMEN_BIT))), REG_RTC_RTCALM);
}


// Driver API to stop tick
void rtcDrvTickOnOff(IN bool on)
{
    uint32_t regValue = __raw_readl(REG_RTC_RTCCON);

    if(on == true)
        __raw_writel( regValue | (0x1<<TICEN_BIT1), REG_RTC_RTCCON);
    else
        __raw_writel( regValue & (uint32_t)(~(0x1<<TICEN_BIT1)), REG_RTC_RTCCON);
}


// Driver API to get generated interrupt type ( alarm or tick or both )
uint32_t rtcDrvWhichInt(void)
{
    uint32_t pendInt;
    uint32_t interrupt = 0;

    pendInt = __raw_readl(REG_RTC_INTP) & INT_MASK;
    __raw_writel(pendInt, REG_RTC_INTP);

    if(pendInt & TICK_INT_MASK)
        interrupt |= 0x1;

    if(pendInt & ALM_INT_MASK)
        interrupt |= 0x2;

    return interrupt;
}

#endif

