/*----------------------------------------------------------------------------
 *      Exynos SoC  -  RTC
 *----------------------------------------------------------------------------
 *      Name:    rtc.c
 *      Purpose: To implement RTC APIs
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <csp_common.h>
#if defined(SEOS)
	#include <cmsis.h>
	#include <atomic.h>
	#include <platform.h>
#endif

#include <rtc.h>
#include <rtcOS.h>
#include <rtcUtil.h>
#include <rtcDrv.h>
#include <cpu/cpuMath.h>
#include <cmsis.h>

#if defined(RTC_REQUIRED)

#define RTC_TICK_INTERRUPT      (1<<0)
#define RTC_ALARM_INTERRUPT     (1<<1)

typedef struct
{
    uint8_t   tickRepeat;
    uint8_t   alarmRepeat;
    uint8_t   tickEn;
    uint8_t   alarmEn;
    uint64_t  tickVal;
    void (*tickCallback)(void);
    void (*alarmCallback)(void);
} RtcCallbackType;

static RtcCallbackType mRtcCallback;

// Public API to initialize RTC. This should be called when OS starts
void rtcInit(void)
{
    CSP_PRINTF_INFO("rtcInit...\n");
    mRtcCallback.tickRepeat = 0;
    mRtcCallback.alarmRepeat = 0;
    mRtcCallback.tickEn = 0;
    mRtcCallback.alarmEn = 0;
    mRtcCallback.alarmCallback = NULL;
    mRtcCallback.tickCallback = NULL;

    rtcDrvInit();
}

void rtcSetWakeupTimer(IN uint64_t delay)
{
    // To avoid compiler complaint
    (void)delay;
}

// Public API to set system time
void rtcSetSystemTime(IN uint32_t year, IN uint32_t mon, IN uint32_t date,
                      IN uint32_t day, IN uint32_t hour, IN uint32_t min,
                      IN uint32_t sec)
{
    if(!(year >= RTC_MIN_YEAR && year <= RTC_MAX_YEAR)) {
        CSP_PRINTF_ERROR("rtcSetSystemTime() - year(%d) is invalid\n", (int)year);
        CSP_ASSERT(0);
    }

    if(!(mon >= 1 && mon <= 12)) {
        CSP_PRINTF_ERROR("rtcSetSystemTime() - mon(%d) is invalid\n", (int)mon);
        CSP_ASSERT(0);
    }

    if(!(date >= 1 && date <= 31)) {
        CSP_PRINTF_ERROR("rtcSetSystemTime() - date(%d) is invalid\n", (int)date);
        CSP_ASSERT(0);
    }

    if(!(day >= 1 && day <= 7)) {
        CSP_PRINTF_ERROR("rtcSetSystemTime() - day(%d) is invalid\n", (int)day);
        CSP_ASSERT(0);
    }

    if(!(hour <= 23)) {
        CSP_PRINTF_ERROR("rtcSetSystemTime() - hour(%d) is invalid\n", (int)hour);
        CSP_ASSERT(0);
    }

    if(!(min <= 59)) {
        CSP_PRINTF_ERROR("rtcSetSystemTime() - min(%d) is invalid\n", (int)min);
        CSP_ASSERT(0);
    }

    if(!(sec <= 59)) {
        CSP_PRINTF_ERROR("rtcSetSystemTime() - sec(%d) is invalid\n", (int)sec);
        CSP_ASSERT(0);
    }

    rtcDrvSetSystemTime(year, mon, date, day, hour, min, sec, RTC_IGNORE_VALUE);
}

// Public API to get system time
void rtcGetSystemTime(OUT uint32_t * year, OUT uint32_t * mon,
                      OUT uint32_t * date, OUT uint32_t * day,
                      OUT uint32_t * hour, OUT uint32_t * min,
                      OUT uint32_t * sec)
{
    if(year == NULL) {
        CSP_PRINTF_ERROR("rtcGetSystemTime() - year is NULL\n");
        CSP_ASSERT(0);
    }

    if(mon == NULL) {
        CSP_PRINTF_ERROR("rtcGetSystemTime() - mon is NULL\n");
        CSP_ASSERT(0);
    }

    if(date == NULL) {
        CSP_PRINTF_ERROR("rtcGetSystemTime() - date is NULL\n");
        CSP_ASSERT(0);
    }

    if(day == NULL) {
        CSP_PRINTF_ERROR("rtcGetSystemTime() - day is NULL\n");
        CSP_ASSERT(0);
    }

    if(hour == NULL) {
        CSP_PRINTF_ERROR("rtcGetSystemTime() - hour is NULL\n");
        CSP_ASSERT(0);
    }

    if(min == NULL) {
        CSP_PRINTF_ERROR("rtcGetSystemTime() - min is NULL\n");
        CSP_ASSERT(0);
    }

    if(sec == NULL) {
        CSP_PRINTF_ERROR("rtcGetSystemTime() - sec is NULL\n");
        CSP_ASSERT(0);
    }

    rtcDrvGetSystemTime(year, mon, date, day, hour, min, sec);
}

// Public API to get system time in second-resolution
uint64_t rtcGetSystemTimeSec(void)
{
    uint32_t year = 0;
    uint32_t mon = 0;
    uint32_t date = 0;
    uint32_t day;
    uint32_t hour = 0;
    uint32_t min = 0;
    uint32_t sec = 0;
    uint64_t system_time_sec;

    rtcGetSystemTime(&year, &mon, &date, &day, &hour, &min, &sec);

#if 0
    /* HACK: Need to check how Host Linux kernel calculate system time */
    system_time_sec = rtcUtilGetSystemTimeSec(year, mon, date, hour, min, sec);
#else
    system_time_sec = rtcMkTime64(year, mon, date, hour, min, sec);
#endif

    return system_time_sec;
}

void rtcSetTimeStampNS(uint64_t time)
{
// delta is masked due to CTS
    return;
    int64_t delta = time - rtcGetTimeStampNS();

    if(delta > 1000000 || delta < -1000000) {
        rtcDrvSetInitTime(delta);
    }
}
// Public API to get system time in nano-seconds
uint64_t rtcGetTimeStampNS(void)
{
    static uint64_t last_system_time_nsec = 0;
    uint64_t system_time_nsec;

    __disable_irq();
    system_time_nsec = rtcDrvGetTimeStampNS();

    if(last_system_time_nsec > system_time_nsec) {
        CSP_PRINTF_ERROR("Oops!, last_system_time_nsec (%llu), system_time_nsec (%llu)\n", last_system_time_nsec, system_time_nsec);
    }

    last_system_time_nsec = system_time_nsec;
    __enable_irq();

    return system_time_nsec;
}

uint64_t rtcGetTimeStampUS(void)
{
    uint64_t system_time_usec;

    system_time_usec = cpuMathU64DivByU16(rtcDrvGetTimeStampNS(),1000);

    return system_time_usec;
}

#if (RTC_ALARM_SUPPORT)
// Public API to set alarm
void rtcSetAlarmTime(IN uint32_t year, IN uint32_t mon, IN uint32_t date,
                     IN uint32_t hour, IN uint32_t min, IN uint32_t sec,
                     OUT void (*callback) (void), IN uint8_t repeat)
{
    if(!((year == RTC_IGNORE_VALUE)
               || (year >= RTC_MIN_YEAR && year <= RTC_MAX_YEAR))) {
        CSP_PRINTF_ERROR("rtcSetAlarmTime() - year(%d) is invalid\n", (int)year);
        CSP_ASSERT(0);
    }

    if(!((mon == RTC_IGNORE_VALUE) || (mon >= 1 && mon <= 12))) {
        CSP_PRINTF_ERROR("rtcSetAlarmTime() - mon(%d) is invalid\n", (int)mon);
        CSP_ASSERT(0);
    }

    if(!((date == RTC_IGNORE_VALUE) || (date >= 1 && date <= 31))) {
        CSP_PRINTF_ERROR("rtcSetAlarmTime() - date(%d) is invalid\n", (int)date);
        CSP_ASSERT(0);
    }

    if(!((hour == RTC_IGNORE_VALUE) || (hour <= 23))) {
        CSP_PRINTF_ERROR("rtcSetAlarmTime() - hour(%d) is invalid\n", (int)hour);
        CSP_ASSERT(0);
    }

    if(!((min == RTC_IGNORE_VALUE) || (min <= 59))) {
        CSP_PRINTF_ERROR("rtcSetAlarmTime() - min(%d) is invalid\n", (int)min);
        CSP_ASSERT(0);
    }

    if(!((sec == RTC_IGNORE_VALUE) || (sec <= 59))) {
        CSP_PRINTF_ERROR("rtcSetAlarmTime() - sec(%d) is invalid\n", (int)sec);
        CSP_ASSERT(0);
    }

    // Should be called first to register callback function before starting alarm
    if ( callback != NULL )
        mRtcCallback.alarmCallback = callback;

    mRtcCallback.alarmRepeat = repeat;

    rtcDrvSetAlarmTime(year, mon, date, hour, min, sec, RTC_IGNORE_VALUE);

    rtcStartAlarm();
}

// Public API to get alarm time
void rtcGetAlarmTime(OUT uint32_t * year, OUT uint32_t * mon,
                     OUT uint32_t * date, OUT uint32_t * hour,
                     OUT uint32_t * min, OUT uint32_t * sec)
{
    if(year == NULL) {
        CSP_PRINTF_ERROR("rtcGetAlarmTime() - year is NULL\n");
        CSP_ASSERT(0);
    }

    if(mon == NULL) {
        CSP_PRINTF_ERROR("rtcGetAlarmTime() - mon is NULL\n");
        CSP_ASSERT(0);
    }

    if(date == NULL) {
        CSP_PRINTF_ERROR("rtcGetAlarmTime() - date is NULL\n");
        CSP_ASSERT(0);
    }

    if(hour == NULL) {
        CSP_PRINTF_ERROR("rtcGetAlarmTime() - hour is NULL\n");
        CSP_ASSERT(0);
    }

    if(min == NULL) {
        CSP_PRINTF_ERROR("rtcGetAlarmTime() - min is NULL\n");
        CSP_ASSERT(0);
    }

    if(sec == NULL) {
        CSP_PRINTF_ERROR("rtcGetAlarmTime() - sec is NULL\n");
        CSP_ASSERT(0);
    }

    *year = RTC_IGNORE_VALUE;
    *mon = RTC_IGNORE_VALUE;
    *date = RTC_IGNORE_VALUE;
    *hour = RTC_IGNORE_VALUE;
    *min = RTC_IGNORE_VALUE;
    *sec = RTC_IGNORE_VALUE;

    rtcDrvGetAlarmTime(year, mon, date, hour, min, sec);
}

void rtcStartAlarm(void)
{
    rtcDrvAlarmOnOff(true);
    mRtcCallback.alarmEn = 1;
#if 0
    NVIC_EnableIRQ(RTC_IRQn);
#endif
}

// Public API to stop alarm
void rtcStopAlarm(void)
{
    rtcDrvAlarmOnOff(false);
    mRtcCallback.alarmEn = 0;

#if 0
    if( mRtcCallback.tickEn == 0)
        NVIC_DisableIRQ(RTC_IRQn);
#endif
}

#endif /* #if (RTC_ALARM_SUPPORT) */

// Public API to set tick time
void rtcSetTickTime(IN uint32_t sec, IN uint32_t msec, IN uint32_t usec,
                    OUT void (*callback) (void), IN uint8_t repeat)
{
    if((sec == 0) && (msec == 0) && (usec == 0)) {
        CSP_PRINTF_ERROR("rtcSetTickTime() - sec(%d) is invalid\n", (int)sec);
        CSP_ASSERT(0);
    }

    if(usec >= 1000) {
	msec += usec / 1000;
	usec = usec % 1000;
    }

    if(msec >= 1000) {
	sec += msec / 1000;
	msec = msec % 1000;
    }

    rtcStopTick();

    // Should be called first to register callback function before starting tick
    mRtcCallback.tickRepeat = repeat;

    mRtcCallback.tickVal = 1000000 * sec + 100 * msec + usec;
    if( callback != NULL )
        mRtcCallback.tickCallback = callback;

    rtcDrvSetTickTime(sec, msec, usec);
    rtcStartTick();
}

// Public API to get current tick time
 uint64_t rtcGetCurrentTickTime(void)
{
    return rtcDrvGetCurrentTickTime();
}

uint64_t rtcGetTickTime(void)
{
    return mRtcCallback.tickVal;
}

int rtcGetTickRepeat(void)
{
    return mRtcCallback.tickRepeat;
}

void rtcStartTick(void)
{
    rtcDrvTickOnOff(true);
    mRtcCallback.tickEn = 1;
#if (RTC_TIMESTAMP_TICK == 1)
    NVIC_EnableIRQ(CHUB_RTC_TICK0_IRQn);
#else
    NVIC_EnableIRQ(CHUB_RTC_TICK1_IRQn);
#endif
}
// Driver API to stop tick
void rtcStopTick(void)
{
    rtcDrvTickOnOff(false);
    mRtcCallback.tickEn = 0;
#if (RTC_TIMESTAMP_TICK == 1)
    NVIC_DisableIRQ(CHUB_RTC_TICK0_IRQn);
#else
    NVIC_DisableIRQ(CHUB_RTC_TICK1_IRQn);
#endif
}

// Public API shutdown RTC.
void rtcDeinit(void)
{
}

#endif

// Called from nanohub hostintf
uint64_t rtcGetTime(void)
{
#if defined(SEOS)
    return platGetTicks();
#else
	return 0;
#endif
}

void rtcClearInt(uint32_t interrupt)
{
}

