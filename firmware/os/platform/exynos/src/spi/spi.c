/*----------------------------------------------------------------------------
 *      Exynos SoC  -  SPI
 *----------------------------------------------------------------------------
 *      Name:    spi.c
 *      Purpose: To implement SPI APIs
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http:www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <csp_common.h>
#if defined(SEOS)
  #include <errno.h>
  //#include <atomicBitset.h>
  //#include <atomic.h>
  #include <heap.h>
  //#include <platform.h>
  #include <spi_priv.h>
#endif
#include <plat/spi/spi.h>
#include <spiOS.h>
#include <spiDrv.h>
#include <usi.h>

/* save the enabled spi channel */
static uint8_t mSpiChannelStatus[SPI_CHANNEL_MAX];
static uint8_t m_portNum[16] = {0,};

/* Public API to initialize SPI. This should be called when OS starts */

uint32_t spiMasterInit(uint8_t *port)
{

    SPI_DEBUG_PRINT("\n\nspiInit port: %x\n", (unsigned int)*port);

    if(*port >= SPI_CHANNEL_MAX)
        return (uint32_t)-1;
    spiOSRequest(SPI_INIT, port);

    //spiDrvOpen(port);
    return 0;
}

int spiMasterTransfer(uint32_t port, uint32_t *txBuf, uint32_t *rxBuf, uint32_t size, const struct SpiMode *mode )
{
    int ret;
	struct spi_device spi;

    SPI_DEBUG_PRINT("spiTransfer\n");

    spiDrvOpen(port, &spi);

    if(txBuf && rxBuf)
        ret = spiDrvWriteRead(port, txBuf, rxBuf, size);
    else if(txBuf)
        ret = spiDrvWrite(port, txBuf, size);
    else
        ret = spiDrvRead(port, rxBuf, size);

    return ret;
}

int spiMasterDeinit(uint8_t *portNum)
{
    if(spiOSRequest(SPI_DEINIT, portNum))
		return -1;

	return 0;
}

void spiSetDeviceInfo(uint32_t portNum, const struct spi_device *spi_dev)
{
    spiDrvSetDeviceInfo(portNum, spi_dev);
    mSpiChannelStatus[portNum]=true;
}

static int exynosSpiRxTx(struct SpiDevice *dev, void *rxBuf, const void *txBuf,
        size_t size, const struct SpiMode *mode)
{
	uint32_t *portNum = (uint32_t *)dev->pdata;

    SPI_DEBUG_PRINT("%s: piTransfer - mode info\n",__func__);
    SPI_DEBUG_PRINT("%s: bitsPerWord: %d\n",__func__,  mode->bitsPerWord);
    SPI_DEBUG_PRINT("%s: cpha: %d\n",__func__,  mode->cpha);
    SPI_DEBUG_PRINT("%s: cpol: %d\n",__func__, mode->cpol);
    SPI_DEBUG_PRINT("%s: format: %d\n",__func__, mode->format);
    SPI_DEBUG_PRINT("%s: nssChange: %d\n",__func__,  mode->nssChange);
    SPI_DEBUG_PRINT("%s: speed: %d\n",__func__, (int)mode->speed);
    SPI_DEBUG_PRINT("%s: size: %d\n",__func__, (int)size);

    return spiMasterTransfer(*portNum, (uint32_t*)txBuf, (uint32_t*)rxBuf, (uint32_t)size, mode);
}

static int exynosSpiRelease(struct SpiDevice *dev)
{
	uint8_t portNum = *(uint8_t *)dev->pdata;

    if(spiMasterDeinit(&m_portNum[portNum]))
		return -1;

	m_portNum[portNum] = 0;

	return 0;
}

const struct SpiDevice_ops mExynosSpiOps = {
    .masterStartSync = NULL,
	  .masterRxTx = exynosSpiRxTx,
    .masterStopSync = NULL,

    .slaveStartSync = NULL,
    .slaveIdle = NULL,
    .slaveRxTx = NULL,
    .slaveStopSync = NULL,

    .slaveSetCsInterrupt = NULL,
    .slaveCsIsActive = NULL,

    .release = exynosSpiRelease,
};

int spiRequest(struct SpiDevice *dev, uint8_t busId)
{
	dev->ops = &mExynosSpiOps;
	m_portNum[busId] = busId;
	if(spiMasterInit(&m_portNum[busId])){
		return -1;
	}

	dev->pdata = (void *)&m_portNum[busId];
	return 0;
}

struct SpiDeviceState {
    struct SpiDevice dev;

    const struct SpiPacket *packets;
    size_t n;
    size_t currentBuf;
    struct SpiMode mode;

    SpiCbkF rxTxCallback;
    void *rxTxCookie;

    SpiCbkF finishCallback;
    void *finishCookie;

    int err;
};
#define SPI_DEVICE_TO_STATE(p) ((struct SpiDeviceState *)p)

static int spiMasterStart(struct SpiDeviceState *state,
        spi_cs_t cs, const struct SpiMode *mode)
{
    struct SpiDevice *dev = &state->dev;
    int ret = 0;
    int i;

    (void)cs;

    for(i=0 ; i< state->n ; i++)
    {
#if 0
    //Debug
    if(*(uint8_t*)state->packets[i].txBuf & 0x80)
        CSP_PRINTF_INFO("SPI Read >>> D[0]=%X \n", *(uint8_t *)state->packets[i].txBuf & 0x7F);
    else
        CSP_PRINTF_INFO("SPI Write >>> D[0]=%X, D[1]=%X \n", *(uint8_t *)state->packets[i].txBuf,
                *((uint8_t*)state->packets[i].txBuf+1));

    CSP_PRINTF_INFO("size: %d\n",(int)state->packets[i].size);
#endif

        ret = dev->ops->masterRxTx(dev, state->packets[i].rxBuf,
            state->packets[i].txBuf, state->packets[i].size, mode);

        if( state->packets[i].delay != 0)
            uSleep( state->packets[i].delay/1000 );
    }

	spiOSRunCallback(state->rxTxCallback, state->rxTxCookie, ret);

	return ret;
}

static int spiSetupRxTx(struct SpiDeviceState *state,
        const struct SpiPacket packets[], size_t n,
        SpiCbkF callback, void *cookie)
{
    state->packets = packets;
    state->n = n;
    state->currentBuf = 0;
    state->rxTxCallback = callback;
    state->rxTxCookie = cookie;

    return 0;
}

int spiMasterRequest(uint8_t busId, struct SpiDevice **dev_out)
{
    int ret = 0;

#if defined(SEOS)
    struct SpiDeviceState *state = heapAlloc(sizeof(*state));
#endif
    if (!state)
        return -ENOMEM;
    struct SpiDevice *dev = &state->dev;

    ret = spiRequest(dev, busId);
    if (ret < 0)
        goto err_request;

    if (!dev->ops->masterRxTx) {
        ret = -EOPNOTSUPP;
        goto err_opsupp;
    }

    *dev_out = dev;
    return 0;

err_opsupp:
    if (dev->ops->release)
        dev->ops->release(dev);
err_request:
#if defined(SEOS)
    heapFree(state);
#endif
    return ret;
}

int spiMasterRxTx(struct SpiDevice *dev, spi_cs_t cs,
        const struct SpiPacket packets[], size_t n,
        const struct SpiMode *mode, SpiCbkF callback,
        void *cookie)
{
    struct SpiDeviceState *state = SPI_DEVICE_TO_STATE(dev);
    int ret = 0;

    if (!n)
        return -EINVAL;

    ret = spiSetupRxTx(state, packets, n, callback, cookie);
    if (ret < 0)
        return ret;

    state->mode = *mode;

    return spiMasterStart(state, cs, mode);
}

int spiMasterRelease(struct SpiDevice *dev)
{
    struct SpiDeviceState *state = SPI_DEVICE_TO_STATE(dev);

    if (dev->ops->release) {
        int ret = dev->ops->release(dev);
        if (ret < 0)
            return ret;
    }

#if defined(SEOS)
    heapFree(state);
#endif
    return 0;
}

int spiOpen(uint32_t port)
{
    return spiDrvOpen(port, NULL);
}

int spiRead(uint32_t port, uint32_t addr, void* rxBuf, size_t size)
{
    return spiDrvReadData(port, addr, rxBuf, size);
}

int spiWrite(uint32_t port, uint32_t addr, void* txBuf, size_t size)
{
    return spiDrvWriteData(port, addr, txBuf, size);
}

void spiSaveState(void)
{
}

void spiRestoreState(void)
{
    int i;

    for (i = 0 ; i < SPI_CHUB_CHANNEL_MAX ; i++){
        if (mSpiChannelStatus[i])
            spiDrvOpen(i, NULL);
    }
}

void spiCmgpSaveState(void)
{
}

void spiCmgpRestoreState(void)
{
    int i;

    for (i = SPI_CHUB_CHANNEL_MAX ; i < SPI_CHANNEL_MAX ; i++){
        if (mSpiChannelStatus[i])
            spiDrvOpen(i, NULL);
    }
}
