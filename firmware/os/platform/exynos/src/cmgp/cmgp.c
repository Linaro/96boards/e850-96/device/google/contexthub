/*----------------------------------------------------------------------------
 *      Exynos SoC  -  CMGP
 *----------------------------------------------------------------------------
 *      Name:    cmgp.c
 *      Purpose: To implement CMGP APIs
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <cmgp.h>
#include <cmgpDrv.h>
#include <cmgpOS.h>

#include CSP_SOURCE(cmgp)

// Public API to initialize GPIO. This should be called when OS starts
void cmgpInit(void)
{
    cmgpDrvInit();
}

void cmgpEnableInterrupt(uint32_t intNum)
{
    cmgpDrvEnableInterrupt( intNum );
}

void cmgpDisableInterrupt(uint32_t intNum)
{
    cmgpDrvDisableInterrupt( intNum );
}

void cmgpSetIrqPriority(uint32_t intNum, uint32_t priority)
{
    cmgpDrvSetIrqPriority( intNum, priority );
}

void cmgpGetIrqInfo(CmgpIrqInfoType *irqInfo)
{
    irqInfo->irq = (CmgpIrqType)cmgpDrvGetIrqNum();
    irqInfo->irqGroup = cmgpGetIrqGroup( irqInfo->irq );
    irqInfo->irqSource = cmgpGetIrqSource( irqInfo->irq );
}

