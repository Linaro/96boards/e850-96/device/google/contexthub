/*----------------------------------------------------------------------------
 *      Exynos SoC  -  CMGP
 *----------------------------------------------------------------------------
 *      Name:    cmgp.c
 *      Purpose: To implement CMGP APIs for 9610
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <cmgp.h>
#include <gpio9610.h>
#include <usi.h>
#include <i2c.h>

uint32_t mCmgpIrqSource[64] = {
    [CMGP_USI0_IRQ] = USI_CMGP0,
    [CMGP_I2C0_IRQ] = I2C_CMGP1,
    [CMGP_USI1_IRQ] = USI_CMGP1,
    [CMGP_I2C1_IRQ] = I2C_CMGP3,
    [CMGP_USI2_IRQ] = USI_CMGP2,
    [CMGP_I2C2_IRQ] = I2C_CMGP5,
    [CMGP_USI3_IRQ] = USI_CMGP3,
    [CMGP_I2C3_IRQ] = I2C_CMGP7,

    [CMGP_ADC_IRQ] = CMGP_ADC_IRQ,

    [CMGP_GPIOM0_0_IRQ] = NWEINT_GPM00_EINT_0,
    [CMGP_GPIOM0_1_IRQ] = NWEINT_GPM01_EINT_0,
    [CMGP_GPIOM0_2_IRQ] = NWEINT_GPM02_EINT_0,
    [CMGP_GPIOM0_3_IRQ] = NWEINT_GPM03_EINT_0,
    [CMGP_GPIOM0_4_IRQ] = NWEINT_GPM04_EINT_0,
    [CMGP_GPIOM0_5_IRQ] = NWEINT_GPM05_EINT_0,
    [CMGP_GPIOM0_6_IRQ] = NWEINT_GPM06_EINT_0,
    [CMGP_GPIOM0_7_IRQ] = NWEINT_GPM07_EINT_0,
    [CMGP_GPIOM0_8_IRQ] = NWEINT_GPM08_EINT_0,
    [CMGP_GPIOM0_9_IRQ] = NWEINT_GPM09_EINT_0,
    [CMGP_GPIOM1_0_IRQ] = NWEINT_GPM10_EINT_0,
    [CMGP_GPIOM1_1_IRQ] = NWEINT_GPM11_EINT_0,
    [CMGP_GPIOM1_2_IRQ] = NWEINT_GPM12_EINT_0,
    [CMGP_GPIOM1_3_IRQ] = NWEINT_GPM13_EINT_0,
    [CMGP_GPIOM1_4_IRQ] = NWEINT_GPM14_EINT_0,
    [CMGP_GPIOM1_5_IRQ] = NWEINT_GPM15_EINT_0,
    [CMGP_GPIOM1_6_IRQ] = NWEINT_GPM16_EINT_0,
    [CMGP_GPIOM1_7_IRQ] = NWEINT_GPM17_EINT_0,
    [CMGP_GPIOM1_8_IRQ] = NWEINT_GPM18_EINT_0,
    [CMGP_GPIOM1_9_IRQ] = NWEINT_GPM19_EINT_0,
    [CMGP_GPIOM2_0_IRQ] = NWEINT_GPM20_EINT_0,
    [CMGP_GPIOM2_1_IRQ] = NWEINT_GPM21_EINT_0,
    [CMGP_GPIOM2_2_IRQ] = NWEINT_GPM22_EINT_0,
    [CMGP_GPIOM2_3_IRQ] = NWEINT_GPM23_EINT_0,
    [CMGP_GPIOM2_4_IRQ] = NWEINT_GPM24_EINT_0,
    [CMGP_GPIOM2_5_IRQ] = NWEINT_GPM25_EINT_0,
};

static CmgpIrqGroupType cmgpGetIrqGroup(CmgpIrqType irq);
static uint32_t cmgpGetIrqSource( CmgpIrqType irq );

static CmgpIrqGroupType cmgpGetIrqGroup(CmgpIrqType irq)
{
    switch( irq ) {
        case CMGP_USI0_IRQ:
        case CMGP_USI1_IRQ:
        case CMGP_USI2_IRQ:
        case CMGP_USI3_IRQ:
            return CMGP_IRQ_GROUP_USI;
        case CMGP_I2C0_IRQ:
        case CMGP_I2C1_IRQ:
        case CMGP_I2C2_IRQ:
        case CMGP_I2C3_IRQ:
            return CMGP_IRQ_GROUP_I2C;
        case CMGP_ADC_IRQ:
            return CMGP_IRQ_GROUP_ADC;
        default:
            return CMGP_IRQ_GROUP_GPIO;
    }
}

static uint32_t cmgpGetIrqSource( CmgpIrqType irq )
{
    return (uint32_t)mCmgpIrqSource[irq];
}

