/*----------------------------------------------------------------------------
 *      Exynos SoC  -  CMGP
 *----------------------------------------------------------------------------
 *      Name:    cmgp.h
 *      Purpose: To expose CMGP APIs and define macros for 9630
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __CMGP9630_H__
#define __CMGP9630_H__

typedef enum{
    CMGP_GPIOM0_0_IRQ = 0,
    CMGP_GPIOM0_1_IRQ = 1,
    CMGP_GPIOM0_2_IRQ = 2,
    CMGP_GPIOM0_3_IRQ = 3,
    CMGP_GPIOM0_4_IRQ = 4,
    CMGP_GPIOM0_5_IRQ = 5,
    CMGP_GPIOM0_6_IRQ = 6,
    CMGP_GPIOM0_7_IRQ = 7,
    CMGP_GPIOM0_8_IRQ = 8,
    CMGP_GPIOM0_9_IRQ = 9,

    CMGP_GPIOM1_0_IRQ = 10,
    CMGP_GPIOM1_1_IRQ = 11,
    CMGP_GPIOM1_2_IRQ = 12,
    CMGP_GPIOM1_3_IRQ = 13,
    CMGP_GPIOM1_4_IRQ = 14,
    CMGP_GPIOM1_5_IRQ = 15,

    CMGP_USI0_IRQ = 32,
    CMGP_I2C0_IRQ = 33,
    CMGP_USI1_IRQ = 34,
    CMGP_I2C1_IRQ = 35,
    CMGP_USI2_IRQ = 36,
    CMGP_I2C2_IRQ = 37,
    CMGP_USI3_IRQ = 38,
    CMGP_I2C3_IRQ = 39,
    CMGP_I3C_IRQ = 40,

    CMGP_ADC_IRQ = 48,


}CmgpIrqType;

#endif

