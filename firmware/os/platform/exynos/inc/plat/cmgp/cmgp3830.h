/*----------------------------------------------------------------------------
 *      Exynos SoC  -  CMGP
 *----------------------------------------------------------------------------
 *      Name:    cmgp.h
 *      Purpose: To expose CMGP APIs and define macros for 3830
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __CMGP3830_H__
#define __CMGP3830_H__

//
// !!! Need to update
//

typedef enum{
    CMGP_USI0_IRQ = 0,
    CMGP_USI1_IRQ = 2,

    CMGP_ADC_IRQ = 16,

    CMGP_GPIOM0_0_IRQ = 20,
    CMGP_GPIOM0_1_IRQ = 21,
    CMGP_GPIOM0_2_IRQ = 22,
    CMGP_GPIOM0_3_IRQ = 23,
    CMGP_GPIOM0_4_IRQ = 24,
    CMGP_GPIOM0_5_IRQ = 25,
    CMGP_GPIOM0_6_IRQ = 26,
    CMGP_GPIOM0_7_IRQ = 27,

}CmgpIrqType;

#endif

