/*----------------------------------------------------------------------------
 *      Exynos SoC  -  CMGP
 *----------------------------------------------------------------------------
 *      Name:    cmgp.h
 *      Purpose: To expose CMGP APIs and define macros for 9610
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __CMGP9610_H__
#define __CMGP9610_H__

//
// !!! Need to update
//

typedef enum{
    CMGP_USI0_IRQ = 0,
    CMGP_I2C0_IRQ = 1,
    CMGP_USI1_IRQ = 2,
    CMGP_I2C1_IRQ = 3,
    CMGP_USI2_IRQ = 4,
    CMGP_I2C2_IRQ = 5,
    CMGP_USI3_IRQ = 6,
    CMGP_I2C3_IRQ = 7,
    CMGP_I2C4_IRQ = 8,
    CMGP_I2C5_IRQ = 9,
    CMGP_I2C6_IRQ = 10,

    CMGP_ADC_IRQ = 16,

    CMGP_GPIOM0_0_IRQ = 28,
    CMGP_GPIOM0_1_IRQ = 29,
    CMGP_GPIOM0_2_IRQ = 30,
    CMGP_GPIOM0_3_IRQ = 31,
    CMGP_GPIOM0_4_IRQ = 32,
    CMGP_GPIOM0_5_IRQ = 33,
    CMGP_GPIOM0_6_IRQ = 34,
    CMGP_GPIOM0_7_IRQ = 35,
    CMGP_GPIOM0_8_IRQ = 36,
    CMGP_GPIOM0_9_IRQ = 37,

    CMGP_GPIOM1_0_IRQ = 38,
    CMGP_GPIOM1_1_IRQ = 39,
    CMGP_GPIOM1_2_IRQ = 40,
    CMGP_GPIOM1_3_IRQ = 41,
    CMGP_GPIOM1_4_IRQ = 42,
    CMGP_GPIOM1_5_IRQ = 43,
    CMGP_GPIOM1_6_IRQ = 44,
    CMGP_GPIOM1_7_IRQ = 45,
    CMGP_GPIOM1_8_IRQ = 46,
    CMGP_GPIOM1_9_IRQ = 47,

    CMGP_GPIOM2_0_IRQ = 48,
    CMGP_GPIOM2_1_IRQ = 49,
    CMGP_GPIOM2_2_IRQ = 50,
    CMGP_GPIOM2_3_IRQ = 51,
    CMGP_GPIOM2_4_IRQ = 52,
    CMGP_GPIOM2_5_IRQ = 53,

}CmgpIrqType;

#endif

