#ifndef _CPU_H_
#define _CPU_H_

uint64_t cpuIntsOff(void);
uint64_t cpuIntsOn(void);
void cpuIntsRestore(uint64_t state);

#endif
