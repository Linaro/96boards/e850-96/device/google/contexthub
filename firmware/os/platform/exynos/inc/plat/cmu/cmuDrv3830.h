/*----------------------------------------------------------------------------
 *      Exynos SoC  -  CMU
 *----------------------------------------------------------------------------
 *      Name:    cmuDrv.c
 *      Purpose: To implement CMU driver functions for 3830
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __CMU_DRIVER_3830_H__
#define __CMU_DRIVER_3830_H__


#define    REG_CMU_PLL_CON0_MUX_CLKCMU_CHUB_BUS_USER                                                 (CMU_BASE_ADDRESS + 0x0600)
#define    REG_CMU_PLL_CON1_MUX_CLKCMU_CHUB_BUS_USER                                                 (CMU_BASE_ADDRESS + 0x0604)
#define    REG_CMU_PLL_CON0_MUX_CLK_RCO_CHUB_USER                                                    (CMU_BASE_ADDRESS + 0x0610)
#define    REG_CMU_PLL_CON1_MUX_CLK_RCO_CHUB_USER                                                    (CMU_BASE_ADDRESS + 0x0614)

#define    REG_CMU_CHUB_CMU_CHUB_CONTROLLER_OPTION                                                   (CMU_BASE_ADDRESS + 0x0800)
#define    REG_CMU_CLKOUT_CON_BLK_CHUB_CMU_CHUB_CLKOUT                                               (CMU_BASE_ADDRESS + 0x0810)

#define    REG_CMU_CHUB_SPARE0                                                                       (CMU_BASE_ADDRESS + 0x0a00)
#define    REG_CMU_CHUB_SPARE1                                                                       (CMU_BASE_ADDRESS + 0x0a04)

#define    REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_BUS                                                      (CMU_BASE_ADDRESS + 0x1000)
#define    REG_CMU_CLK_CON_MUX_MUX_CLK_CHUB_TIMER_FCLK                                               (CMU_BASE_ADDRESS + 0x1004)

#define    REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_BUS                                                      (CMU_BASE_ADDRESS + 0x1800)
#define    REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_DMIC                                                     (CMU_BASE_ADDRESS + 0x1804)
#define    REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_DMIC_IF                                                  (CMU_BASE_ADDRESS + 0x1808)
#define    REG_CMU_CLK_CON_DIV_DIV_CLK_CHUB_DMIC_IF_DIV2                                             (CMU_BASE_ADDRESS + 0x180C)

#define    REG_CMU_CLK_CON_GAT_CLK_BLK_CHUB_UID_CHUB_CMU_CHUB_IPCLKPORT_PCLK                         (CMU_BASE_ADDRESS + 0x2000)
#define    REG_CMU_CLK_CON_GAT_CLK_BLK_CHUB_UID_RSTNSYNC_CLK_CHUB_OSCCLK_RCO_IPCLKPORT_CLK           (CMU_BASE_ADDRESS + 0x2004)
#define    REG_CMU_CLK_CON_GAT_CLK_BLK_CHUB_UID_RSTNSYNC_CLK_CHUB_RTCCLK_IPCLKPORT_CLK               (CMU_BASE_ADDRESS + 0x2008)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_AHB_BUSMATRIX_CHUB_IPCLKPORT_HCLK                   (CMU_BASE_ADDRESS + 0x200C)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_BAAW_C_CHUB_IPCLKPORT_I_PCLK                        (CMU_BASE_ADDRESS + 0x2010)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_BAAW_D_CHUB_IPCLKPORT_I_PCLK                        (CMU_BASE_ADDRESS + 0x2014)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_BPS_AXI_LP_CHUB_IPCLKPORT_I_CLK                     (CMU_BASE_ADDRESS + 0x2018)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_BPS_AXI_P_CHUB_IPCLKPORT_I_CLK                      (CMU_BASE_ADDRESS + 0x201C)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_CM4_CHUB_IPCLKPORT_FCLK                             (CMU_BASE_ADDRESS + 0x2020)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_DMIC_AHB0_IPCLKPORT_HCLK                            (CMU_BASE_ADDRESS + 0x2024)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_DMIC_AHB0_IPCLKPORT_PCLK                            (CMU_BASE_ADDRESS + 0x2028)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_DMIC_IF_IPCLKPORT_DMIC_CLK                          (CMU_BASE_ADDRESS + 0x202C)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_DMIC_IF_IPCLKPORT_DMIC_IF_CLK                       (CMU_BASE_ADDRESS + 0x2030)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_DMIC_IF_IPCLKPORT_PCLK                              (CMU_BASE_ADDRESS + 0x2034)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_D_TZPC_CHUB_IPCLKPORT_PCLK                          (CMU_BASE_ADDRESS + 0x2038)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_HWACG_SYS_DMIC0_IPCLKPORT_HCLK                      (CMU_BASE_ADDRESS + 0x203C)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_HWACG_SYS_DMIC0_IPCLKPORT_HCLK_BUS                  (CMU_BASE_ADDRESS + 0x2040)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_LHM_AXI_LP_CHUB_IPCLKPORT_I_CLK                     (CMU_BASE_ADDRESS + 0x2044)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_LHM_AXI_P_CHUB_IPCLKPORT_I_CLK                      (CMU_BASE_ADDRESS + 0x2048)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_LHS_AXI_C_CHUB_IPCLKPORT_I_CLK                      (CMU_BASE_ADDRESS + 0x204C)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_LHS_AXI_D_CHUB_IPCLKPORT_I_CLK                      (CMU_BASE_ADDRESS + 0x2050)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_PWM_CHUB_IPCLKPORT_I_PCLK_S0                        (CMU_BASE_ADDRESS + 0x2054)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_RSTNSYNC_CLK_CHUB_BUS_IPCLKPORT_CLK                 (CMU_BASE_ADDRESS + 0x2058)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_RSTNSYNC_CLK_CHUB_DMIC_IF_IPCLKPORT_CLK             (CMU_BASE_ADDRESS + 0x205C)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_RSTNSYNC_CLK_CHUB_TIMER_FCLK_IPCLKPORT_CLK          (CMU_BASE_ADDRESS + 0x2060)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_SWEEPER_C_CHUB_IPCLKPORT_ACLK                       (CMU_BASE_ADDRESS + 0x2064)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_SWEEPER_D_CHUB_IPCLKPORT_ACLK                       (CMU_BASE_ADDRESS + 0x2068)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_SYSREG_CHUB_IPCLKPORT_PCLK                          (CMU_BASE_ADDRESS + 0x206C)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_TIMER_CHUB_IPCLKPORT_PCLK                           (CMU_BASE_ADDRESS + 0x2070)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_U_DMIC_CLK_SCAN_MUX_IPCLKPORT_D0                    (CMU_BASE_ADDRESS + 0x2074)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_WDT_CHUB_IPCLKPORT_PCLK                             (CMU_BASE_ADDRESS + 0x2078)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_XHB_LP_CHUB_IPCLKPORT_CLK                           (CMU_BASE_ADDRESS + 0x207C)
#define    REG_CMU_CLK_CON_GAT_GOUT_BLK_CHUB_UID_XHB_P_CHUB_IPCLKPORT_CLK                            (CMU_BASE_ADDRESS + 0x2080)

#define    REG_CMU_DMYQCH_CON_DMIC_IF_QCH_DMIC_CLK                                                   (CMU_BASE_ADDRESS + 0x3000)
#define    REG_CMU_DMYQCH_CON_U_DMIC_CLK_SCAN_MUX_QCH                                                (CMU_BASE_ADDRESS + 0x3004)
#define    REG_CMU_MEMPG_CON_AHB_BUSMATRIX_CHUB_0                                                    (CMU_BASE_ADDRESS + 0x3008)
#define    REG_CMU_MEMPG_CON_AHB_BUSMATRIX_CHUB_1                                                    (CMU_BASE_ADDRESS + 0x300C)
#define    REG_CMU_PCH_CON_LHM_AXI_LP_CHUB_PCH                                                       (CMU_BASE_ADDRESS + 0x3010)
#define    REG_CMU_PCH_CON_LHM_AXI_P_CHUB_PCH                                                        (CMU_BASE_ADDRESS + 0x3014)
#define    REG_CMU_PCH_CON_LHS_AXI_C_CHUB_PCH                                                        (CMU_BASE_ADDRESS + 0x3018)
#define    REG_CMU_PCH_CON_LHS_AXI_D_CHUB_PCH                                                        (CMU_BASE_ADDRESS + 0x301C)
#define    REG_CMU_QCH_CON_BAAW_C_CHUB_QCH                                                           (CMU_BASE_ADDRESS + 0x3020)
#define    REG_CMU_QCH_CON_BAAW_D_CHUB_QCH                                                           (CMU_BASE_ADDRESS + 0x3024)
#define    REG_CMU_QCH_CON_CHUB_CMU_CHUB_QCH                                                         (CMU_BASE_ADDRESS + 0x3028)
#define    REG_CMU_QCH_CON_CM4_CHUB_QCH                                                              (CMU_BASE_ADDRESS + 0x302C)
#define    REG_CMU_QCH_CON_DMIC_AHB0_QCH                                                             (CMU_BASE_ADDRESS + 0x3030)
#define    REG_CMU_QCH_CON_DMIC_IF_QCH_PCLK                                                          (CMU_BASE_ADDRESS + 0x3034)
#define    REG_CMU_QCH_CON_D_TZPC_CHUB_QCH                                                           (CMU_BASE_ADDRESS + 0x3038)
#define    REG_CMU_QCH_CON_HWACG_SYS_DMIC0_QCH                                                       (CMU_BASE_ADDRESS + 0x303C)
#define    REG_CMU_QCH_CON_LHM_AXI_LP_CHUB_QCH                                                       (CMU_BASE_ADDRESS + 0x3040)
#define    REG_CMU_QCH_CON_LHM_AXI_P_CHUB_QCH                                                        (CMU_BASE_ADDRESS + 0x3044)
#define    REG_CMU_QCH_CON_LHS_AXI_C_CHUB_QCH                                                        (CMU_BASE_ADDRESS + 0x3048)
#define    REG_CMU_QCH_CON_LHS_AXI_D_CHUB_QCH                                                        (CMU_BASE_ADDRESS + 0x304C)
#define    REG_CMU_QCH_CON_PWM_CHUB_QCH                                                              (CMU_BASE_ADDRESS + 0x3050)
#define    REG_CMU_QCH_CON_SWEEPER_C_CHUB_QCH                                                        (CMU_BASE_ADDRESS + 0x3054)
#define    REG_CMU_QCH_CON_SWEEPER_D_CHUB_QCH                                                        (CMU_BASE_ADDRESS + 0x3058)
#define    REG_CMU_QCH_CON_SYSREG_CHUB_QCH                                                           (CMU_BASE_ADDRESS + 0x305C)
#define    REG_CMU_QCH_CON_TIMER_CHUB_QCH                                                            (CMU_BASE_ADDRESS + 0x3060)
#define    REG_CMU_QCH_CON_WDT_CHUB_QCH                                                              (CMU_BASE_ADDRESS + 0x3064)

#define    REG_CMU_QUEUE_CTRL_REG_BLK_CHUB_CMU_CHUB                                                  (CMU_BASE_ADDRESS + 0x3C00)

#define    REG_CUM_CLK_CON_MUX_MUX_CLK_CMGP_USI_CMGP0                                                (CMU_CMGP_BASE_ADDRESS + 0x1004)
#define    REG_CUM_CLK_CON_MUX_MUX_CLK_CMGP_USI_CMGP1                                                (CMU_CMGP_BASE_ADDRESS + 0x1008)

#define    REG_CMU_CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP0                                                (CMU_CMGP_BASE_ADDRESS + 0x1804)
#define    REG_CMU_CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP1                                                (CMU_CMGP_BASE_ADDRESS + 0x1808)

// PLL_CON0_MUX_CLKCMU_CHUB_BUS_USER

// -- Select
#define SELECT_OSCCLK_RCO                                   (0 << 4)
#define SELECT_CLKCMU_CHUB_BUS                              (1 << 4)
#define MUX_CLKCMU_CHUB_BUS_USER_IS_BUSY                    0x10000



// PLL_CON1_MUX_CLKCMU_CHUB_BUS_USER

// -- Enable or disable HWACG
#define MUX_CLKCMU_CHUB_BUS_USER_HWACG_DISABLE              (0 << 28)
#define MUX_CLKCMU_CHUB_BUS_USER_HWACG_ENABLE               (1 << 28)


// PLL_CON0_MUX_CLK_RCO_CHUB_BUS_USER

// -- Select
#define SELECT_OSCCLK_RCO_CHUB__ALV                         (0 << 4)
#define SELECT_CLK_RCO_CHUB__ALV                            (1 << 4)
#define MUX_CLK_RCO_CHUB_BUS_USER_IS_BUSY                   0x10000



// PLL_CON1_MUX_CLK_RCO_CHUB_USER

// -- Enable or disable HWACG
#define MUX_CLK_RCO_CHUB_USER_HWACG_DISABLE                 (0 << 28)
#define MUX_CLK_RCO_CHUB_USER_HWACG_ENABLE                  (1 << 28)



// CLK_CON_MUX_MUX_CLK_CHUB_BUS

// -- Select
#define SELECT_MUX_CLK_RCO_CHUB_USER                        (0 << 0)
#define SELECT_MUX_CLKCMU_CHUB_BUS_USER                     (1 << 0)
#define MUX_CLK_CHUB_BUS_IS_BUSY                            0x10000



// CLK_CON_MUX_MUX_CLK_CHUB_TIMER_FCLK

// -- Select
#define SELECT_CLK_CHUB_OSCCLK_RCO                          (0 << 0)
#define SELECT_CLK_CHUB_RTCCLK                              (1 << 0)
#define MUX_CLK_CHUB_TIMER_FCLK_IS_BUSY                     0x10000



// CHUB_CMU_CHUB_CONTROLLER_OPTION

// -- Enable or disable pwer management
#define CHUB_CONTROLLER_OPTION_PM_DISABLE                   (0 << 29)
#define CHUB_CONTROLLER_OPTION_PM_ENABLE                    (1 << 29)

// -- Enable or disable HWACG
#define CHUB_CONTROLLER_OPTION_HWACG_DISABLE                (0 << 28)
#define CHUB_CONTROLLER_OPTION_HWACG_ENABLE                 (1 << 28)



// CLK_CON_DIV_DIV_CLK_CHUB_BUS

// -- DIVRATIO
#define CLK_CON_DIV_DIV_CLK_CHUB_BUS_DEFAULT_DIVRATIO       (0 << 0)
#define DIV_CLK_CHUB_BUS_IS_BUSY                            0x10000

// -- Enable or disable HWACG
#define DIV_CLK_CHUB_BUS_HWACG_DISABLE                      (0 << 28)
#define DIV_CLK_CHUB_BUS_HWACG_ENABLE                       (1 << 28)


// QCH_CON_xxx

// -- ENABLE
#define QCH_CON_xxx_ENABLE                                  (1 << 0)

// -- IGNORE_FORCE_PM_EN
#define QCH_CON_xxx_IGNORE_FORCE_PM_EN                      (1 << 2)



// CLK_CON_xxx

// -- ENABLE_AUTOMATIC_CLKGATING
#define CLK_CON_xxx_ENABLE_AUTOMATIC_CLKGATING              (1 << 28)



// CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP0
#define SELECT_CLK_CMGP_USI0_CLK_RCO                        (0 << 0)
#define SELECT_CLK_CMGP_USI0_CLK_CMGP_BUS                   (1 << 0)
#define DIV_CLK_CMGP_USI_CMGP0_IS_BUSY                      0x10000



// CLK_CON_DIV_DIV_CLK_CMGP_USI_CMGP1
#define SELECT_CLK_CMGP_USI1_CLK_RCO                        (0 << 0)
#define SELECT_CLK_CMGP_USI1_CLK_CMGP_BUS                   (1 << 0)
#define DIV_CLK_CMGP_USI_CMGP1_IS_BUSY                      0x10000

#endif
