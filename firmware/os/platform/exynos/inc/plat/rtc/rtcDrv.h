/*----------------------------------------------------------------------------
 *      Exynos SoC  -  RTC
 *----------------------------------------------------------------------------
 *      Name:    rtcDrv.h
 *      Purpose: To expose RTC driver functions
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __RTC_DRIVER_H__
#define __RTC_DRIVER_H__

#include <csp_common.h>

// Driver APIs
void rtcDrvInit(void);
void rtcDrvSetSystemTime(IN uint32_t year, IN uint32_t mon, IN uint32_t date,
                         IN uint32_t day, IN uint32_t hour, IN uint32_t min,
                         IN uint32_t sec, IN uint32_t ignoreVal);
void rtcDrvGetSystemTime(OUT uint32_t * year, OUT uint32_t * mon,
                         OUT uint32_t * date, OUT uint32_t * day,
                         OUT uint32_t * hour, OUT uint32_t * min,
                         OUT uint32_t * sec);
void rtcDrvSetInitTime(int64_t delta);
uint64_t rtcDrvGetTimeStampNS(void);
void rtcDrvSetAlarmTime(IN uint32_t year, IN uint32_t mon, IN uint32_t date,
                        IN uint32_t hour, IN uint32_t min, IN uint32_t sec,
                        IN uint32_t ignoreVal);
void rtcDrvGetAlarmTime(OUT uint32_t * year, OUT uint32_t * mon,
                        OUT uint32_t * date, OUT uint32_t * hour,
                        OUT uint32_t * min, OUT uint32_t * sec);
void rtcDrvAlarmOnOff(IN bool on);
void rtcDrvSetTickTime(IN uint32_t sec, IN uint32_t msec, IN uint32_t usec);
uint64_t rtcDrvGetCurrentTickTime(void);
void rtcDrvTickOnOff(IN bool on);
void rtcDrvClearInt(uint32_t interrupt);
uint32_t rtcDrvWhichInt(void);

void rtcDrvSetTick0Time(IN uint32_t sec, IN uint32_t mSec, IN uint32_t uSec);
uint64_t rtcDrvGetCurrentTick0Time(void);
void rtcDrvTick0OnOff(IN bool on);

#endif /* __RTC_DRIVER_H__ */
