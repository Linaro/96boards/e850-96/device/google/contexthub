/*----------------------------------------------------------------------------
 *      Exynos SoC  -  RTC
 *----------------------------------------------------------------------------
 *      Name:    rtcOS.h
 *      Purpose: OS-dependent part for RTC driver
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __RTC_OS_H__
#define __RTC_OS_H__

#include <csp_common.h>

#define RTC_INIT                0
#define RTC_SET_ALARM           1
#define RTC_STOP_ALARM          2
#define RTC_SET_TICK            3
#define RTC_STOP_TICK           4
#define RTC_DEINIT              5

struct RtcTickIsr {
    void (*func)(void);
};

void rtc_IRQHandler(void);
void rtcSetHandler(uint8_t rtcNum, void *isr);
void rtcUnsetHandler(uint8_t rtcNum);
void rtcTick0_IRQHandler(void);
void rtcTick1_IRQHandler(void);
void rtc_callback(void);
void logbuf_callback(void);
#endif
