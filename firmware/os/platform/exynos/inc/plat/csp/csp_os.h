/*----------------------------------------------------------------------------
 *      Exynos SoC  -  CSP
 *----------------------------------------------------------------------------
 *      Name:    csp_os.h
 *      Purpose: To define os api
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __CSP_OS_H__
#define __CSP_OS_H__

#if defined(SEOS)
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <seos.h>
#include <csp_printf.h>
#endif

enum error_type {
	ERR_ASSERT,
	ERR_FAULT,
	ERR_REBOOT,
	ERR_ERROR,
	ERR_PANIC,
};

void *cspHeapAlloc(uint32_t sz);
void *cspHeapRealloc(void *ptr, uint32_t newSz);
void cspHeapFree(void *ptr);
uint64_t cspTimeGetTime(void);
uint32_t cspTimeSetTime(uint64_t length, uint32_t jitterPpm, uint32_t driftPpm,
                            void *cookie, bool oneShot);
bool cspTimeCancelTime(uint32_t timerId);
void uSleep(uint32_t us);
void mSleep(uint32_t ms);
#define msleep(ms) mSleep(ms);
void cspNotify(enum error_type);

#ifdef USE_CSP_SNAPSHOT
enum csp_log_id {
	csp_log_mb_send,
	csp_log_mb_rcv,
	csp_log_sleep,
	csp_log_wakeup,
	csp_log_max,
};

void cspSetLogging(enum csp_log_id id);
void cspShowLogging(enum csp_log_id id);
#else
#define cspSetLogging(a) ((void)0)
#define cspShowLogging(a) ((void)0)
#endif
#endif
