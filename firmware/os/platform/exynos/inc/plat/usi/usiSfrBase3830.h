/*----------------------------------------------------------------------------
 *      Exynos SoC  -  USI
 *----------------------------------------------------------------------------
 *      Name:    usiSfrBase3830.h
 *      Purpose: To expose USI driver functions
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __USI_SFR_BASE3830_H__
#define __USI_SFR_BASE3830_H__

#include <csp_common.h>

static UsiSfrBaseType mUsiSfrBase[USI_CHANNEL_MAX] = {
    {USI0_CMGP_BASE_ADDRESS, SYSREG_CMGP_BASE_ADDRESS + 0x2000},
    {USI1_CMGP_BASE_ADDRESS, SYSREG_CMGP_BASE_ADDRESS + 0x2010},
};

#endif /* __USI_SFR_BASE_H__ */

