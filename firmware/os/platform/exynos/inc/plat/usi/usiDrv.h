/*----------------------------------------------------------------------------
 *      Exynos SoC  -  USI
 *----------------------------------------------------------------------------
 *      Name:    usiDrv.h
 *      Purpose: To expose USI driver functions
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __USI_DRIVER_H__
#define __USI_DRIVER_H__

#include <csp_common.h>

int32_t usiDrvOpen(IN uint32_t port, IN UsiProtocolType protocol, IN CmgpHwacgControlType hwacg);
int32_t usiDrvSetProtocol(IN uint32_t port, IN UsiProtocolType protocol, IN CmgpHwacgControlType hwacg);
UsiProtocolType usiDrvGetProtocol(IN uint32_t port);
void usiDrvClose(IN uint32_t port);

#endif

