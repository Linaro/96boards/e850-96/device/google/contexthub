/*----------------------------------------------------------------------------
 *      Exynos SoC  -  USI
 *----------------------------------------------------------------------------
 *      Name:    usiSfrBase.h
 *      Purpose: To expose USI driver functions
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __USI_SFR_BASE_H__
#define __USI_SFR_BASE_H__

#include <csp_common.h>

// SFR OFFSETs
#define    REG_USI_CONFIG               (0xC0)
#define    REG_USI_CON                  (0xC4)
#define    REG_USI_OPTION               (0xC8)
#define    REG_USI_USI_VERSION          (0xCC)
#define    REG_USI_UART_VERSION         (0xD0)
#define    REG_USI_SPI_VERSION          (0xD4)
#define    REG_USI_I2C_VERSION          (0xD8)
#define    REG_USI_FIFO_DEPTH           (0xDC)

typedef struct {
    uint32_t USI;
    uint32_t SW_CONF;
}UsiSfrBaseType;

#include CSP_HEADER(usiSfrBase)

#endif /* __USI_SFR_BASE_H__ */

