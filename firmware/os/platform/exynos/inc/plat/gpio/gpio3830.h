/*----------------------------------------------------------------------------
 *      Exynos SoC  -  GPIO
 *----------------------------------------------------------------------------
 *      Name:    gpio3830.h
 *      Purpose: To expose GPIO APIs and define macros
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __GPIO3830_H__
#define __GPIO3830_H__

#include <csp_common.h>
#include <csp_assert.h>
#include <csp_printf.h>

typedef enum {
    GPIO_M00_0,
#define     GPM00_0_USI_CMGP00_RXD_SPICLK_SCL       0x2
#define     GPM00_0_NWEINT                          0xF
    GPIO_M01_0,
#define     GPM01_0_USI_CMGP00_TXD_SPIDO_SDA        0x2
#define     GPM01_0_NWEINT                          0xF
    GPIO_M02_0,
#define     GPM02_0_USI_CMGP00_RTS_SPIDI_NA         0x2
#define     GPM02_0_I2C_CMGP00_SCL                  0x3
#define     GPM02_0_NWEINT                          0xF
    GPIO_M03_0,
#define     GPM03_0_USI_CMGP00_CTS_SPICS_NA         0x2
#define     GPM03_0_I2C_CMGP00_SDA                  0x3
#define     GPM03_0_NWEINT                          0xF
    GPIO_M04_0,
#define     GPM04_0_USI_CMGP01_RXD_SPICLK_SCL       0x2
#define     GPM04_0_NWEINT                          0xF
    GPIO_M05_0,
#define     GPM05_0_USI_CMGP01_TXD_SPIDO_SDA        0x2
#define     GPM05_0_NWEINT                          0xF
    GPIO_M06_0,
#define     GPM06_0_USI_CMGP01_RTS_SPIDI_NA         0x2
#define     GPM06_0_I2C_CMGP01_SCL                  0x3
#define     GPM06_0_NWEINT                          0xF
    GPIO_M07_0,
#define     GPM07_0_USI_CMGP01_CTS_SPICS_NA         0x2
#define     GPM07_0_I2C_CMGP01_SDA                  0x3
#define     GPM07_0_NWEINT                          0xF
    GPIO_MAX_PIN_NUM
} GpioPinNumType;

typedef enum {
    // CHUB EINTs

    GPIO_CHUB_MAX_EINT_NUM = 0,

    // From here, CMGP EINTs
    NWEINT_GPM00_EINT_0 = GPIO_CHUB_MAX_EINT_NUM,
    NWEINT_GPM01_EINT_0,
    NWEINT_GPM02_EINT_0,
    NWEINT_GPM03_EINT_0,
    NWEINT_GPM04_EINT_0,
    NWEINT_GPM05_EINT_0,
    NWEINT_GPM06_EINT_0,
    NWEINT_GPM07_EINT_0,

    GPIO_MAX_EINT_NUM
} GpioEintNumType;

#endif

