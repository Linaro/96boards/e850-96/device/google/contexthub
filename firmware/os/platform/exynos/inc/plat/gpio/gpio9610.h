/*----------------------------------------------------------------------------
 *      Exynos SoC  -  GPIO
 *----------------------------------------------------------------------------
 *      Name:    gpio9610.h
 *      Purpose: To expose GPIO APIs and define macros
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __GPIO9610_H__
#define __GPIO9610_H__

#include <csp_common.h>
#include <csp_assert.h>
#include <csp_printf.h>

typedef enum {
    GPIO_H0_0 = 0,
#define     GPH0_0_USI00_CHUB_USI_RXD_SPICLK_SCL    0x2
#define     GPH0_0_CHUB_TOUT3                       0x3
#define     GPH0_0_NWEINT                           0xF
    GPIO_H0_1,
#define     GPH0_1_USI00_CHUB_USI_TXD_SPIDO_SDA     0x2
#define     GPH0_1_NWEINT                           0xF
    GPIO_H0_2,
#define     GPH0_2_USI00_CHUB_USI_RTS_SPIDI_NA      0x2
#define     GPH0_2_I2C_CHUB00_SCL                   0x3
#define     GPH0_2_NWEINT                           0xF
    GPIO_H0_3,
#define     GPH0_3_USI00_CHUB_USI_CTS_SPICS_NA      0x2
#define     GPH0_2_I2C_CHUB00_SDA                   0x3
#define     GPH0_3_NWEINT                           0xF

    GPIO_H1_0,
#define     GPH1_0_CHUB_TOUT0                       0x2
#define     GPH1_0_NWEINT                           0xF
    GPIO_H1_1,
#define     GPH1_1_CHUB_TOUT1                       0x2
#define     GPH1_1_NWEINT                           0xF
    GPIO_H1_2,
#define     GPH1_2_CHUB_TOUT2                       0x2
#define     GPH1_2_NWEINT                           0xF

    GPIO_M00_0,
#define     GPM00_0_USI_CMGP00_RXD_SPICLK_SCL       0x2
#define     GPM00_0_NWEINT                          0xF
    GPIO_M01_0,
#define     GPM01_0_USI_CMGP00_TXD_SPIDO_SDA        0x2
#define     GPM01_0_NWEINT                          0xF
    GPIO_M02_0,
#define     GPM02_0_USI_CMGP00_RTS_SPIDI_NA         0x2
#define     GPM02_0_I2C_CMGP00_SCL                  0x3
#define     GPM02_0_NWEINT                          0xF
    GPIO_M03_0,
#define     GPM03_0_USI_CMGP00_CTS_SPICS_NA         0x2
#define     GPM03_0_I2C_CMGP00_SDA                  0x3
#define     GPM03_0_NWEINT                          0xF
    GPIO_M04_0,
#define     GPM04_0_USI_CMGP01_RXD_SPICLK_SCL       0x2
#define     GPM04_0_NWEINT                          0xF
    GPIO_M05_0,
#define     GPM05_0_USI_CMGP01_TXD_SPIDO_SDA        0x2
#define     GPM05_0_NWEINT                          0xF
    GPIO_M06_0,
#define     GPM06_0_USI_CMGP01_RTS_SPIDI_NA         0x2
#define     GPM06_0_I2C_CMGP01_SCL                  0x3
#define     GPM06_0_NWEINT                          0xF
    GPIO_M07_0,
#define     GPM07_0_USI_CMGP01_CTS_SPICS_NA         0x2
#define     GPM07_0_I2C_CMGP01_SDA                  0x3
#define     GPM07_0_NWEINT                          0xF
    GPIO_M08_0,
#define     GPM08_0_USI_CMGP02_RXD_SPICLK_SCL       0x2
#define     GPM08_0_NWEINT                          0xF
    GPIO_M09_0,
#define     GPM09_0_USI_CMGP02_TXD_SPIDO_SDA        0x2
#define     GPM09_0_NWEINT                          0xF
    GPIO_M10_0,
#define     GPM10_0_USI_CMGP02_RTS_SPIDI_NA         0x2
#define     GPM10_0_I2C_CMGP02_SCL                  0x3
#define     GPM10_0_NWEINT                          0xF
    GPIO_M11_0,
#define     GPM11_0_USI_CMGP02_CTS_SPICS_NA         0x2
#define     GPM11_0_I2C_CMGP02_SDA                  0x3
#define     GPM11_0_NWEINT                          0xF
    GPIO_M12_0,
#define     GPM12_0_USI_CMGP03_RXD_SPICLK_SCL       0x2
#define     GPM12_0_GYRO_O_CSIS0_VVALID             0x6
#define     GPM12_0_NWEINT                          0xF
    GPIO_M13_0,
#define     GPM13_0_USI_CMGP03_TXD_SPIDO_SDA        0x2
#define     GPM13_0_GYRO_O_CSIS1_VVALID             0x6
#define     GPM13_0_NWEINT                          0xF
    GPIO_M14_0,
#define     GPM14_0_USI_CMGP03_RTS_SPIDI_NA         0x2
#define     GPM14_0_I2C_CMGP03_SCL                  0x3
#define     GPM14_0_GYRO_O_CSIS2_VVALID             0x6
#define     GPM14_0_NWEINT                          0xF
    GPIO_M15_0,
#define     GPM15_0_USI_CMGP03_CTS_SPICS_NA         0x2
#define     GPM15_0_I2C_CMGP03_SDA                  0x3
#define     GPM15_0_GYRO_O_CSIS3_VVALID             0x6
#define     GPM15_0_NWEINT                          0xF
    GPIO_M16_0,
#define     GPM16_0_USI_CMGP04_RXD_SPICLK_SCL       0x2
#define     GPM16_0_WB2AP_FEMCTRL_WLBT00            0x6
#define     GPM16_0_NWEINT                          0xF
    GPIO_M17_0,
#define     GPM17_0_USI_CMGP04_TXD_SPIDO_SDA        0x2
#define     GPM17_0_WB2AP_FEMCTRL_WLBT01            0x6
#define     GPM17_0_NWEINT                          0xF
    GPIO_M18_0,
#define     GPM18_0_USI_CMGP04_RTS_SPIDI_NA         0x2
#define     GPM18_0_I2C_CMGP04_SCL                  0x3
#define     GPM18_0_WB2AP_FEMCTRL_WLBT02            0x6
#define     GPM18_0_NWEINT                          0xF
    GPIO_M19_0,
#define     GPM19_0_USI_CMGP04_CTS_SPICS_NA         0x2
#define     GPM19_0_I2C_CMGP04_SDA                  0x3
#define     GPM19_0_WB2AP_FEMCTRL_WLBT03            0x6
#define     GPM19_0_NWEINT                          0xF
    GPIO_M20_0,
#define     GPM20_0_NWEINT                          0xF
    GPIO_M21_0,
#define     GPM21_0_NWEINT                          0xF
    GPIO_M22_0,
#define     GPM22_0_NWEINT                          0xF
    GPIO_M23_0,
#define     GPM23_0_NWEINT                          0xF
    GPIO_M24_0,
#define     GPM24_0_NWEINT                          0xF
    GPIO_M25_0,
#define     GPM25_0_NWEINT                          0xF


    GPIO_MAX_PIN_NUM
} GpioPinNumType;

typedef enum {
    // CHUB EINTs

    NWEINT_GPH0_EINT_0 = 0,
    NWEINT_GPH0_EINT_1,
    NWEINT_GPH0_EINT_2,
    NWEINT_GPH0_EINT_3,

    NWEINT_GPH1_EINT_0,
    NWEINT_GPH1_EINT_1,
    NWEINT_GPH1_EINT_2,
    GPIO_CHUB_MAX_EINT_NUM,

    // From here, CMGP EINTs
    NWEINT_GPM00_EINT_0 = GPIO_CHUB_MAX_EINT_NUM,
    NWEINT_GPM01_EINT_0,
    NWEINT_GPM02_EINT_0,
    NWEINT_GPM03_EINT_0,
    NWEINT_GPM04_EINT_0,
    NWEINT_GPM05_EINT_0,
    NWEINT_GPM06_EINT_0,
    NWEINT_GPM07_EINT_0,
    NWEINT_GPM08_EINT_0,
    NWEINT_GPM09_EINT_0,
    NWEINT_GPM10_EINT_0,
    NWEINT_GPM11_EINT_0,
    NWEINT_GPM12_EINT_0,
    NWEINT_GPM13_EINT_0,
    NWEINT_GPM14_EINT_0,
    NWEINT_GPM15_EINT_0,
    NWEINT_GPM16_EINT_0,
    NWEINT_GPM17_EINT_0,
    NWEINT_GPM18_EINT_0,
    NWEINT_GPM19_EINT_0,
    NWEINT_GPM20_EINT_0,
    NWEINT_GPM21_EINT_0,
    NWEINT_GPM22_EINT_0,
    NWEINT_GPM23_EINT_0,
    NWEINT_GPM24_EINT_0,
    NWEINT_GPM25_EINT_0,

    GPIO_MAX_EINT_NUM
} GpioEintNumType;

#endif

