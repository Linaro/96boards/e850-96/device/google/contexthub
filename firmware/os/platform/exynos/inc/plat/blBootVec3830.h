
// Boot vector
const struct BlVecTable __attribute__ ((section(".blvec"))) __BL_VECTORS = {
    /* cortex requirements */
    .blStackTop = (uint32_t) &__stack_top,
    .blEntry = &__blEntry,
    .blNmiHandler = &defaultHandler,
    .blHardFaultHandler = &defaultHandler,
    .blMmuFaultHandler = &defaultHandler,
    .blBusFaultHandler = &defaultHandler,
    .blUsageFaultHandler = &defaultHandler,
    .reservedHandler7 = &defaultHandler,
    .reservedHandler8 = &defaultHandler,
    .reservedHandler9 = &defaultHandler,
    .reservedHandler10 = &defaultHandler,
    .blSvcCallHandler = &defaultHandler,
    .reservedHandler12 = &defaultHandler,
    .reservedHandler13 = &defaultHandler,
    .blPendSvHandler = &defaultHandler,
    .blSysTickHandler = &defaultHandler,

    /* soc requirements */
    .isr0 = &defaultHandler,
    .isr1 = &defaultHandler,
    .isr2 = &defaultHandler,
    .isr3 = &defaultHandler,
    .isr4 = &defaultHandler,
    .isr5 = &defaultHandler,
    .isr6 = &defaultHandler,
    .isr7 = &defaultHandler,
    .isr8 = &defaultHandler,
    .isr9 = &defaultHandler,
    .isr10 = &defaultHandler,
    .isr11 = &mailboxApHandler,
    .isr12 = &defaultHandler,
#ifdef FULL_BL
    .isr13 = &mailboxApmHandler,
#endif
    .isr14 = &defaultHandler,
    .isr15 = &defaultHandler,
    .isr16 = &defaultHandler,
    .isr17 = &defaultHandler,
    .isr18 = &defaultHandler,
    .isr19 = &defaultHandler,
    .isr20 = &defaultHandler,
    .isr21 = &defaultHandler,
};

