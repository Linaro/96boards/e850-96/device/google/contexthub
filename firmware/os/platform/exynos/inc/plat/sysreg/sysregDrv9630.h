/*----------------------------------------------------------------------------
 *      Exynos SoC  -  SYSREG
 *----------------------------------------------------------------------------
 *      Name:    sysregDrv9630.h
 *      Purpose: To implement SYSREG driver functions for 963330
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __SYSREG_DRIVER_9630_H__
#define __SYSREG_DRIVER_9630_H__

// CHUB
#define    REG_SYSREG_BUS_COMPONENET_DRCG_EN       (SYSREG_BASE_ADDRESS + 0x104)
#define    REG_SYSREG_PD_REQ                       (SYSREG_BASE_ADDRESS + 0x220)
#define    REG_SYSREG_EALY_WAKEUP_WINDOW_REQ       (SYSREG_BASE_ADDRESS + 0x230)
#define    REG_SYSREG_APM_UP_STATUS                (SYSREG_BASE_ADDRESS + 0x240)
#define    REG_SYSREG_CLEAR_VVALID                 (SYSREG_BASE_ADDRESS + 0x250)
#define    REG_SYSREG_CMGP_REQ_OUT                 (SYSREG_BASE_ADDRESS + 0x260)
#define    REG_SYSREG_CMGP_REQ_ACK_IN              (SYSREG_BASE_ADDRESS + 0x264)
#define    REG_SYSREG_OSCCLK_ENABLE                (SYSREG_BASE_ADDRESS + 0x270)
#define    REG_SYSREG_HWACG_CM4_CLKREQ             (SYSREG_BASE_ADDRESS + 0x428)

#define    REG_SYSREG_USI_CHUB00_SW_CONF           (SYSREG_BASE_ADDRESS + 0x2000)
#define    REG_SYSREG_I2C_CHUB00_SW_CONF           (SYSREG_BASE_ADDRESS + 0x2004)
#define    REG_SYSREG_USI_CHUB01_SW_CONF           (SYSREG_BASE_ADDRESS + 0x2010)
#define    REG_SYSREG_I2C_CHUB01_SW_CONF           (SYSREG_BASE_ADDRESS + 0x2014)
#define    REG_SYSREG_USI_CHUB02_SW_CONF           (SYSREG_BASE_ADDRESS + 0x2020)
#define    REG_SYSREG_I2C_CHUB02_SW_CONF           (SYSREG_BASE_ADDRESS + 0x2024)

#define    REG_SYSREG_USI_CHUB00_IPCLK             (SYSREG_BASE_ADDRESS + 0x3000)
#define    REG_SYSREG_I2C_CHUB00_IPCLK             (SYSREG_BASE_ADDRESS + 0x3004)
#define    REG_SYSREG_USI_CHUB01_IPCLK             (SYSREG_BASE_ADDRESS + 0x3010)
#define    REG_SYSREG_I2C_CHUB01_IPCLK             (SYSREG_BASE_ADDRESS + 0x3014)
#define    REG_SYSREG_USI_CHUB02_IPCLK             (SYSREG_BASE_ADDRESS + 0x3020)
#define    REG_SYSREG_I2C_CHUB02_IPCLK             (SYSREG_BASE_ADDRESS + 0x3024)

#define    REG_SYSREG_USI_CMGP00_SW_CONF           (SYSREG_CMGP_BASE_ADDRESS + 0x2000)
#define    REG_SYSREG_I2C_CMGP00_SW_CONF           (SYSREG_CMGP_BASE_ADDRESS + 0x2004)
#define    REG_SYSREG_USI_CMGP01_SW_CONF           (SYSREG_CMGP_BASE_ADDRESS + 0x2010)
#define    REG_SYSREG_I2C_CMGP01_SW_CONF           (SYSREG_CMGP_BASE_ADDRESS + 0x2014)
#define    REG_SYSREG_USI_CMGP02_SW_CONF           (SYSREG_CMGP_BASE_ADDRESS + 0x2020)
#define    REG_SYSREG_I2C_CMGP02_SW_CONF           (SYSREG_CMGP_BASE_ADDRESS + 0x2024)
#define    REG_SYSREG_USI_CMGP03_SW_CONF           (SYSREG_CMGP_BASE_ADDRESS + 0x2030)
#define    REG_SYSREG_I2C_CMGP03_SW_CONF           (SYSREG_CMGP_BASE_ADDRESS + 0x2034)

#define    REG_SYSREG_USI_CMGP00_IPCLK             (SYSREG_CMGP_BASE_ADDRESS + 0x3000)
#define    REG_SYSREG_I2C_CMGP00_IPCLK             (SYSREG_CMGP_BASE_ADDRESS + 0x3004)
#define    REG_SYSREG_USI_CMGP01_IPCLK             (SYSREG_CMGP_BASE_ADDRESS + 0x3010)
#define    REG_SYSREG_I2C_CMGP01_IPCLK             (SYSREG_CMGP_BASE_ADDRESS + 0x3014)
#define    REG_SYSREG_USI_CMGP02_IPCLK             (SYSREG_CMGP_BASE_ADDRESS + 0x3020)
#define    REG_SYSREG_I2C_CMGP02_IPCLK             (SYSREG_CMGP_BASE_ADDRESS + 0x3024)
#define    REG_SYSREG_USI_CMGP03_IPCLK             (SYSREG_CMGP_BASE_ADDRESS + 0x3030)
#define    REG_SYSREG_I2C_CMGP03_IPCLK             (SYSREG_CMGP_BASE_ADDRESS + 0x3034)

typedef enum {
    SWCONF_USI_CHUB00_SW_CONF,
    SWCONF_I2C_CHUB00_SW_CONF,
    SWCONF_USI_CHUB01_SW_CONF,
    SWCONF_I2C_CHUB01_SW_CONF,
    SWCONF_USI_CHUB02_SW_CONF,
    SWCONF_I2C_CHUB02_SW_CONF,

    SWCONF_USI_CMGP00_SW_CONF,
    SWCONF_I2C_CMGP00_SW_CONF,
    SWCONF_USI_CMGP01_SW_CONF,
    SWCONF_I2C_CMGP01_SW_CONF,
    SWCONF_USI_CMGP02_SW_CONF,
    SWCONF_I2C_CMGP02_SW_CONF,
    SWCONF_USI_CMGP03_SW_CONF,
    SWCONF_I2C_CMGP03_SW_CONF,

    SYSREG_SWCONF_PORT_MAX
} SysregSwConfPortType;

typedef enum {
    SWCONF_USI_UART = 0x1,
    SWCONF_USI_SPI = 0x2,
    SWCONF_USI_I2C = 0x4,

    SYSREG_SWCONF_PROTOCOL_MAX
} SysregSwConfProtocolType;

void sysregDrvInit(void);
void sysregDrvSetSwConf(IN SysregSwConfPortType port, IN SysregSwConfProtocolType protocol);
void sysregDrvSetHWACG(IN uint32_t enable);
void sysregDrvSetClkreqMaskIrq(IN uint32_t irq);
void sysregDrvClearClkreqMaskIrq(IN uint32_t irq);
uint32_t sysregDrvGetClkreqMaskIrq(void);
void sysregDrvDeinit(void);
void sysregDrvSetOscEn(void);
void sysregDrvSetOscDis(void);
void sysregDrvSaveState(void);
void sysregDrvRestoreState(void);
#endif

