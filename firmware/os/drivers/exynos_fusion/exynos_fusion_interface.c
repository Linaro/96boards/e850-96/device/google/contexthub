/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <seos.h>
#include "exynos_fusion.h"

#define DERIV_THR_AMD 0.55f
#define MODE_AMD		7		// Mode
// MODE_AMD Description; [1, 7]
// Which axis to be activated is determined depending on the MODE_AMD as follows,
//
// MODE_AMD   |   z_axis   |   y_axis   |   x_axis
//	   1      |     X      |     X      |     O
//	   2      |     X      |     O      |     X
//	   3      |     X      |     O      |     O
//	   4      |     O      |     X      |     X
//	   5      |     O      |     X      |     O
//	   6      |     O      |     O      |     X
//	   7      |     O      |     O      |     O

void exynosFusionGetParams(struct exynosFusionParams *params)
{
    if (params) {
        params->deriv_thr = DERIV_THR_AMD;
		params->axis_on_flags = MODE_AMD;
    }
}

enum exynosFusionSensorType {
    ANYMOTION_DETECT_MODULE,
    SIGMOTION_DETECT_MODULE,
    STEP_DETECT_MODULE,
    STEP_COUNT_MODULE,
    NUM_OF_EXYNOS_FUSION_SENSOR
};

void exynosFusionEnableModules(struct tEnableModule *enM)
{
	if (enM) {
	    enM->enableModule[ANYMOTION_DETECT_MODULE] = true;
	    enM->enableModule[SIGMOTION_DETECT_MODULE] = true;
	    enM->enableModule[STEP_DETECT_MODULE] = true;
	    enM->enableModule[STEP_COUNT_MODULE] = true;	
	}
}

INTERNAL_APP_INIT(
    EXYNOS_FUSION_APP_ID,
    EXYNOS_FUSION_APP_VERSION,
    exynosFusionStart,
    exynosFusionEnd,
    exynosFusionHandleEvent);
