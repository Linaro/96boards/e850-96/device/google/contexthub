/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXYNOS_FUSION_H
#define EXYNOS_FUSION_H

#define EXYNOS_FUSION_APP_ID APP_ID_MAKE(NANOHUB_VENDOR_EXYNOS, 3)

#define EXYNOS_FUSION_APP_VERSION  1

struct exynosFusionParams {
    float deriv_thr;
	int axis_on_flags;
};

struct tEnableModule {
    bool enableModule[4];
};

bool exynosFusionStart(uint32_t tid);
void exynosFusionEnd(void);
void exynosFusionHandleEvent(uint32_t evtType, const void *evtData);
void exynosFusionGetParams(struct exynosFusionParams *params);
void exynosFusionEnableModules(struct tEnableModule *enM);


#endif
