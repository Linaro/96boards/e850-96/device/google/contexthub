/*----------------------------------------------------------------------------
 *      Exynos SoC  - Test Configuration
 *----------------------------------------------------------------------------
 *      Name:    exynos_testxxxx.h
 *      Purpose: To implement gpio UTC codes
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __EXYNOS_TEST_CONFIG_H__
#define __EXYNOS_TEST_CONFIG_H__

/*********************************
 * I2C Test
 * Test w/ BMP280
 */
#define Test_I2C_BUS_ID     I2C_CMGP5
#define Test_I2C_ADDR       0x76
#define Test_I2C_SPEED      400000
#define Test_I2C_ADDR       0x76
#define Test_I2C_SPEED      400000
#define REG_RESET           0x60
#define REG_VALUE           0xb6
#define REG_ID              0xD0
#define REG_CTRL_MEAS       0xF4
#define REG_CONFIG          0xF5

#define ID_VALUE            0x58

#define RESET_REQUIRED      1


/*********************************
 * GPIO Test
 */
#define GPIO_NO             GPIO_H0_0
#define EINT_NO             NWEINT_GPH0_EINT_0
#define IRQ_NO              GPIO_CHUB_IRQn
#define MAX_COUNT_GPIO_EINT 3

#endif
