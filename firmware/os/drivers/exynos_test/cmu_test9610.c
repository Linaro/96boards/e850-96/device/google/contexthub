/*----------------------------------------------------------------------------
 *      Exynos SoC  -  CMU UTC
 *----------------------------------------------------------------------------
 *      Name:    cmu_test.c
 *      Purpose: To implement cmu UTC codes
 *      Rev.:    V1.00
 *----------------------------------------------------------------------------
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if defined(UTC_REQUIRED)

#include <cmu_test.h>

/*
    CMU UTC
*/
void cmu_test(void)
{
    CSP_PRINTF_INFO("RCO - %ld Hz\n", cmuGetSpeed(CMU_CLK_OSCCLK_RCO));
	CSP_PRINTF_INFO("RTC - %ld Hz\n", cmuGetSpeed(CMU_CLK_RTCCLK));
	//CSP_PRINTF_INFO("CMUCMU - %ld Hz\n", cmuGetSpeed(CMU_CLK_OUTPUT_CMUCMU));
	//CSP_PRINTF_INFO("CMUAPM - %ld Hz\n", cmuGetSpeed(CMU_CLK_OUTPUT_CMUAPM));
	CSP_PRINTF_INFO("CPU - %ld Hz\n", cmuGetSpeed(CMU_CLK_OUTPUT_CPU));
	CSP_PRINTF_INFO("TIMER - %ld Hz\n", cmuGetSpeed(CMU_CLK_CHUB_TIMER));
	CSP_PRINTF_INFO("BUS - %ld Hz\n", cmuGetSpeed(CMU_CLK_CHUB_BUS));
	CSP_PRINTF_INFO("I2C - %ld Hz\n", cmuGetSpeed(CMU_CLK_CHUB_I2C));
	CSP_PRINTF_INFO("USI00 - %ld Hz\n", cmuGetSpeed(CMU_CLK_CHUB_USI00));

	//CSP_PRINTF_INFO("WDT - %ld Hz\n", cmuGetSpeed(CMU_CLK_CHUB_WDT));
	CSP_PRINTF_INFO("CMGP I2C - %ld Hz\n", cmuGetSpeed(CMU_CLK_CMGP_I2C));
	CSP_PRINTF_INFO("CMGP USI00 - %ld Hz\n", cmuGetSpeed(CMU_CLK_CMGP_USI00));
	CSP_PRINTF_INFO("CMGP USI01 - %ld Hz\n", cmuGetSpeed(CMU_CLK_CMGP_USI01));
	CSP_PRINTF_INFO("CMGP USI02 - %ld Hz\n", cmuGetSpeed(CMU_CLK_CMGP_USI02));
	CSP_PRINTF_INFO("CMGP USI03 - %ld Hz\n", cmuGetSpeed(CMU_CLK_CMGP_USI03));
}

void cmu_test_clean_up(void)
{
    // Do clean-up

	CSP_PRINTF_ERROR("%s is called\n", __func__);
}

#endif

