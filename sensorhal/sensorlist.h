/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SENSORLIST_H_

#define SENSORLIST_H_

#include <hardware/sensors.h>

#define SENSOR_TYPE_MAX (SENSOR_TYPE_ACCELEROMETER_UNCALIBRATED + 1) /* match chub fw */

extern const float kScaleAccel;
extern const float kScaleMag;
// A list of sensors provided by a device.
extern sensor_t kSensorList[];
extern const size_t kSensorCount;

enum vendor_sensor_list_id {
	sensor_list_no_active,
	sensor_list_rpr0521_prox,
	sensor_list_rpr0521_ligth,
	sensor_list_bmi160_accel,
	sensor_list_bmi160_accel_ucal,
	sensor_list_bmi160_gyro,
	sensor_list_bmi160_gyro_ucal,
	sensor_list_stlis3mdl_mag,
	sensor_list_stlis3mdl_mag_ucal,
	sensor_list_bmp280_press,
	sensor_list_max,
};

struct vendor_sensor_list_t {
	const char *name;
	const char *vendor;
};

struct saved_setting {
    char magic[15];
    int8_t num_os;
    char readbuf[SENSOR_TYPE_MAX];
};

#define sensors_accel ( 1 << sensor_list_bmi160_accel )

#define sensors_accel_uncal ( 1 << sensor_list_bmi160_accel_ucal )

#define sensors_mag ( 1 << sensor_list_stlis3mdl_mag )

#define sensors_mag_uncal ( 1 << sensor_list_stlis3mdl_mag_ucal )

#define sensors_gyro ( 1 << sensor_list_bmi160_gyro )

#define sensors_gyro_uncal ( 1 << sensor_list_bmi160_gyro_ucal )

#define sensors_light ( 1 << sensor_list_rpr0521_ligth )

#define sensors_prox ( 1 << sensor_list_rpr0521_prox )

#define sensors_press ( 1 << sensor_list_bmp280_press )

extern vendor_sensor_list_t vensor_sensor_list[];
const char unactive_sensor[] = "unactive sensor";
const char default_vendor[] = "exynos";

// TODO(aarossig): Consider de-duplicating Google sensors.

#endif  // SENSORLIST_H_
